<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Интеграция сайта и 1С");
?><div class="banner-about banner-with-img">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1><? $APPLICATION->ShowTitle(false);?></h1>
				<p class="baner-disc">
					 Интеграция сайта с системой «1С:Предприятие» поможет автоматизировать учет товаров и продажи в интернете.
				</p>
			</div>
			<div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
				 <!--<img src="/local/templates/prbel/img/page/kotly.png" alt="" class="img-responsive">-->
			</div>
		</div>
		<div class="row krosh">
			<p>
 <a href="/">Главная</a> <i class="fa fa-angle-right" aria-hidden="true"></i>
				Интеграция сайта и 1С
			</p>
		</div>
	</div>
</div>
<div class="direction">
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-2 col-lg-10">
				<div class="row">
					<div class="col-lg-6">
						<p>
							 Интеграция «1С-Битрикс: Управление сайтом» с торговыми конфигурациями «1С: Предприятие» помогает вам решить следующие технологические задачи:
						</p>
						<p>
						</p>
						<ol>
							<li>Публикация товарной номенклатуры в каталоге интернет-магазина</li>
							<li>Передача заказов на сайте в 1С для обработки</li>
							<li>Выгрузка результатов обработки заказов на сайт для уведомления клиентов</li>
						</ol>
					</div>
					<div class="col-lg-6">
 <img src="https://partners.1c-bitrix.ru/images/partners/gold_sm.gif" style="padding-left: 15px; padding-bottom: 20px;">
						<h2 style="margin-bottom: 20px;">
						100+ запущенных сайтов в различных отраслях </h2>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="why-dev">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h2>Интернет-магазин, интегрированный с 1С</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 dev-item">
				<div class="dev-icon">
 <img width="100px" src="https://ground-c.dev-prbel.ru/images/synchronization.png" alt="" style="padding:5px;">
				</div>
				<div class="dev-title">
					 Real-time обмен с «1С»
				</div>
				<div class="dev-text">
					 Это технология двусторонней непрерывной связи между «1С-Битрикс: Управление сайтом» и «1С». Система обеспечивает обмен данными в режиме реального времени между этими программными продуктами. Она не требует установки отдельного сервера и специальных настроек.
				</div>
			</div>
			<div class="col-lg-3 dev-item">
				<div class="dev-icon">
 <img width="100px" src="https://pngimage.net/wp-content/uploads/2018/05/clock-icon-png-white-4.png" alt="" style="padding:6px;">
				</div>
				<div class="dev-title" style="margin-bottom: 12px;">
					 Возможность автоматической работы по расписанию
				</div>
				<div class="dev-text">
					 Функционал интеграции может работать совершенно незаметно для сотрудников, не нагружая их рутинными операциями. Система сама будет выполнять заданные операции в указанные временные интервалы.
				</div>
			</div>
			<div class="col-lg-3 dev-item">
				<div class="dev-icon">
 <img width="100px" src="https://confirmedautomation.com/wp-content/uploads/2017/01/efficiency-icon.png" alt="">
				</div>
				<div class="dev-title">
					 Производительность
				</div>
				<div class="dev-text">
					 Ваш интернет-магазин может работать с огромным количеством посетителей, одновременно многие из них могут работать с каталогом продукции, осуществлять заказ товара. Сайт может выдерживать любую нагрузку, при этом не влиять на производительность «1С».
				</div>
			</div>
			<div class="col-lg-3 dev-item">
				<div class="dev-icon">
 <img src="https://www.nibe.com/images/200.5e8ad41616fafe28d6c22ff7/1580122300434/CS_icon_white.png" height="100px" alt="">
				</div>
				<div class="dev-title">
					 Безопасность
				</div>
				<div class="dev-text">
					 Никакие угрозы безопасности сайта не могут угрожать безопасности ваших данных в «1С». Веб-сайт работает на удаленном хостинге и не имеет доступа к системе «1С» и ее базе данных. Нарушение безопасности «1С» также не грозит безопасности веб-сайта.
				</div>
			</div>
		</div>
	</div>
</div>
<div class="why-profit">
	<div class="container">
		<div class="row text-center">
			<h2>
			Принципы разработки сайтов </h2>
		</div>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-2">
				<div class="row">
					 <!--<div class="col-lg-1 col-md-1 col-sm-2 col-xs-12">
						<div class="img-wrap">
 <img src="/local/templates/prbel/img/page/check.svg" alt="" class="img-responsive">
						</div>
					</div>-->
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="profit-title">
							 Просто
						</div>
						<div class="profit-text">
							 Сайт легко продвинуть, и он приносит прибыль. Весь процесс разработки подчинён идее эффективности и целесообразности.
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="profit-title">
							 Удобно
						</div>
						<div class="profit-text">
							 Мы разрабатываем продающие и SEO-оптимизированные сайты, а вы экономите на поисковом продвижении. При разработке сайта мы всегда помним о цели — сайт должен приносить прибыль бизнесу.
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="profit-title">
							 Выгодно
						</div>
						<div class="profit-text">
							 Мы разработали более 100 проектов, которые окупили затраты на их разработку.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/contact_us.php"
	)
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>