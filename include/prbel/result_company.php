<div class="result-company">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 text-center">
        <h2>
          Наши кейсы
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="owl-carousel owl-theme" id="result-company">
             <div class="item">
            <div class="row">
              <div class="col-xs-12">
            <h3 style="text-align: center;margin: 0px 0px 35px 0;">Оптимизация рекламной кампании для тренажерного зала</h3>
                <img src="/local/templates/prbel/img/page/3slide.png" alt="">
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 text-slide">
              Сравнение созданной нами рекламной кампаниии (авг.) и кампании прошлого подрядчика (июл.) для клиента-тренажерного зала. <br /> Количество заявок увеличилось на 127%. При этом цена клика сократилась в 2,5 раза.
              </div>
            </div>
          </div>

           <!--  <div class="item">
            <div class="row">
              <div class="col-xs-12">
                <img src="/local/templates/prbel/img/page/4slide.png" alt="">
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 text-slide">
                Пример улучшенной нами рекламной кампаниии для клиента-дилерского центра. <br /> Количество заявок увеличилось на 39% при скращении бюджета почти на 10%
              </div>
            </div>
          </div> -->

             <div class="item">
            <div class="row">
              <div class="col-xs-12">
                 <h3 style="text-align: center;margin: 0px 0px 35px 0;">Оптимизация рекламной кампании для автодилера</h3>
                <img src="/local/templates/prbel/img/page/5slide.png" alt="">
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 text-slide">
                В результате оптимизации при том же бюджете удалось получить на 92% заявок больше. При этом стоимость заявки снизилась практически в 2 раза.
                <!--Пример улучшенной нами рекламной кампаниии для клиента-дилерского центра. <br /> Количество заявок увеличилось на 92%-->
              </div>
            </div>
          </div>

            <!-- <div class="item">
            <div class="row">
              <div class="col-xs-12">
                <img src="/local/templates/prbel/img/page/6slide.png" alt="">
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 text-slide">
                Пример улучшенной нами рекламной кампаниии для клиента в сфере гостиничного бизнеса. <br /> Количество заявок увеличилось на 46%  при скращении бюджета на 10%
              </div>
            </div>
          </div> -->






          <div class="item">
            <div class="row">
              <div class="col-xs-12">
                <img src="/local/templates/prbel/img/page/2slide.png" alt="">
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 text-slide">
                Пример правильно настроенной рекламной кампании в сфере натяжных потолков.<br />
                Рентабельность инвестиций составила 699%
              </div>
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="col-xs-12">
                                <h3 style="text-align: center;margin: 0px 0px 35px 0;">Оптимизация рекламной кампании для гостиницы</h3>
                <img src="/local/templates/prbel/img/page/1slide.png" alt="">
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 text-slide">
                В результате оптимизации кампаний мы получили в 5,5 раз больше заявок при том, что общий бюджет уменьшился на 44%, а цена заявки снизилась более, чем в 10 раз.
                <!-- Пример улучшенной нами рекламной кампании в сфере гостиничного бизнеса.<br />
                При уменьшении бюджета на 10 714 руб.,
                количество бронирований<br /> увеличилось на 550% -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>