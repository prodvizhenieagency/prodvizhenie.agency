<noindex>
<div class="trust">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<h2 style="margin-bottom:25px;">Нам доверяют диджитал-маркетинг</h2>
				<p class="discription"> Выстраиваем прочные, взаимовыгодные и долгосрочные отношения
</p>
			</div>
		</div>

<!--		<div class="row visible-xs-block">-->
<!--			<div class="col-xs-12 ">-->
<!--				<div id="owl-demo2" class="owl-carousel owl-theme ">-->
<!--					<div class="item">-->
<!--						<div class="container">-->
<!--							<div class="row">-->
<!--								<div class="col-xs-8 col-xs-offset-2 text-center">-->
<!--									<p><img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/trast-1.png" alt="trast-1"></p>-->
<!--								</div>-->
<!--							</div>-->
<!---->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--					<div class="item">-->
<!--						<div class="container">-->
<!--							<div class="row">-->
<!--								<div class="col-xs-8 col-xs-offset-2 text-center">-->
<!--									<p><img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/trast-2.png" alt="trast-1"></p>-->
<!--								</div>-->
<!--							</div>-->
<!---->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--					<div class="item">-->
<!--						<div class="container">-->
<!--							<div class="row">-->
<!--								<div class="col-xs-8 col-xs-offset-2 text-center">-->
<!--									<p><img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/trast-3.png" alt="trast-1"></p>-->
<!--								</div>-->
<!--							</div>-->
<!---->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--					<div class="item">-->
<!--						<div class="container">-->
<!--							<div class="row">-->
<!--								<div class="col-xs-8 col-xs-offset-2 text-center">-->
<!--									<p><img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/trast-4.png" alt="trast-1"></p>-->
<!--								</div>-->
<!--							</div>-->
<!---->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--					<div class="item">-->
<!--						<div class="container">-->
<!--							<div class="row">-->
<!--								<div class="col-xs-8 col-xs-offset-2 text-center">-->
<!--									<p><img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/trast-5.png" alt="trast-1"></p>-->
<!--								</div>-->
<!--							</div>-->
<!---->
<!--						</div>-->
<!--					</div>-->
<!---->
<!---->
<!---->
<!---->
<!--				</div><!-- olw-demo -->
<!--			</div>-->
<!--		</div>-->
<div class="row row-sapce">
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-md-offset-2 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/wwf-min.png"  srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/wwf-min.png" alt="trast-1" style=""></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/toyota-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/toyota-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/ground-c-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/ground-c-min.png" alt="trast-1" style=""></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/istore-min.svg" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/istore-min.svg" alt="trast-1" style=""></p>
			</div>
		</div>



		<div class="row row-sapce">
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-md-offset-2 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/belarus-min.png"  srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/belarus-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/volkswagen-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/volkswagen-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/fosagro-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/fosagro-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/amaks-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/amaks-min.png" alt="trast-1"></p>
			</div>
		</div>

		<div class="row  row-sapce">
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-md-offset-2 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/balans-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/balans-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/aerobel-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/aerobel-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/lada-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/lada-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/max-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/max-min.png" alt="trast-1"></p>
			</div>
		</div>

		<div class="row row-sapce ">
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-md-offset-2 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/renault-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/renault-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
			    <p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/trikolor-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/trikolor-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
			    <p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/famil-dom-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/famil-dom-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/opc-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/opc-min.png" alt="trast-1"></p>
			</div>
		</div>

		<div class="row ">
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-md-offset-2 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/shit-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/shit-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/premiera-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/premiera-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
			    <p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/santehkomplekt-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/santehkomplekt-min.png" alt="trast-1"></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space wow zoomIn">
				<p class="theytrustus"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/tehnodref-min.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/tehnodref-min.png" alt="trast-1"></p>
			</div>
		</div>
	</div>
</div>
<br>
</noindex>