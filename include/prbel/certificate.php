<div class="row">
			<div class="col-sm-12 text-center">
				<h2 style="font-weight:600;">Наши награды и достижения</h2>
			</div>
		</div>
<section class="certificate text-center">
<div class="container">
	<div class="row row-certificate">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
			<div class="img-wrap">
<a href="https://yandex.ru/adv/contact/agencies/prodvizhenie" rel="nofollow" target="_blank"> <img width="115" alt="Сертифицированное агентство" src="https://avatars.mds.yandex.net/get-adv/114583/2a0000015d60261ab2a5916f9d25bcdd552b/orig" height="115" border="0"> </a>
			</div>
			<div class="text">
				 Сертифицированное агентство
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
			<div class="img-wrap">
 <img src="/local/templates/prbel/img/sert/yandex-min.png" srcset="/local/templates/prbel/img/sert/yandex-min.png" alt="">
			</div>
			<div class="text">
				 Сертифицированные специалисты
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
			<div class="img-wrap">
 <a href="https://www.google.com/partners/agency?id=2113442239&roistat_visit=61346" rel="nofollow" target="_blank"> <img src="/local/templates/prbel/img/sert/google-min.png" srcset="/local/templates/prbel/img/sert/google-min.png" alt=""> </a>
			</div>
			<div class="text">
				 Google Partner
			</div>
		</div>	
	</div>
	<br>
			<div class="row row-certificate">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
			<div class="img-wrap">
 <img src="/local/templates/prbel/img/sert/adv-min.png" srcset="/local/templates/prbel/img/sert/adv-min.png" alt="">
			</div>
			<div class="text">
				 Сертифицированные специалисты
			</div>
		</div>

		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 wow zoomIn">
			<div class="img-wrap">
 <img src="/images/gold-part.png" class="bxhtmled-surrogate">
			</div>
			<div class="text">
				 Золотой партнер 1С-Битрикс
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 wow zoomIn">
			<div class="img-wrap">
 <img src="/images/seo-news-12.png" class="bxhtmled-surrogate" style=" width: 105px !important;">
			</div>
			<div class="text">
				 12 место по России
			</div>
		</div>
	</div>
	 <!--		<div class="row">--> <!--			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 wow zoomIn">--> <!--				<div class="img-wrap">--> <!--					<img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/sert/karta-min.png" srcset="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/sert/karta-min.png" alt="">--> <!--				</div>--> <!--				<div class="text">--> <!--					Местные--> <!--					эксперты--> <!--				</div>--> <!--			</div>--> <!--			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 wow zoomIn">--> <!--				<div class="img-wrap">--> <!--					<img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/sert/reg-min.png" srcset="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/sert/reg-min.png" alt="">--> <!--				</div>--> <!--				<div class="text">--> <!--					Официальный--> <!--					партнер--> <!--				</div>--> <!--			</div>--> <!--			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 wow zoomIn">--> <!--				<div class="img-wrap">--> <!--					<img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/sert/partner-min.png" srcset="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/sert/partner-min.png" alt="">--> <!--				</div>--> <!--				<div class="text">--> <!--					Официальный--> <!--					партнер--> <!--				</div>--> <!--			</div>--> <!--			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 wow zoomIn">--> <!--				<div class="img-wrap">--> <!--					<img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/sert/crm-min.png" srcset="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/sert/crm-min.png" alt="">--> <!--				</div>--> <!--				<div class="text">--> <!--					Официальный--> <!--					партнер--> <!--				</div>--> <!--			</div>--> <!--		</div>-->
</div>
 </section>