<div class="trust">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<h2>Они нам доверяют</h2>
				<p class="discription">Больше всего мы гордимся успехами наших клиентов. Нам доверяют компании из Москвы, Санкт-Петербурга, Белгорода. Мы выстраиваем прочные, надёжные и долгосрочные отношения.
</p>
			</div>
		</div>

<!--		<div class="row visible-xs-block">-->
<!--			<div class="col-xs-12 ">-->
<!--				<div id="owl-demo2" class="owl-carousel owl-theme ">-->
<!--					<div class="item">-->
<!--						<div class="container">-->
<!--							<div class="row">-->
<!--								<div class="col-xs-8 col-xs-offset-2 text-center">-->
<!--									<a href="#"><img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/trast-1.png" alt="trast-1"></a>-->
<!--								</div>-->
<!--							</div>-->
<!---->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--					<div class="item">-->
<!--						<div class="container">-->
<!--							<div class="row">-->
<!--								<div class="col-xs-8 col-xs-offset-2 text-center">-->
<!--									<a href="#"><img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/trast-2.png" alt="trast-1"></a>-->
<!--								</div>-->
<!--							</div>-->
<!---->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--					<div class="item">-->
<!--						<div class="container">-->
<!--							<div class="row">-->
<!--								<div class="col-xs-8 col-xs-offset-2 text-center">-->
<!--									<a href="#"><img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/trast-3.png" alt="trast-1"></a>-->
<!--								</div>-->
<!--							</div>-->
<!---->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--					<div class="item">-->
<!--						<div class="container">-->
<!--							<div class="row">-->
<!--								<div class="col-xs-8 col-xs-offset-2 text-center">-->
<!--									<a href="#"><img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/trast-4.png" alt="trast-1"></a>-->
<!--								</div>-->
<!--							</div>-->
<!---->
<!--						</div>-->
<!--					</div>-->
<!---->
<!--					<div class="item">-->
<!--						<div class="container">-->
<!--							<div class="row">-->
<!--								<div class="col-xs-8 col-xs-offset-2 text-center">-->
<!--									<a href="#"><img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/trast-5.png" alt="trast-1"></a>-->
<!--								</div>-->
<!--							</div>-->
<!---->
<!--						</div>-->
<!--					</div>-->
<!---->
<!---->
<!---->
<!---->
<!--				</div><!-- olw-demo -->
<!--			</div>-->
<!--		</div>-->
		<div class="row row-sapce ">
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-md-offset-2 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/belarus.png"  srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/belarus@2x.png" alt="trast-1"></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/volkswagen.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/volkswagen@2x.png" alt="trast-1"></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/fosagro.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/fosagro@2x.png" alt="trast-1"></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/amaks.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/amaks@2x.png" alt="trast-1"></a>
			</div>
		</div>

		<div class="row  row-sapce">
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-md-offset-2 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/balance.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/balance@2x.png" alt="trast-1"></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/aerobel.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/aerobel@2x.png" alt="trast-1"></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/lada.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/lada@2x.png" alt="trast-1"></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/max_interior.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/max_interior@2x.png" alt="trast-1"></a>
			</div>
		</div>

		<div class="row row-sapce ">
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-md-offset-2 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/renault.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/renault@2x.png" alt="trast-1"></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/trikolor.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/trikolor@2x.png" alt="trast-1"></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/family_house.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/family_house@2x.png" alt="trast-1"></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/old_president_club.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/old_president_club@2x.png" alt="trast-1"></a>
			</div>
		</div>

		<div class="row ">
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-md-offset-2 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/shield.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/shield@2x.png" alt="trast-1"></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/premiera.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/premiera@2x.png" alt="trast-1"></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/santechcomplect.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/santechcomplect@2x.png" alt="trast-1"></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center col-space">
				<a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/clients/technodrev.png" srcset="<?= SITE_TEMPLATE_PATH; ?>/img/clients/technodrev@2x.png" alt="trast-1"></a>
			</div>
		</div>
	</div>
</div>