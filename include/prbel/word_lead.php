<div class="word-lead">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
				<div class="word-wrap">
					 Мы считаем каждый клик, каждый звонок, каждый лид, поэтому Вы знаете, что Ваш рекламный рубль работает на развитие бизнеса.
				</div>
				<div class="bottom-line">
					<svg version="1.1" viewBox="0 0 1200 31.7" width="1200px" height="40px"> <g><polygon points="40,31.7 40,4 0,4 0,0 44,0 44,22.3 67.2,0 1200,0 1200,4 68.8,4 "></polygon></g> </svg>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
				<div class="lead-wrap">
					<div class="row">
						<div class="author-img">
 <img src="/images/Sveta.jpg" alt="" class="img-responsive">
						</div>
						<div class="author-name">
							<div class="name">
								 Светлана Бочарникова
							</div>
							<div class="status">
								 Ведущий менеджер проектов
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <br>