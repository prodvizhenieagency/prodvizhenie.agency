<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "⭐ Комплексное SEO продвижение сайтов в Санкт-Петербурге ⚡ SEO оптимизация и эффективная раскрутка сайта в ТОП Google и Яндекса от digital-агентства “Продвижение” ☎️ Звоните 8 (812) 660-59-31!");
$APPLICATION->SetPageProperty("title", "SEO-продвижение сайтов в СПб | Цены на раскрутку сайта");
$APPLICATION->SetTitle("SEO-продвижение сайтов в Санкт-Петербурге");
?>

<div class="banner-about banner-with-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <h1>SEO-продвижение сайтов в Санкт-Петербурге</h1>
                 #WF_SEO_TEXT_2#
            </div>
            <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                 <!--<img src="/local/templates/prbel/img/page/seo.png" alt="Продвижение сайтов в #WF_CITY_PRED#" class="img-responsive">-->
            </div>
        </div>
        <div class="row krosh">
            <p>
 <a href="/">Главная</a> <i class="fa fa-angle-right" aria-hidden="true"></i> <a href="/services/">Услуги</a> <i class="fa fa-angle-right" aria-hidden="true"></i>
                SEO в Санкт-Петербурге
            </p>
        </div>
    </div>
</div>
<div class="direction">
    <div class="container">
        <div class="row">
            <div class="col-lg-1">
            </div>
             #WF_SEO_TEXT_3#
            <div class="col-lg-1">
            </div>
        </div>
    </div>
</div>
<div class="cta-banner">
    <div class="container">
        <div class="col-xs-12">
             Продвинули более 100 сайтов: интернет-магазины, корпоративные сайты. Чаще это B2B, сайты производителей и дилеров, торговых компаний и компаний оказывающих профессиональные услуги.
        </div>
    </div>
</div>
<div class="why-profit">
    <div class="container">
        <div class="row text-center">
            <h2>
            Что даёт поисковое SEO-продвижение </h2>
        </div>
        <div class="row">
             #WF_SEO_TEXT_4#
        </div>
    </div>
</div>
<div class="step-to-step">
    <div class="container">
         #WF_SEO_TEXT_5#
    </div>
</div>
<div class="why-we-the-one">
    <div class="container">
         #WF_SEO_TEXT_6# <!-- <div class="row">
            <div class="col-xs-12 text-center">
                <img src="/images/diplom.png" width="600px">
            </div>
        </div> -->
    </div>
</div>
 <?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    Array(
        "AREA_FILE_SHOW" => "file",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/prbel/result_slider.php"
    )
);?>

         #WF_SEO_TEXT_7#
    #WF_SEO_TEXT_9#
<div class="faq" style="padding: 75px 0 50px; background-color: #efefef;">
    <div class="container">
         #WF_SEO_TEXT_8#
    </div>
</div>
 <?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    Array(
        "AREA_FILE_SHOW" => "file",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/prbel/trust.php"
    )
);?> <?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    Array(
        "AREA_FILE_SHOW" => "file",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/prbel/contact_us.php"
    )
);?>
<div class="services context-service" style="background-color: white;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center ">
                <h2 style="margin-bottom: 25px; padding-top: 10px;">Наши услуги</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <div class="description">
                     Для наилучшего результата&nbsp;мы объединяем инструменты. Кроме SEO, мы предлагаем:
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-10 col-md-offset-1">
            <div class="row row-space ">
 <a href="/sozdanie-sajtov/">
                <div class="col-md-6 col-sm-6 col-space text-center row-space">
                    <div class="info-block alt">
                        <div class="service-title">
                             Разработка сайтов
                        </div>
                        <p>
                             Разрабатываем продающие и оптимизированные сайты для максимальной эффективности интернет-маркетинга
                        </p>
                         <!--                               <a href="#" class="more-link">Подробнее →</a>-->
                        <div class="arrow_link">
                        </div>
                    </div>
                </div>
 </a> <a href="/kontekstnaya-reklama/">
                <div class="col-md-6 col-sm-6 col-space text-center row-space">
                    <div class="info-block alt">
                        <div class="service-title">
                             Контекстная реклама
                        </div>
                        <p>
                             Запускаем эффективные рекламные кампании в Яндекс и Google, ориентированные на конверсии, ROI и целевые показатели
                        </p>
                        <div class="arrow_link">
                        </div>
                    </div>
                </div>
 </a>
                <!-- / Development --> <!-- Services --> <a href="/podderzhka-sajtov/">
                <div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
                    <div class="info-block alt">
                        <div class="service-title">
                             Техподдержка сайтов
                        </div>
                        <p>
                             Большой опыт позволяет нам осуществлять поддержку и сопровождение сайтов на Bitrix любой сложности на долгосрочной основе
                        </p>
                        <div class="arrow_link">
                        </div>
                    </div>
                </div>
 </a> <a href="/audit/">
                <div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
                    <div class="info-block alt">
                        <div class="service-title">
                             Аудит
                        </div>
                        <p>
                             Проконсультируем, какие действия предпринять, чтобы потенциальные клиенты находили вас быстрее
                        </p>
                        <div class="arrow_link">
                        </div>
                    </div>
                </div>
 </a>
            </div>
             <!-- / Services   <a href="/services/analytics/">--> <!-- / ??? -->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-offset-0 col-md-12 col-sm-12 col-space text-center blank-element">
 <noindex>
            <div href="#">
                 <a class="b24-web-form-popup-btn-15" >
                         <h2 style="padding-top: 30px;  margin-bottom: 25px;">Не нашли нужную услугу?</h2>
                         <div class="description" style="margin-bottom:0px">
                    <span style="color:#333">Опишите какую задачу нужно решить и мы найдем оптимальное решение</span>
                </div>
<button class="webform-services">
Написать</button>
</a>
            </div>
 </noindex>
        </div>
    </div>
</div>
 <br>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>