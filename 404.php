<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
$APPLICATION->SetTitle("Страница не найдена");
?>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400&amp;subset=cyrillic-ext" rel="stylesheet">
	<link rel="stylesheet" href="/local/templates/prbel/css/bootstrap.min.css">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" id="favicon_32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" id="favicon_16">
	<title>Страница не найдена</title>
	<style>
		html,body {
			background: #fc5135;
			font-family: 'Montserrat', sans-serif;
		}
		.page404 {
			padding-top: 100px;
		}
		.status-code {
			font-size: 220px;
			font-weight: bold;
			font-style: normal;
			font-stretch: normal;
			line-height: 200px;
			letter-spacing: normal;
			text-align: center;
			color: #ffffff;
		}
		.code-name {
			font-size: 38px;
			font-weight: bold;
			font-style: normal;
			font-stretch: normal;
			line-height: normal;
			letter-spacing: normal;
			text-align: center;
			color: #ffffff;
		}
		.btn-to-home {
			border-radius: 51px;
			border: solid 2px #ffffff;
			padding: 24px 53px;
			font-size: 16px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: normal;
			letter-spacing: normal;
			text-align: center;
			color: #ffffff;
			background: none;
			margin-top: 93px;
			transition: 0.5s;
			text-transform: uppercase;
		}
		.btn-to-home:hover,
		.btn-to-home:active,
		.btn-to-home:focus
		{
			outline: none;
			background: #fff;
			color: #fc5135;
		}
		.travolta {
			width: 370px;
			position: absolute;
			right: 20px;
			bottom: 0;
		}
		.copyright {
			position: absolute;
			bottom: 66px;
			left: 15%;
			font-size: 16px;
			font-weight: 300;
			font-style: normal;
			font-stretch: normal;
			line-height: normal;
			letter-spacing: normal;
			text-align: center;
			color: #ffffff;
		}
		.container {}
		.position-icon {
			position: absolute;
			left: 15%;
			bottom: 14%;
		}
		.icon img {
			width: 40px;
			outline: none !important;
			padding: 10px;
			margin-bottom: 0;
			float: left;
		}
		.icon a {
			color: #263346;
			float: left;
			border-radius: 100%;
			width: 40px;
			height: 40px;
		}
		#link_vk img {
			margin-top: 4px;
		}
		#link_vk:hover {
			background-color: #4c75a3;
		}
		#link_fc:hover {
			background-color: #3b5998;
		}
		#link_i:hover {
			/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fcc06c+0,e0396c+50,a434ad+100 */
			background: #fcc06c; /* Old browsers */
			background: -moz-linear-gradient(45deg, #fcc06c 0%, #e0396c 50%, #a434ad 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(45deg, #fcc06c 0%,#e0396c 50%,#a434ad 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(45deg, #fcc06c 0%,#e0396c 50%,#a434ad 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcc06c', endColorstr='#a434ad',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
		}
		#link_te img {
			margin-top: 2px;
		}
		#link_te:hover {
			background-color: #26a6dc;
		}
		.social-btn .last {
			margin: 0;
		}
		.social-btn .last img {
			padding-top: 14px;
		}
		#link_be:hover {
			background-color:#0056FF;
		}
	</style>
</head>
<body>
<div class="page404">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 status-code text-center">
				404
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 code-name text-center">
				Страница не найдена
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 text-center">
				<a class="btn btn-to-home" href="/">
					Перейти на главную
				</a>
			</div>

		</div>
		<div class="travolta">
			<img src="/local/templates/prbel/img/page/travolta.gif" alt="" class="img-responsive">
		</div>
		<div class="position-icon social-btn focusTextAlign">
			<div class="icon">
				<a href="https://vk.com/prbel" class="fst" rel="nofollow" target="_blank" id="link_vk">
					<img src="/markup/img/main/vk.svg" alt="">
				</a>
				<a href="https://www.facebook.com/%D0%9F%D1%80%D0%BE%D0%B4%D0%B2%D0%B8%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5-204683376565225/" class="mid" rel="nofollow" target="_blank" id="link_fc">
					<img src="/markup/img/main/facebook.svg" alt="">
				</a>
				<a href="https://www.instagram.com/prbel/" rel="nofollow" target="_blank" id="link_i">
					<img src="/markup/img/main/instagram.svg" alt="">
				</a>
				<a href="https://t.me/prbelnews" class="mid" rel="nofollow" target="_blank" id="link_te">
					<img src="/local/templates/prbel/prodaction/telegram.svg" alt="">
				</a>
				<a href="https://www.behance.net/prbel"  class="last"  rel="nofollow" target="_blank" id="link_be">
					<img src="/local/templates/prbel/prodaction/behance.svg" alt="">
				</a>
			</div>					</div>
		<div class="copyright">
			© 2012 – <?= date('Y',time()); ?>, ООО «Продвижение»
		</div>
	</div>
</div>
</body>
<script src="/bitrix/js/main/jquery/jquery-2.1.3.min.js"></script>
<script>
	$(function(){
		$(document).ready(function () {
			console.log('test');
			var windowH = $(window).height();
			var wrapperH = $('.page404').height();
			if(windowH > wrapperH) {
				$('.page404').css({'height':($(window).height())+'px'});
			}
			$(window).resize(function(){
				var windowH = $(window).height();
				var wrapperH = $('.page404').height();
				var differenceH = windowH - wrapperH;
				var newH = wrapperH + differenceH;
				var truecontentH = $('.container').height();
				if(windowH > truecontentH) {
					$('.page404').css('height', (newH)+'px');
				}

			})
		});
	});
</script>
</html>
