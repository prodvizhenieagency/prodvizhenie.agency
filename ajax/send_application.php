<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$_POST = array_map('trim', $_POST);

$el = new CIBlockElement;
$PROP = array();
$arLoadProductArray = Array(
    "IBLOCK_ID"       => 19,
    "NAME"            => 'Дата заявки ' . date('d.m.Y H:i'),
    "PROPERTY_VALUES" => array('FIO' => $_POST['name'], 'PHONE' => $_POST['phone']),
    "ACTIVE"          => "Y",            // активен
);

$el->Add($arLoadProductArray);
