</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="contact-us">
                    <div class="title">
                        Связаться с нами
                    </div>
                    <div class="office-phone">
                        <div class="item">
                            Санкт-Петербург <a href="">+7 (812) 550 5050</a>
                        </div>
                        <div class="item">
                            Москва <a href="">8 (800) 550 5050</a>
                        </div>
                        <div class="item">
                            Белгород <a href="">+7 (4722) 550 5050</a>
                        </div>
                    </div>
                    <button class="btn btn-orange">
                        Обратный звонок
                    </button>

                </div>
            </div>
            <div class="col-lg-6 col-lg-offset-2 col-md-6 col-md-offset-2 col-sm-12 col-xs-12">
                <div class="contact-info">
                    Белгород, ул. Королева, 2-а, к. 2, оф. 711<br/>
                    +7 (4722) 777-431<br/>
                    go@pr-bel.ru
                </div>
                <div class="social">
                    <a href="#">
                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15.2082 21.9925C15.9076 21.9925 15.9076 20.9518 15.9076 20.547C15.9317 20.1718 16.0868 19.8184 16.3441 19.5526C16.6014 19.2867 16.9434 19.1264 17.3065 19.1015C18.0059 19.1015 19.2019 20.3398 20.1041 21.2698C20.8035 21.9925 20.8035 21.9925 21.5029 21.9925H23.6012C23.6012 21.9925 25 21.9371 25 20.547C25 20.0965 24.5221 19.3424 22.9018 17.656C21.5029 16.2106 20.7872 16.9574 22.9018 14.0423C24.1887 12.2692 25.1259 10.6695 24.965 10.1998C24.8042 9.72997 21.2208 9.06264 20.7895 9.72274C19.3907 11.891 19.1366 12.4186 18.6913 13.3365C17.9919 14.7819 17.9243 15.5047 17.2925 15.5047C16.6607 15.5047 16.5931 14.1026 16.5931 13.3365C16.5931 10.9514 16.9288 9.2626 15.8936 9H13.7954C13.0447 9.03556 12.3183 9.28576 11.6972 9.72274C11.6972 9.72274 10.8299 10.4214 10.9977 10.4455C11.2052 10.4744 12.3966 10.1468 12.3966 11.1682V12.6137C12.3966 12.6137 12.3966 15.5047 11.6972 15.5047C10.9977 15.5047 9.59892 12.6137 8.2001 10.4455C7.65222 9.59506 7.50068 9.72274 6.80127 9.72274C6.0529 9.72274 5.40244 9.75406 4.70303 9.75406C4.00362 9.75406 3.943 10.2046 4.03159 10.4455C5.43042 14.0592 6.43291 16.3214 9.08602 19.1762C11.5223 21.7925 13.1473 21.9323 14.5088 21.9925C14.8585 22.0094 14.5088 21.9925 15.2082 21.9925Z" fill="white"/>
                        </svg>
                    </a>
                    <a href="#">
                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22.7156 7.10645L5.88578 13.6239C4.73733 14.0859 4.74367 14.7302 5.67467 15.0164L9.87367 16.3262L11.4802 21.2752C11.6744 21.8157 11.5794 22.0319 12.1452 22.0319C12.5822 22.0319 12.7786 21.8326 13.0192 21.5931L15.1177 19.5436L19.4834 22.7822C20.2878 23.2273 20.8662 22.9941 21.0668 22.034L23.9337 8.46928C24.2271 7.29084 23.484 6.75673 22.7156 7.10645ZM10.5323 16.0295L19.988 10.0356C20.4609 9.74733 20.8937 9.90205 20.5369 10.22L12.4429 17.5598L12.1283 20.9361L10.5323 16.0295Z" fill="white"/>
                        </svg>
                    </a>
                    <a href="#">
                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15 8.43822C17.1476 8.43822 17.3893 8.43822 18.2409 8.47911C18.7471 8.48136 19.2486 8.57719 19.72 8.76178C20.068 8.889 20.3807 9.09744 20.632 9.36978C20.8935 9.63018 21.1002 9.94027 21.24 10.2818C21.4122 10.7567 21.5076 11.256 21.5227 11.7609C21.5636 12.6107 21.5636 12.8542 21.5636 15.0018C21.5636 17.1493 21.5636 17.3911 21.5227 18.2427C21.5204 18.7489 21.4246 19.2504 21.24 19.7218C21.1128 20.0698 20.9043 20.3825 20.632 20.6338C20.3716 20.8952 20.0615 21.102 19.72 21.2418C19.2451 21.4139 18.7458 21.5093 18.2409 21.5244C17.3911 21.5653 17.1476 21.5653 15 21.5653C12.8524 21.5653 12.6107 21.5653 11.7591 21.5244C11.2529 21.5222 10.7514 21.4264 10.28 21.2418C9.93196 21.1146 9.6193 20.9061 9.368 20.6338C9.10653 20.3734 8.89981 20.0633 8.76 19.7218C8.58785 19.2469 8.49243 18.7476 8.47733 18.2427C8.43644 17.3929 8.43644 17.1493 8.43644 15.0018C8.43644 12.8542 8.43644 12.6124 8.47733 11.7609C8.47958 11.2546 8.57541 10.7532 8.76 10.2818C8.88722 9.93374 9.09566 9.62108 9.368 9.36978C9.6284 9.10831 9.93849 8.90158 10.28 8.76178C10.7549 8.58963 11.2542 8.49421 11.7591 8.47911C12.6089 8.43822 12.8524 8.43822 15 8.43822ZM15 7C12.8329 7 12.5502 7 11.6987 7.04089C11.0354 7.06049 10.3792 7.18345 9.75378 7.40533C9.21713 7.60781 8.73159 7.92584 8.33156 8.33689C7.92731 8.74248 7.61029 9.22648 7.4 9.75911C7.17082 10.3805 7.04937 11.0364 7.04089 11.6987C7 12.5502 7 12.8329 7 15C7 17.1671 7 17.4498 7.04089 18.3013C7.06049 18.9646 7.18345 19.6208 7.40533 20.2462C7.60781 20.7829 7.92584 21.2684 8.33689 21.6684C8.74248 22.0727 9.22648 22.3897 9.75911 22.6C10.3819 22.8316 11.0397 22.9549 11.704 22.9644C12.5502 23 12.8329 23 15 23C17.1671 23 17.4498 23 18.3013 22.9591C18.9646 22.9395 19.6208 22.8165 20.2462 22.5947C20.7829 22.3922 21.2684 22.0742 21.6684 21.6631C22.0727 21.2575 22.3897 20.7735 22.6 20.2409C22.8316 19.6181 22.9549 18.9603 22.9644 18.296C23 17.4498 23 17.1671 23 15C23 12.8329 23 12.5502 22.9591 11.6987C22.9395 11.0354 22.8165 10.3792 22.5947 9.75378C22.3922 9.21713 22.0742 8.73159 21.6631 8.33156C21.2575 7.92731 20.7735 7.61029 20.2409 7.4C19.6195 7.17082 18.9636 7.04937 18.3013 7.04089C17.4302 7 17.1671 7 15 7Z" fill="white"/>
                            <path d="M14.9999 10.8879C14.1867 10.8879 13.3917 11.1291 12.7154 11.5809C12.0392 12.0328 11.5122 12.675 11.2009 13.4263C10.8897 14.1777 10.8083 15.0045 10.967 15.8022C11.1256 16.5998 11.5172 17.3325 12.0923 17.9076C12.6674 18.4826 13.4001 18.8743 14.1977 19.0329C14.9954 19.1916 15.8222 19.1102 16.5735 18.7989C17.3249 18.4877 17.9671 17.9607 18.4189 17.2844C18.8708 16.6082 19.1119 15.8132 19.1119 14.9999C19.1115 13.9095 18.6781 12.8639 17.907 12.0928C17.136 11.3218 16.0904 10.8884 14.9999 10.8879ZM14.9999 17.6666C14.4725 17.6666 13.957 17.5102 13.5184 17.2172C13.0799 16.9242 12.7381 16.5077 12.5363 16.0204C12.3344 15.5332 12.2816 14.997 12.3845 14.4797C12.4874 13.9624 12.7414 13.4873 13.1143 13.1143C13.4873 12.7414 13.9624 12.4874 14.4797 12.3845C14.997 12.2816 15.5332 12.3344 16.0204 12.5363C16.5077 12.7381 16.9242 13.0799 17.2172 13.5184C17.5102 13.957 17.6666 14.4725 17.6666 14.9999C17.6666 15.7072 17.3857 16.3855 16.8856 16.8856C16.3855 17.3857 15.7072 17.6666 14.9999 17.6666Z" fill="white"/>
                            <path d="M19.2737 11.6774C19.799 11.6774 20.2249 11.2515 20.2249 10.7263C20.2249 10.201 19.799 9.77515 19.2737 9.77515C18.7485 9.77515 18.3226 10.201 18.3226 10.7263C18.3226 11.2515 18.7485 11.6774 19.2737 11.6774Z" fill="white"/>
                        </svg>
                    </a>
                    <a href="#">
                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M23 9.66667C23 8.26578 21.6551 7 20.1667 7H9.83333C8.34489 7 7 8.26578 7 9.66667V20.3333C7 21.7342 8.34489 23 9.83333 23H14.5V16.9556H12.4222V14.2889H14.5V13.2507C14.5 11.4587 15.9299 9.84444 17.6884 9.84444H19.9778V12.5111H17.6884C17.4372 12.5111 17.1444 12.7973 17.1444 13.2222V14.2889H19.9778V16.9556H17.1444V23H20.1667C21.6551 23 23 21.7342 23 20.3333V9.66667Z" fill="white"/>
                        </svg>
                    </a>
                    <a href="#">
                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M18.9174 9.85153H23.8545V11.0938H18.9174V9.85153ZM21.4198 14.1192C20.2604 14.1192 19.4875 14.8706 19.4102 16.0728H23.3521C23.2459 14.8606 22.6371 14.1192 21.4198 14.1192ZM21.5744 19.9798C22.3086 19.9798 23.2555 19.5691 23.4874 18.7877H25.6226C24.9656 20.8815 23.6033 21.8632 21.4971 21.8632C18.7146 21.8632 16.9852 19.9097 16.9852 17.0645C16.9852 14.3196 18.8112 12.2258 21.4971 12.2258C24.2603 12.2258 25.7868 14.4799 25.7868 17.1848C25.7868 17.3451 25.7771 17.5054 25.7674 17.6556H19.4102C19.4102 19.1183 20.1541 19.9798 21.5744 19.9798ZM8.67622 19.4789H11.5361C12.6278 19.4789 13.5167 19.0782 13.5167 17.8059C13.5167 16.5136 12.7727 16.0026 11.594 16.0026H8.67622V19.4789ZM8.67622 14.0992H11.3911C12.3476 14.0992 13.024 13.6684 13.024 12.5965C13.024 11.4344 12.1544 11.1539 11.1882 11.1539H8.67622V14.0992ZM6 9H11.739C13.8259 9 15.6326 9.61111 15.6326 12.1257C15.6326 13.3979 15.0625 14.2194 13.9708 14.7604C15.4683 15.2012 16.1929 16.3733 16.1929 17.9562C16.1929 20.5208 14.1157 21.6228 11.9032 21.6228H6V9Z" fill="white"/>
                        </svg>
                    </a>
                </div>
                <div class="bottom-menu clearfix">
                    <ul>
                        <li>
                            <a href="">
                                Компания
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Вакансии
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Работы
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Команда
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Контакты
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row" id="rebuild-copyright">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="politics">
                    <a href="#">
                        <span>Политика обработки данных</span>
                        <span class="hidden-xs">/</span>
                        <span>Политика конфиденциальности</span>
                    </a>
                </div>
            </div>
            <div class="hidden-lg hidden-sm hidden-md prbel-mobile text-center">
                <img src="<?= SITE_TEMPLATE_PATH; ?>//img/pr_mobile.svg" alt="">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                <div class="copyright">
                    <span>© 2019 ООО «Продвижение».</span>
                    <span>Все права защищены</span>
                </div>
            </div>
        </div>
        <div id="footer-mobile-built"></div>
    </div>
</footer>
<div class="overlay" style=""></div>

<div class="popup order-call" id="order-call">
	<div class="close_order"></div>
	<div class="body_pop_up">
		<div class="title uppercase">
			<p>Закажите звонок</p>
<!--			<form method="post">-->
<!--				<input type="text" name="name" class="form-input" placeholder="Ваше имя" required="">-->
<!--				<input type="text" name="phone" class="form-input maskphone" placeholder="Ваш телефон" required="">-->
<!--				<input type="submit" class="popup-btn" value="ЗАКАЗАТЬ ЗВОНОК">-->
<!--			</form>-->
<!--			<script id="bx24_form_inline" data-skip-moving="true">-->
<!--				(function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;-->
<!--						(w[b].forms=w[b].forms||[]).push(arguments[0])};-->
<!--					if(w[b]['forms']) return;-->
<!--					var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());-->
<!--					var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);-->
<!--				})(window,document,'https://prbel.bitrix24.ru/bitrix/js/crm/form_loader.js','b24form');-->
<!---->
<!--				b24form({"id":"9","lang":"ru","sec":"3ru8up","type":"inline"});-->
<!--			</script>-->
		</div>
	</div>
</div>
<div class="popup order-call" id="order-call-service">
	<div class="close_order"></div>
	<div class="body_pop_up">
		<div class="title uppercase">
			<p>Отправьте свой запрос</p>
			<form method="post">
				<input type="text" name="name" class="form-input" placeholder="Ваше имя" required="">
				<input type="text" name="phone" class="form-input maskphone" placeholder="Ваш телефон" required="">
				<textarea name="" id="" cols="30" rows="10" class="form-input" placeholder="Комментарий"></textarea>
				<input type="submit" class="popup-btn" value="ОТПРАВИТЬ ЗАПРОС">
			</form>
		</div>
	</div>
</div>
<div class="popup order-call thnx" id="thnx-popup">
	<div class="close_order"></div>
	<div class="body_pop_up">
		<div class="title uppercase">
			<p>Спасибо</p>
		</div>
		<p>Менеджер свяжется<br>
			с вами в ближайшее время</p>
	</div>
</div>
<!-- <div id="toTop">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	     x="0px" y="0px" width="50px" height="50px" viewBox="0 0 48 48" enable-background="new 0 0 48 48"
	     xml:space="preserve"> <g>
			<path style="fill:#f85831;"
			      d="M24,3.125c11.511,0,20.875,9.364,20.875,20.875S35.511,44.875,24,44.875S3.125,35.511,3.125,24S12.489,3.125,24,3.125 M24,0.125C10.814,0.125,0.125,10.814,0.125,24S10.814,47.875,24,47.875S47.875,37.186,47.875,24S37.186,0.125,24,0.125L24,0.125z"></path>
		</g>
		<path style="fill:#f85831;"
		      d="M25.5,36.033c0,0.828-0.671,1.5-1.5,1.5s-1.5-0.672-1.5-1.5V16.87l-7.028,7.061c-0.293,0.294-0.678,0.442-1.063,0.442 c-0.383,0-0.766-0.146-1.058-0.437c-0.587-0.584-0.589-1.534-0.005-2.121l9.591-9.637c0.281-0.283,0.664-0.442,1.063-0.442 c0,0,0.001,0,0.001,0c0.399,0,0.783,0.16,1.063,0.443l9.562,9.637c0.584,0.588,0.58,1.538-0.008,2.122 c-0.589,0.583-1.538,0.58-2.121-0.008l-6.994-7.049L25.5,36.033z"></path> </svg>
</div> -->


<button id="ajaxButton" type="button" class="" data-toggle="modal" data-target="#ajaxWindow" hidden>
  Launch demo modal
</button>


<div class="modal fade" id="ajaxWindow" tabindex="-1" role="dialog" aria-labelledby="ajaxWindowLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content text-center">
      <div class="modal-header">
        <div class="close_order" data-dismiss="modal" aria-label="Close"></div>
      </div>
      <div class="modal-body">
        <img src="<?= SITE_TEMPLATE_PATH; ?>/prodaction/galochka.svg" alt="result" class="img-responsive">
        <h2>Ваша заявка принята. Спасибо!</h2>
        <span>В ближайшее время мы свяжемся с вами</span>
      </div>
    </div>
  </div>
</div>

<div class="preloaderLeft transitionPreloader"></div>
<div id="brif-modal" class="mfp-hide white-popup-block question-popup">
    <form action="">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <div class="title-form">
                    Заполните бриф
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="desc">
                    Оставьте заявку, мы перезвоним вам и ответим на все вопросы.
                </div>
            </div>
            <button title="Close (Esc)" type="button" class="mfp-close">
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.713867 24.6853L5.31441 29.2858L15.06 19.54L24.7454 29.2858L29.3457 24.6853L19.6001 15.0001L29.3457 5.31484L24.7454 0.714355L15.06 10.3996L5.43531 0.714355L0.835007 5.31484L10.4597 15.0001L0.713867 24.6853Z" fill="#F3542F"/>
                </svg>
            </button>
        </div>
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="form-set">
                    <div class="title">
                        Контактные данные
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="">
                                <input type="text" placeholder="Имя" name="name">
                            </label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="">
                                <input type="text" placeholder="Компания" name="company">
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="">
                                <input type="tel" placeholder="Телефон" name="phone">
                            </label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="">
                                <input type="email" placeholder="Почта" name="email">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-set">
                    <div class="title">
                        Какое направление вас интересует?
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="fake-radio-wrap">
                                <input type="radio" name="way">
                                <div class="fake-radio">
                                    Разработка сайтов
                                    <svg width="29" height="29" viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="14.5" cy="14.5" r="14.5" fill="#C4C4C4"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M7.67922 12.103L5.27246 14.5098L12.4927 21.73L13.1804 21.0424L13.1804 21.0424L23.8389 10.3839L21.4322 7.97718L12.4928 16.9166L7.67922 12.103Z" fill="white"/>
                                    </svg>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="fake-radio-wrap">
                            <input type="radio" name="way">
                            <div class="fake-radio spoiler-service">
                                Интернет-маркетинг
                                <svg width="29" height="29" viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="14.5" cy="14.5" r="14.5" fill="#C4C4C4"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M7.67922 12.103L5.27246 14.5098L12.4927 21.73L13.1804 21.0424L13.1804 21.0424L23.8389 10.3839L21.4322 7.97718L12.4928 16.9166L7.67922 12.103Z" fill="white"/>
                                </svg>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-set hidden-set">
                    <div class="title">
                        Что именно нужно из интернет-маркетинга?
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="fake-radio-wrap">
                                <input type="radio" name="way">
                                <div class="fake-radio">
                                    SEO
                                    <svg width="29" height="29" viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="14.5" cy="14.5" r="14.5" fill="#C4C4C4"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M7.67922 12.103L5.27246 14.5098L12.4927 21.73L13.1804 21.0424L13.1804 21.0424L23.8389 10.3839L21.4322 7.97718L12.4928 16.9166L7.67922 12.103Z" fill="white"/>
                                    </svg>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="fake-radio-wrap">
                            <input type="radio" name="way">
                            <div class="fake-radio">
                                PPC
                                <svg width="29" height="29" viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="14.5" cy="14.5" r="14.5" fill="#C4C4C4"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M7.67922 12.103L5.27246 14.5098L12.4927 21.73L13.1804 21.0424L13.1804 21.0424L23.8389 10.3839L21.4322 7.97718L12.4928 16.9166L7.67922 12.103Z" fill="white"/>
                                </svg>
                            </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="fake-radio-wrap">
                            <input type="radio" name="way">
                            <div class="fake-radio">
                                Конверсии
                                <svg width="29" height="29" viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="14.5" cy="14.5" r="14.5" fill="#C4C4C4"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M7.67922 12.103L5.27246 14.5098L12.4927 21.73L13.1804 21.0424L13.1804 21.0424L23.8389 10.3839L21.4322 7.97718L12.4928 16.9166L7.67922 12.103Z" fill="white"/>
                                </svg>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="fake-radio-wrap">
                                <input type="radio" name="way">
                                <div class="fake-radio">
                                    Автоматизации
                                    <svg width="29" height="29" viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="14.5" cy="14.5" r="14.5" fill="#C4C4C4"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M7.67922 12.103L5.27246 14.5098L12.4927 21.73L13.1804 21.0424L13.1804 21.0424L23.8389 10.3839L21.4322 7.97718L12.4928 16.9166L7.67922 12.103Z" fill="white"/>
                                    </svg>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="fake-radio-wrap">
                            <input type="radio" name="way">
                            <div class="fake-radio">
                                Другое
                                <svg width="29" height="29" viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="14.5" cy="14.5" r="14.5" fill="#C4C4C4"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M7.67922 12.103L5.27246 14.5098L12.4927 21.73L13.1804 21.0424L13.1804 21.0424L23.8389 10.3839L21.4322 7.97718L12.4928 16.9166L7.67922 12.103Z" fill="white"/>
                                </svg>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-set">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="title">
                                Задача
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="text" name="task" placeholder="Описание того что нужно сделать">
                        </div>
                    </div>
                </div>
                <div class="bottom">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                            <button class="btn btn-orange glitch-btn">
                                Отправить заявку
                            </button>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 politics">
                            Нажимая кнопку «Отправить заявку», вы соглашаетесь на обработку персональных данных в соответсвии с политикой конфиденциальности.
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>


</div>
<?
//	global $USER;
//	if ($USER->IsAdmin()):
?>
		<div class="cookies-eu" >
			<span class="cookies-eu-content-holder">
				Мы используем <a rel="nofollow" href="/ispolzovanie-failov-cookie.pdf" target="_blank">cookie-файлы</a>. Продолжая пользоваться сайтом, вы соглашаетесь с этим условием
				<span class="cookies-eu-button-holder">
				<button class="cookies-eu-ok">Ok</button>
			</span></div>
<?php //endif; ?>
<!--<div class="message transition">-->
<!--	<div class="container">-->
<!--		<div class="row">-->
<!--			<div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">-->
<!--				<noindex><p>-->
<!--						Мы используем <a rel="nofollow" href="/ispolzovanie-failov-cookie.pdf" target="_blank">cookie-файлы</a>. Продолжая пользоваться сайтом, вы соглашаетесь с этим условием.-->
<!--					</p></noindex>-->
<!--			</div>-->
<!--			<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">-->
<!--				<noindex> <button id="closemessage">-->
<!--						Понятно </button></noindex>-->
<!--			</div>-->
<!--			<div class="col-sm-2 col-md-1 hidden-xs hidden-lg">-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--	<form>-->
<!--		<input type="checkbox" id="storage" onchange="funStorage()">-->
<!--	</form>-->
<!--</div>-->
<!-- / Cookie disclaimer -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/bootstrap.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/owl.carousel.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/wow.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/jquery.maskedinput.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/jquery.cookie.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/tilt.jquery.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/jquery.magnific-popup.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/jquery.waypoints.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/jquery.countup.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/script.js"></script>
<!-- Prodaction Js -->
<script src="<?= SITE_TEMPLATE_PATH; ?>/prodaction/prodaction.js"></script>


<script id="bx24_form_link" data-skip-moving="true">
	(function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
			(w[b].forms=w[b].forms||[]).push(arguments[0])};
		if(w[b]['forms']) return;
		var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
		var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
	})(window,document,'https://prbel.bitrix24.ru/bitrix/js/crm/form_loader.js','b24form');

	b24form({"id":"9","lang":"ru","sec":"3ru8up","type":"link","click":""});
</script>

<script id="bx24_form_link" data-skip-moving="true">
	(function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
			(w[b].forms=w[b].forms||[]).push(arguments[0])};
		if(w[b]['forms']) return;
		var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
		var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
	})(window,document,'https://prbel.bitrix24.ru/bitrix/js/crm/form_loader.js','b24form');

	b24form({"id":"15","lang":"ru","sec":"241nqa","type":"link","click":""});
</script>

<!-- <script data-skip-moving="true">
        (function(w,d,u){
                var s=d.createElement('script');s.async=1;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b5768447/crm/site_button/loader_3_16os19.js');
</script> -->

<script data-skip-moving="true">
        (function(w,d,u){
                var s=d.createElement('script');s.async=1;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b5768447/crm/site_button/loader_3_16os19.js');
</script>

<!--
Сеошник, разработчик, аналитик, спец в своём деле?
Приходи к нам в команду! Заполняй анкету: https://goo.gl/forms/F3GqvIhJmmCWoWhG2
-->
<?$APPLICATION->IncludeComponent(
    "webfly:meta.edit",
    "",
    Array(
    )
);?>

<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(25552757, "init", {
        id:25552757,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        trackHash:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/25552757" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=qOb7pYCHxJs2995AYnLPk0jHmWCV8t4LJwDk7LqGd8MrmcaLySVrBXuEDeoQDFgbiOB*rJLZ1hWy8NgBabRU*hCHq3/h5TdkLUnUskCl3dsUw9qkWejwoZht9wWmfXQ9t09hoQYC/M25mfPQWxofo/gOrRrryb3XvU4aZ1bt1fs-';</script>
	<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 936864218;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/936864218/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript" src="//vk.com/js/api/openapi.js?133"></script>

<?php 
$roistatVisitId = array_key_exists('roistat_visit', $_COOKIE) ? $_COOKIE['roistat_visit'] : "неизвестно";
?>

<script>(function(w, d, s, h, id) { w.roistatProjectId = id; w.roistatHost = h; var p = d.location.protocol == "https:" ? "https://" : "http://"; var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init"; var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);})(window, document, 'script', 'cloud.roistat.com', 'd3ceac81d365423805e262b3b516dce3');</script>

<?$APPLICATION->IncludeComponent(
    "webfly:meta.edit",
    "",
    Array(
    )
);?>

<script data-skip-moving="true">
$( document ).on( "b24-sitebutton-form-init", function (e, form){
    form.presets = {
        'roistatID': window.roistat.visit
        };
});
</script>

<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
var _tmr = window._tmr || (window._tmr = []);
_tmr.push({id: "3030591", type: "pageView", start: (new Date()).getTime()});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script><noscript><div>
<img src="//top-fwz1.mail.ru/counter?id=3030591;js=na" style="border:0;position:absolute;left:-9999px;" alt="" />
</div></noscript>
<!-- //Rating@Mail.ru counter -->


</body>
</html>
