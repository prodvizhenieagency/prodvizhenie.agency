<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="feedback-slider">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center ">
				<h2>Отзывы клиентов</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
                <div id="owl-demo1" class="owl-carousel owl-theme">
                    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
<!--                        <pre>-->
<!--                            --><?php //var_dump($arItem); ?>
<!--                        </pre>-->
                        <div class="shadow-block">
                            <div class="item">
<!--	                            --><?php //var_dump($arItem['PREVIEW_PICTURE']); ?>
                                <div>
                                    <div class="row">
                                        <div class="col-md-3 col-md-offset-1 col-sm-3 col-xs-12 text-center">

                                                <img src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>"
                                                     alt="feedback"
                                                     class="img-responsive">
                                            <p class="text-center">
<!--                                                <a href="--><?//= $arItem['DISPLAY_PROPERTIES']['LINK']['VALUE']; ?><!--">-->
                                                    <?= $arItem['DISPLAY_PROPERTIES']['COMPANY_NAME']['VALUE']; ?>
<!--                                                </a>-->
                                            </p>
	                                        <div class="">
		                                        <? if ($arItem['DISPLAY_PROPERTIES']['AUTHOR']['VALUE']): ?>
			                                        <p class="person"><?= $arItem['DISPLAY_PROPERTIES']['AUTHOR']['VALUE']; ?></p>
		                                        <? endif ?>
		                                        <? if ($arItem['DISPLAY_PROPERTIES']['POST']['VALUE']): ?>
			                                        <p class="person-pos"><?= $arItem['DISPLAY_PROPERTIES']['POST']['VALUE']; ?></p>
		                                        <? endif ?>
	                                        </div>
                                            <div class="link-testimonials">
                                                <? if ($arItem['PROPERTIES']['LINK_TO_PDF']['VALUE']): ?>
                                                    <a href="<?= CFile::GetPath($arItem['PROPERTIES']['LINK_TO_PDF']['VALUE']); ?>">
                                                        Смотреть отзыв
                                                    </a>
                                                <? endif ?>
                                            </div>
                                        </div>
                                        <div class="col-md-7 col-md-offset-1 col-sm-12 col-xs-12 item-pos">
                                            <?= $arItem['DISPLAY_PROPERTIES']['TEXT']['~VALUE']['TEXT']; ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
                    <? endforeach; ?>
                </div>
			</div>
		</div>
	</div>
</div>
<?// foreach ($arResult["ITEMS"] as $key => $arItem): ?>
<!--    --><?//
//    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
//    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
//    ?>
<!--    --><?// if ($key == 0): ?>
<!--        <div class="shadow-block">-->
<!--    --><?// endif; ?>
<!--    <div class="item" id="--><?//= $this->GetEditAreaId($arItem['ID']); ?><!--">-->
<!--        <div class="">-->
<!--            <div class="row">-->
<!--                <div class="col-md-8 col-md-offset-2 text-center">-->
<!--                    <h2>Отзывы клиентов</h2>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row">-->
<!--                <div class="col-md-2 col-md-offset-1 text-center">-->
<!--                    <a href=""><img src="--><?//= $arItem['DISPLAY_PROPERTIES']['LOGO']['FILE_VALUE']['SRC']; ?><!--"-->
<!--                                    alt="feedback"-->
<!--                                    class="img-responsive"></a>-->
<!--                    --><?// if ($arItem['DISPLAY_PROPERTIES']['LINK']['VALUE']): ?>
<!--                        <p class="text-center">-->
<!--                            <a href="//--><?//= $arItem['DISPLAY_PROPERTIES']['LINK']['VALUE']; ?><!--">-->
<!--                                --><?//= $arItem['DISPLAY_PROPERTIES']['LINK']['VALUE']; ?>
<!--                            </a>-->
<!--                        </p>-->
<!--                    --><?// endif ?>
<!--                </div>-->
<!--                <div class="col-md-8  col-md-offset-1 item-pos">-->
<!--                    --><?//= $arItem['DISPLAY_PROPERTIES']['TEXT']['~VALUE']['TEXT']; ?>
<!--                    --><?// if ($arItem['DISPLAY_PROPERTIES']['AUTHOR']['VALUE']): ?>
<!--                        <p class="person">--><?//= $arItem['DISPLAY_PROPERTIES']['AUTHOR']['VALUE']; ?><!--</p>-->
<!--                    --><?// endif ?>
<!--                    --><?// if ($arItem['DISPLAY_PROPERTIES']['POST']['VALUE']): ?>
<!--                        <p class="person-pos">--><?//= $arItem['DISPLAY_PROPERTIES']['POST']['VALUE']; ?><!--</p>-->
<!--                    --><?// endif ?>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    --><?// if ($key == 0): ?>
<!--        </div>-->
<!--    --><?// endif; ?>
<?// endforeach; ?>