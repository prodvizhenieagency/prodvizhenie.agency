<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="result-slider">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center ">
                <h2>Результаты наших клиентов</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="owl-demo" class="owl-carousel owl-theme">
                <? foreach ($arResult["ITEMS"] as $arItem): ?>
                    <div class="item">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 text-center">
                                <img src="<?= $arItem['DETAIL_PICTURE']['SRC'];?>" alt="result" class="img-responsive">
                            </div>
                            <div class="col-lg-5 col-md-7 col-sm-12 col-xs-12 item-pos">
                                <div class="row">
                                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12"><div class="h4"><?= $arItem['NAME']; ?></div></div>
                                    <div class="col-xs-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-10 col-md-9 col-sm-6 col-xs-9 slide-text">
	                                    <? if(!empty($arItem['PROPERTIES']['price']['VALUE'])):; ?>
		                                    <p>
			                                    <?= $arItem['PROPERTIES']['price']['NAME']; ?>:
		                                    </p>
	                                    <? endif ?>
	                                    <? if(!empty($arItem['PROPERTIES']['budget']['VALUE'])):; ?>
		                                    <p>
			                                    <?= $arItem['PROPERTIES']['budget']['NAME']; ?>:
		                                    </p>
	                                    <? endif ?>
	                                    <? if(!empty($arItem['PROPERTIES']['qnt']['VALUE'])):; ?>
		                                    <p>
			                                    <?= $arItem['PROPERTIES']['qnt']['NAME']; ?>:
		                                    </p>
	                                    <? endif ?>
	                                    <? if($arItem['PROPERTIES']['start']['VALUE'] !='' ):; ?>
		                                    <p>
			                                    <?= $arItem['PROPERTIES']['start']['NAME']; ?>
		                                    </p>
	                                    <? endif ?>
	                                    <? if(!empty($arItem['PROPERTIES']['itog']['VALUE'])):; ?>
		                                    <p>
			                                    <?= $arItem['PROPERTIES']['itog']['NAME']; ?>
		                                    </p>
	                                    <? endif ?>
	                                    <? if(!empty($arItem['PROPERTIES']['okup']['VALUE'])):; ?>
		                                    <p>
			                                    <?= $arItem['PROPERTIES']['okup']['NAME']; ?>:
		                                    </p>
	                                    <? endif ?>
                                    </div>
                                    <div class="col-lg-2 col-md-3 col-xs-3 number-slide">
	                                    <? if(!empty($arItem['PROPERTIES']['price']['VALUE'])):; ?>
                                            <p><span><?= $arItem['PROPERTIES']['price']['VALUE']; ?></span></p>
	                                    <? endif ?>
	                                    <? if(!empty($arItem['PROPERTIES']['budget']['VALUE'])):; ?>
                                            <p><span><?= $arItem['PROPERTIES']['budget']['VALUE']; ?> &#8381;</span></p>
	                                    <? endif ?>
	                                    <? if(!empty($arItem['PROPERTIES']['qnt']['VALUE'])):; ?>
		                                    <p><span><?= $arItem['PROPERTIES']['qnt']['VALUE']; ?></span></p>
	                                    <? endif ?>
	                                    <? if($arItem['PROPERTIES']['start']['VALUE'] != ''):; ?>
                                            <p><span><?= $arItem['PROPERTIES']['start']['VALUE']; ?></span></p>
	                                    <? endif ?>
	                                    <? if(!empty($arItem['PROPERTIES']['itog']['VALUE'])):; ?>
                                            <p><span><?= $arItem['PROPERTIES']['itog']['VALUE']; ?></span></p>
	                                    <? endif ?>
	                                    <? if(!empty($arItem['PROPERTIES']['okup']['VALUE'])):; ?>
		                                    <p><span><?= $arItem['PROPERTIES']['okup']['VALUE']; ?></span></p>
	                                    <? endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
                </div>
             </div>
        </div>
    </div>
</div>