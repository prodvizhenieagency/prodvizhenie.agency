<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="portfolio-page">
	<div class="container">
		<div class="row">
			
		</div>
		<?php
			$numOfCols = 3;
			$rowCount = 0;
			$bootstrapColWidth = 12 / $numOfCols;
		?>
		<div class="row">
			<?php foreach($arResult["ITEMS"] as $arItem):?>

				<div class="col-md-4 col-sm-6 text-center col-space row-sapce">
					<div class="portfolio-item text-center test">
						<a href="<?= $arItem["PROPERTIES"]["LINK_TO_CASE"]['VALUE']; ?>">
							<img src="<?= $arItem['DETAIL_PICTURE']['SRC'];?>" alt="portfolio" class="img-responsive front">
							<div class="portfolio-image-block test">
								<div class="portfolio-image-discription">
									<?php
										$name = $arItem['NAME'];
										if ($arItem['ID'] == 313) {
											$name = 'Евростоматология';
										}
									?>
									<p class="pid-title"><?= $name; ?></p>
									<?php if ($arItem["~PREVIEW_TEXT"]) { ?>
										<p class="pid-text"><?= $arItem["~PREVIEW_TEXT"]; ?></p>
									<? } else { ?>
										<p class="pid-text"><?= $arItem["~DETAIL_TEXT"]; ?></p>
									<?php } ?>

								</div>
							</div>
						</a>
					</div>
				</div>
				<?php
				$rowCount++;
				if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
				?>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<div class="news-list test2">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
