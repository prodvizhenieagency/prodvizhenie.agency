<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
	<?php
		$urlImg = '';
		if ($arResult["PROPERTIES"]['IMG_HEADER']['VALUE'] != '') {
			$urlImg = CFile::GetPath($arResult["PROPERTIES"]['IMG_HEADER']['VALUE']);
		} else {
			$urlImg = $arResult["PREVIEW_PICTURE"]['SRC'];
		}
	?>
	<div class="banner-about post-detail-banner" style="background: url('<?= $urlImg; ?>');">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<h1><? $APPLICATION->ShowTitle(false); ?></h1>
				</div>
				<div class="col-xs-12 meta-box-posts">
					<div class="post-date">
						<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
							<?echo $arResult["DISPLAY_ACTIVE_FROM"]?>
						<?endif?>
					</div>
					<div class="post-hit">
						<svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12">
							<g fill="#FFF" fill-rule="nonzero">
								<path d="M8 0C3.582 0 0 5.922 0 5.922S3.582 12 8 12s8-6.078 8-6.078S12.418 0 8 0zm0 10a4 4 0 1 1 0-8 4 4 0 0 1 0 8z"/>
								<circle cx="8" cy="6.008" r="2"/>
							</g>
						</svg>
						<?= $arResult["SHOW_COUNTER"]; ?>
					</div>
				</div>
			</div>
			<div class="row krosh">
				<p>
					<?$APPLICATION->IncludeComponent(
						"bitrix:breadcrumb",
						".default",
						array(
							"COMPONENT_TEMPLATE" => ".default",
							"START_FROM" => "0",
							"PATH" => "",
							"SITE_ID" => "s1"
						),
						false
					);?>
				</p>
			</div>
		</div>
	</div>
<div class="direction">

	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
<!--					<pre>-->
<!--						--><?php //var_dump($arResult["PROPERTIES"]['IMG_HEADER']['VALUE']); ?>
<!--					</pre>-->
				    <div class="col-xs-1"></div>
					<div class="col-xs-12 post-detail">
						<?= $arResult["~DETAIL_TEXT"]; ?>

<div id="hypercomments_widget"></div>
<script type="text/javascript">
_hcwp = window._hcwp || [];
_hcwp.push({widget:"Stream", widget_id: 100912});
(function() {
if("HC_LOAD_INIT" in window)return;
HC_LOAD_INIT = true;
var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true;
hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://w.hypercomments.com/widget/hc/100912/"+lang+"/widget.js";
var s = document.getElementsByTagName("script")[0];
s.parentNode.insertBefore(hcc, s.nextSibling);
})();
</script>
<a href="http://hypercomments.com" rel = "nofollow" class="hc-link" title="comments widget">comments powered by HyperComments</a>


					</div>

					<div class="col-xs-1"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"more-posts",
	Array(
		"ACTIVE_DATE_FORMAT" => "f j, Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("PREVIEW_PICTURE", "DETAIL_TEXT", "DETAIL_PICTURE", ""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "24",
		"IBLOCK_TYPE" => "aspro_digital_content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "3",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("", ""),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	)
);?>