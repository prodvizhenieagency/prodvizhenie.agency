<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="direction">
	<div class="container">

		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-10 col-lg-offset-2 col-md-10 col-md-offset-2 col-sm-12 col-xs-12">
						<?= $arResult["~DETAIL_TEXT"]; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="more-posts">
	<div class="row">
		<div class="container">
			<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 posts-item">
				<a href="#">
					<div class="post-img">
						<img src="/local/templates/prbel/img/page/blob.png" alt="" class="img-responsive">
					</div>
					<div class="post-title">
						20 facts about hydrogel that will make you question reality
					</div>
					<div class="post-excerpt">
						Bad news: you can't eat it. Good news: you can do just about anything else with it
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 posts-item">
				<a href="#">
					<div class="post-img">
						<img src="/local/templates/prbel/img/page/blob.png" alt="" class="img-responsive">
					</div>
					<div class="post-title">
						20 facts about hydrogel that will make you question reality
					</div>
					<div class="post-excerpt">
						Bad news: you can't eat it. Good news: you can do just about anything else with it
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 posts-item">
				<a href="#">
					<div class="post-img">
						<img src="/local/templates/prbel/img/page/blob.png" alt="" class="img-responsive">
					</div>
					<div class="post-title">
						20 facts about hydrogel that will make you question reality
					</div>
					<div class="post-excerpt">
						Bad news: you can't eat it. Good news: you can do just about anything else with it
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 posts-item">
				<a href="#">
					<div class="post-img">
						<img src="/local/templates/prbel/img/page/blob.png" alt="" class="img-responsive">
					</div>
					<div class="post-title">
						20 facts about hydrogel that will make you question reality
					</div>
					<div class="post-excerpt">
						Bad news: you can't eat it. Good news: you can do just about anything else with it
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 posts-item">
				<a href="#">
					<div class="post-img">
						<img src="/local/templates/prbel/img/page/blob.png" alt="" class="img-responsive">
					</div>
					<div class="post-title">
						20 facts about hydrogel that will make you question reality
					</div>
					<div class="post-excerpt">
						Bad news: you can't eat it. Good news: you can do just about anything else with it
					</div>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 posts-item">
				<a href="#">
					<div class="post-img">
						<img src="/local/templates/prbel/img/page/blob.png" alt="" class="img-responsive">
					</div>
					<div class="post-title">
						20 facts about hydrogel that will make you question reality
					</div>
					<div class="post-excerpt">
						Bad news: you can't eat it. Good news: you can do just about anything else with it
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="news-detail">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
	<?endif?>
</div>