<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><? $APPLICATION->ShowTitle(); ?></title><!--<title>--><?// $APPLICATION->ShowMeta('title'); ?><!--</title>-->
	<? $APPLICATION->ShowHead(); ?>
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon180x180.png" sizes="180x180" id="favicon_180">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon152x152.png" sizes="152x152" id="favicon_152">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon144x144.png" sizes="144x144" id="favicon_144">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon120x120.png" sizes="120x120" id="favicon_120">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon114x114.png" sizes="114x114" id="favicon_114">

		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon76x76.png" sizes="76x76" id="favicon_76">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon72x72.png" sizes="72x72" id="favicon_72">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon60x60.png" sizes="60x60" id="favicon_60">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon57x57.png" sizes="57x57" id="favicon_57">
		<link rel="icon" type="image/png" href="/favicons/favicon96x96.png" sizes="96x96" id="favicon_96">
		<link rel="icon" type="image/png" href="/favicons/favicon32x32.png" sizes="32x32" id="favicon_32">
		<link rel="icon" type="image/png" href="/favicons/favicon16x16.png" sizes="16x16" id="favicon_16">
		<meta name="apple-mobile-web-app-title" content="DA Продвижение">
		<meta name="application-name" content="DA Продвижение">
		<meta name="msapplication-TileColor" content="#FFFFFF">

	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/owl.carousel.min.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/owl.theme.default.min.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/animate.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/magnific-popup.css" rel="stylesheet">

	<?
	global $USER;
	if ($USER->IsAdmin()):;
		?>

<!--        <link href="--><?//= SITE_TEMPLATE_PATH; ?><!--/css/style.css" rel="stylesheet">-->
<!--        <link href="--><?//= SITE_TEMPLATE_PATH; ?><!--/prodaction/prodaction.css" rel="stylesheet">-->

        <link href="<?= SITE_TEMPLATE_PATH; ?>/css/style-test.css" rel="stylesheet">



        <link href="<?= SITE_TEMPLATE_PATH; ?>/prodaction/prodaction-test.css" rel="stylesheet">

        <link href="<?= SITE_TEMPLATE_PATH; ?>/css/redesign.css" rel="stylesheet">
	<? else : ?>
        <link href="<?= SITE_TEMPLATE_PATH; ?>/css/style-test.css" rel="stylesheet">


        <link href="<?= SITE_TEMPLATE_PATH; ?>/prodaction/prodaction-test.css" rel="stylesheet">
	<? endif; ?>

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<meta name='yandex-verification' content='703e95b6b470bc80' />
	<meta name="yandex-verification" content="3962e787208df875" />
	<meta name="google-site-verification" content="aKreIVv7U3Sjqig4R-Bzyn5hIkBo7HIVn7OIAMfOEQA" />

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5XLR6T7');</script>
</head>
<body>

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5XLR6T7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<?$APPLICATION->ShowPanel();?>
<header>
	<nav class="navbar navbar-default">
		<div class="container">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" aria-expanded="false" id="mobile-btn">
					<img src="<?= SITE_TEMPLATE_PATH; ?>/img/burger.svg" alt="">
				</button>
				<? if ($APPLICATION->GetCurPage(false) === '/'): ?>
					<a class="navbar-brand" >
<!--						<img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/ng-1.gif" alt="logo" class="gif-ny">-->
						<img src="<?= SITE_TEMPLATE_PATH; ?>/img/logo-rd.svg" alt="logo" class="desktop-logo">
						<img src="<?= SITE_TEMPLATE_PATH; ?>/img/logo-rd.svg" alt="logo" class="mobile-logo">
					</a>
				<? endif; ?>
				<? if ($APPLICATION->GetCurPage(false) !== '/'): ?>
					<a class="navbar-brand" href="/">
<!--						<img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/ng-1.gif" alt="logo" class="gif-ny">-->
						<img src="<?= SITE_TEMPLATE_PATH; ?>/img/logo-rd.svg" alt="logo" class="desktop-logo">
						<img src="<?= SITE_TEMPLATE_PATH; ?>/img/logo-rd.svg" alt="logo" class="mobile-logo">
					</a>
				<? endif; ?>

			</div>

			<div class="collapse navbar-collapse mobile-menu desktop-menu" id="bs-example-navbar-collapse-1">
				<? $APPLICATION->IncludeComponent(
					"bitrix:menu",
					"top",
					Array(
						"ALLOW_MULTI_SELECT"    => "N",
						"CHILD_MENU_TYPE"       => "left",
						"COMPONENT_TEMPLATE"    => "top",
						"COUNT_ITEM"            => "6",
						"DELAY"                 => "N",
						"MAX_LEVEL"             => "4",
						"MENU_CACHE_GET_VARS"   => array(),
						"MENU_CACHE_TIME"       => "3600000",
						"MENU_CACHE_TYPE"       => "A",
						"MENU_CACHE_USE_GROUPS" => "N",
						"ROOT_MENU_TYPE"        => "top",
						"USE_EXT"               => "Y",
					)
				); ?>

				<?
					$cities = array(
						'prodvizhenie-bel.ru'                  => 'Белгород',
						'voronezh.prodvizhenie-bel.ru'         => 'Воронеж',
						'msk.prodvizhenie-bel.ru'              => 'Москва',
						'kursk.prodvizhenie-bel.ru'            => 'Курск',
						'spb.prodvizhenie-bel.ru' => 'Санкт-Петербург',
						'krasnodar.prodvizhenie-bel.ru' => 'Краснодар',
						'tula.prodvizhenie-bel.ru' => 'Тула',
						'rostov.prodvizhenie-bel.ru' => 'Ростов-на-Дону',
						'ekaterinburg.prodvizhenie-bel.ru' => 'Екатеринбург',
					)
				?>
				<ul class="nav navbar-nav navbar-right hidden-xs hidden-sm">
					<li style="position: relative;">


                        <a href="#" class="location pop-up" data-attr="city-choice">
                            <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M10.9453 0.726563L5.9453 10.7266C5.85936 10.8985 5.68749 11 5.49999 11C5.46092 11 5.42186 10.9922 5.3828 10.9844C5.15624 10.9297 4.99999 10.7344 4.99999 10.5V6.00001H0.499978C0.265603 6.00001 0.0702907 5.84376 0.0156031 5.61719C-0.0390844 5.39063 0.0702904 5.15626 0.273416 5.05469L10.2734 0.0546875C10.3437 0.015625 10.4219 0 10.5 0C10.6328 0 10.7578 0.0468749 10.8516 0.148438C11.0078 0.296875 11.0469 0.531251 10.9453 0.726563Z" fill="white"/>
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <rect width="11" height="11" fill="white"/>
                                    </clipPath>
                                </defs>
                            </svg>

                            <?= $cities[$_SERVER['SERVER_NAME']]; ?>
                        </a>


					</li>
					<li>
						<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => "/include/prbel/header_phone.php",
							"EDIT_TEMPLATE" => ""
						),
							false
						);?>
						
<!--						<p class="call-back"><a class="b24-web-form-popup-btn-9">Заказать звонок</a></p></li>-->
				</ul>

			</div>
            <div class="mobile-close-btn">
                <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M3 15.5847L5.41528 18L10.5317 12.8835L15.6166 18L18.0317 15.5847L12.9153 10.5L18.0317 5.41525L15.6166 3L10.5317 8.08475L5.47876 3L3.0636 5.41525L8.11658 10.5L3 15.5847Z" fill="white"/>
                </svg>
            </div>
			<div class="close-bg"></div>
		</div>

	</nav>
	<div class="focusNav hidden"></div>
    <div class="popup city-choice" id="city-choice">
        <div class="close_order">
            <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect y="1.18433" width="1.67474" height="19.5386" transform="rotate(-45 0 1.18433)" fill="#FE4B34"/>
                <rect x="1.1842" y="15.0002" width="1.67474" height="19.5386" transform="rotate(-135 1.1842 15.0002)" fill="#FE4B34"/>
            </svg>
        </div>
        <div class="body_pop_up"><noindex>
                <div class="title uppercase">
                    <p>Выберите ваш город</p>
                </div>
                <ul class="cities clearfix">

                    <li>
                        <a href="https://prodvizhenie-bel.ru" class="city active">Белгород</a>
                    </li>
                    <li>
                        <a href="https://voronezh.prodvizhenie-bel.ru" class="city">Воронеж</a>
                    </li>
                    <li>
                        <a href="https://kursk.prodvizhenie-bel.ru" class="city">Курск</a>
                    </li>
                    <li>
                        <a href="https://msk.prodvizhenie-bel.ru" class="city">Москва</a>
                    </li>
                    <li>
                        <a href="https://spb.prodvizhenie-bel.ru" class="city">Санкт-Петербург</a>
                    </li>
                    <li>
                        <a href="https://krasnodar.prodvizhenie-bel.ru" class="city">Краснодар</a>
                    </li>
                    <li>
                        <a href="https://tula.prodvizhenie-bel.ru" class="city">Тула</a>
                    </li>
                    <li>
                        <a href="https://rostov.prodvizhenie-bel.ru" class="city">Ростов-на-Дону</a>
                    </li>
                    <li>
                        <a href="https://ekaterinburg.prodvizhenie-bel.ru" class="city">Екатеринбург</a>
                    </li>
                </ul>
        </div></noindex>
    </div>
</header>
<section class="main-content">