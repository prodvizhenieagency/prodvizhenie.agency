<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
<div class="accordion open-vacancy">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h2>
					Открытые вакансии </h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<ul>
					<?foreach($arResult["ITEMS"] as $arItem):?>
						<li class="accordion-item">
							<div class="title-accordion">
								<?echo $arItem["NAME"]?>
								<div class="btn-accordion">
									<svg width="24px" height="24px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<g stroke="none" stroke-width="1px" fill="none" fill-rule="evenodd" stroke-linecap="square">
											<g transform="translate(1.000000, 1.000000)" stroke="#222222">
												<path d="M0,11 L22,11"></path>
												<path d="M11,0 L11,22"></path>
											</g>
										</g>
									</svg>
								</div>
							</div>
							<div class="accordion-body">
								<?echo $arItem["~DETAIL_TEXT"];?>
							</div>
						</li>
					<? endforeach;?>
				</ul>
			</div>
		</div>
	</div>
</div>
