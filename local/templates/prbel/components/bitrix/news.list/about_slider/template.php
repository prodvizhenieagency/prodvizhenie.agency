<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><style>
	.bg-item {
	width: 100%;
    height: 260px;
    background-size: cover;
    background-repeat: no-repeat;
}
</style>
<noindex>
<div class="result-slider">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center ">
                <h2> Ивенты, которые проводит наше Digital-агентство </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="owl-about" class="owl-carousel owl-theme">
	                
		            <? foreach ($arResult["ITEMS"] as $arItem): ?>
		            
			            <div class="item">
		                    <a href="<?= $arItem['PREVIEW_PICTURE']['SRC'];?>"  data-fancybox="gallery">
			                    <div class="bg-item" style="background-image: url('<?= $arItem['PREVIEW_PICTURE']['SRC'];?>')">
			                    	
			                    </div>
		                    </a>
			            </div>
			            
		            <? endforeach; ?>
		            
                </div>
             </div>
        </div>
    </div>
</div>
</noindex>