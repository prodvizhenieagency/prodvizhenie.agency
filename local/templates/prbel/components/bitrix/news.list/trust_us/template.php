<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row visible-xs-block">
    <div class="col-xs-12 ">
        <div id="owl-demo2" class="owl-carousel owl-theme ">
            <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>

                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-8 col-xs-offset-2 text-center">
                                <a href="<?= $arItem['DISPLAY_PROPERTIES']['LINK']['VALUE']; ?>">
                                    <img src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="<?= $arItem['NAME']; ?>">
                                </a>
                            </div>
                        </div>

                    </div>
                </div>

            <? endforeach; ?>
        </div>
    </div>
</div>

<? $arResult["ITEMS"] = array_chunk($arResult["ITEMS"], 5); ?>
<? foreach ($arResult["ITEMS"] as $arItems): ?>

    <div class="row row-sapce hidden-xs wow fadeInUp">
        <? foreach ($arItems as $key => $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <? $additionClass = $key == 0 ? ' col-md-offset-1' : ''; ?>

            <div class="col-md-2 col-sm-4<?= $additionClass; ?> text-center col-space" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <a href="<?= $arItem['DISPLAY_PROPERTIES']['LINK']['VALUE']; ?>">
                    <img src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="<?= $arItem['NAME']; ?>">
                </a>
            </div>
        <? endforeach; ?>
    </div>
<? endforeach; ?>


<? if (0): ?>
    <div class="row visible-xs-block">
        <div class="col-xs-12 ">
            <div id="owl-demo2" class="owl-carousel owl-theme ">
                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-8 col-xs-offset-2 text-center">
                                <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-1.png"
                                                 alt="trast-1"></a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-8 col-xs-offset-2 text-center">
                                <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-2.png"
                                                 alt="trast-1"></a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-8 col-xs-offset-2 text-center">
                                <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-3.png"
                                                 alt="trast-1"></a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-8 col-xs-offset-2 text-center">
                                <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-4.png"
                                                 alt="trast-1"></a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-8 col-xs-offset-2 text-center">
                                <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-5.png"
                                                 alt="trast-1"></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div><!-- olw-demo -->
        </div>
    </div>
    <div class="row row-sapce hidden-xs wow fadeInUp">
        <div class="col-md-2 col-sm-4 col-md-offset-1 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-1.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-2.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-3.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-4.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-5.png" alt="trast-1"></a>
        </div>
    </div>

    <div class="row row-sapce hidden-xs wow fadeInUp">
        <div class="col-md-2 col-sm-4 col-md-offset-1 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-6.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-4.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-7.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-2.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-8.png" alt="trast-1"></a>
        </div>
    </div>

    <div class="row row-sapce hidden-xs wow fadeInUp">
        <div class="col-md-2 col-sm-4 col-md-offset-1 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-1.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-2.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-3.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-4.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-5.png" alt="trast-1"></a>
        </div>
    </div>

    <div class="row row-sapce hidden-xs wow fadeInUp">
        <div class="col-md-2 col-sm-4 col-md-offset-1 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-6.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-4.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-7.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-2.png" alt="trast-1"></a>
        </div>
        <div class="col-md-2 col-sm-4 text-center col-space">
            <a href="#"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/trast-8.png" alt="trast-1"></a>
        </div>
    </div>
<? endif ?>

