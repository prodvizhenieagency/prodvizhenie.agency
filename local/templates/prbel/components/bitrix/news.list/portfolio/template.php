<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arResult["ITEMS"] = array_chunk($arResult["ITEMS"], 3);
?>

<? foreach ($arResult["ITEMS"] as $key => $arItems): ?>
    <? $additionClass = $key == 0 ? ' row-sapce' : ''; ?>
    <div class="row<?= $additionClass; ?> wow fadeInUp">
        <? foreach ($arItems as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="col-md-4 col-sm-6 text-center col-space" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="portfolio-item text-center">
                    <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
                        <img src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="<?= $arItem['NAME']; ?>"
                             class="img-responsive front">
                        <div class="portfolio-image-block">
                            <div class="portfolio-image-discription">
                                <p class="pid-title"><?= $arItem['NAME']; ?></p>
                                <p class="pid-text">
                                    <?= strip_tags($arItem['PREVIEW_TEXT']); ?>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

        <? endforeach; ?>
    </div>
<? endforeach; ?>

<? if (0): ?>
    <div class="row row-sapce wow fadeInUp">
        <div class="col-md-4 col-sm-6 text-center col-space">
            <div class="portfolio-item text-center">
                <a href="#">
                    <img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/portfolio-1.jpg" alt="portfolio"
                         class="<?= SITE_TEMPLATE_PATH; ?>/img-responsive front">
                    <div class="portfolio-image-block">
                        <div class="portfolio-image-discription">
                            <p class="pid-title">Название проекта</p>
                            <p class="pid-text">Краткое описание проекта, <br>здесь будет написано</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 text-center col-space">
            <div class="portfolio-item text-center">
                <a href="#">
                    <img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/portfolio-2.jpg" alt="portfolio"
                         class="<?= SITE_TEMPLATE_PATH; ?>/img-responsive front">
                    <div class="portfolio-image-block">
                        <div class="portfolio-image-discription">
                            <p class="pid-title">Название проекта</p>
                            <p class="pid-text">Краткое описание проекта, <br>здесь будет написано</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-md-offset-0 col-sm-offset-3 text-center col-space">
            <div class="portfolio-item text-center">
                <a href="#">
                    <img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/portfolio-3.jpg" alt="portfolio"
                         class="<?= SITE_TEMPLATE_PATH; ?>/img-responsive front">
                    <div class="portfolio-image-block">
                        <div class="portfolio-image-discription">
                            <p class="pid-title">Название проекта</p>
                            <p class="pid-text">Краткое описание проекта, <br>здесь будет написано</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="row wow fadeInUp">
        <div class="col-md-4 col-sm-6 text-center col-space">
            <div class="portfolio-item text-center">
                <a href="#">
                    <img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/portfolio-4.jpg" alt="portfolio"
                         class="<?= SITE_TEMPLATE_PATH; ?>/img-responsive front">
                    <div class="portfolio-image-block">
                        <div class="portfolio-image-discription">
                            <p class="pid-title">Название проекта</p>
                            <p class="pid-text">Краткое описание проекта, <br>здесь будет написано</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 text-center hidden-xs col-space">
            <div class="portfolio-item text-center">
                <a href="#">
                    <img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/portfolio-5.jpg" alt="portfolio"
                         class="<?= SITE_TEMPLATE_PATH; ?>/img-responsive front">
                    <div class="portfolio-image-block">
                        <div class="portfolio-image-discription">
                            <p class="pid-title">Название проекта</p>
                            <p class="pid-text">Краткое описание проекта, <br>здесь будет написано</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 text-center col-md-offset-0 col-sm-offset-3 hidden-xs col-space">
            <div class="portfolio-item text-center">
                <a href="#">
                    <img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/portfolio-6.jpg" alt="portfolio"
                         class="<?= SITE_TEMPLATE_PATH; ?>/img-responsive front">
                    <div class="portfolio-image-block">
                        <div class="portfolio-image-discription">
                            <p class="pid-title">Название проекта</p>
                            <p class="pid-text">Краткое описание проекта, <br>здесь будет написано</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
<? endif ?>