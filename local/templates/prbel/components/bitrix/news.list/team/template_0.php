<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?php
	$numOfCols = 3;
	$rowCount = 0;
	$bootstrapColWidth = 12 / $numOfCols;
?>
<div class="row row-space">
	<?php foreach($arResult["ITEMS"] as $arItem):?>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 team-item">
			<div class="img-wrap ">
				<img
					src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
				/>
			</div>
			<div class="name">
				<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
					<?echo $arItem["NAME"]?>
				<?endif;?>
			</div>
			<div class="status">
				<?= $arItem["PROPERTIES"]["POST"]['~VALUE']; ?>
			</div>
			<div class="description">
				<?echo $arItem["PREVIEW_TEXT"];?>
			</div>
		</div>

		<?php
		$rowCount++;
		if($rowCount % $numOfCols == 0) echo '</div><div class="row row-space">';
		?>
	<?endforeach;?>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 team-item">
		<a href="/vacancy">
			<div class="img-wrap ">
				<img src="/local/templates/prbel/img/page/new.png" alt="" class="img-responsive">
			</div>
			<div class="name">
				Новый специалист
			</div>
			<div class="status">
				Свободная вакансия
			</div>
			<div class="description">
				Eva is the voice of our brand. She spends hours to make our clients feel care and enjoy communication with the company. If you have any suggestions or ideas you can write her.
			</div>
		</a>
	</div>
</div>
