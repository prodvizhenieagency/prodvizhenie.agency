<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
<div class="more-posts">

		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 hidden-sm hidden-xs"></div>
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 posts-item">
						<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
							<div class="post-img">
								<img src="<?= $arItem["PREVIEW_PICTURE"]['SRC']; ?>" alt="" class="img-responsive">
							</div>
							<div class="post-title">
								<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
									<?echo $arItem["NAME"]?>
								<?endif;?>
							</div>
						</a>
					</div>
				<? endforeach;?>
			</div>

		</div>
</div>
