<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="portfolio-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center col-space row-sapce" id="filter">
                <label data-filter="ALL">Все кейсы</label>
                <label data-filter="SEO">SEO - поисковая оптимизация</label>
                <label data-filter="DEV">Разработка сайтов</label>
                <label data-filter="PPC">Контекстная реклама </label>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

        </div>
        <?php
        $numOfCols = 3;
        $rowCount = 0;
        $bootstrapColWidth = 12 / $numOfCols;
        ?>

        <div class="row">
            <?php foreach($arResult["ITEMS"] as $arItem):?>
                <div class="col-md-4 col-sm-4 col-xs-12 text-center col-space row-sapce element" data-filter="<?=$arItem["PROPERTIES"]["FILTER"]["VALUE_XML_ID"]?>">
                    <div class="portfolio-item text-center test">
                        <a href="<?= $arItem["PROPERTIES"]["LINK_TO_CASE"]['VALUE']; ?>">
                            <img src="<?= $arItem['DETAIL_PICTURE']['SRC'];?>" alt="portfolio" class="img-responsive front">
                            <div class="portfolio-image-block test">

                            </div>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <!--
    <div class="row">
    <?php //foreach($arResult["ITEMS"] as $arItem):?>
        <div class="col-md-4 col-sm-4 col-xs-12 text-center col-space row-sapce element" data-filter="<?=$arItem["PROPERTIES"]["FILTER"]["VALUE_XML_ID"]?>">
          <div class="portfolio-item text-center test">
            <a href="<?= $arItem["PROPERTIES"]["LINK_TO_CASE"]['VALUE']; ?>">
              <img src="<?= $arItem['DETAIL_PICTURE']['SRC'];?>" alt="portfolio" class="img-responsive front">
              <div class="portfolio-image-block test">

              </div>
            </a>
          </div>
        </div>
        <?php
//        $rowCount++;
//        if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
        ?>
<?php //endforeach; ?>
    </div>
    -->
    </div>
</div>

<?
$filters=(object)[];

foreach(["ALL","PPC","DEV","SEO"] as $value){
    $Element=\CIBlockElement::GetList([],["IBLOCK_ID"=>59,"SECTION_ID"=>56,"CODE"=>$value]);
    if( $element=$Element->GetNextElement() ){
        $field=$element->GetFields();

        $property = $element->GetProperty(341);

        $h1=$property["VALUE"]?:"";

        $Seo = new \Bitrix\Iblock\InheritedProperty\ElementValues($field["IBLOCK_ID"], $field["ID"]);
        $seo = $Seo->getValues();

        $meta_title=$seo["ELEMENT_META_TITLE"];
        $meta_keywords=$seo["ELEMENT_META_KEYWORDS"];
        $meta_description=$seo["ELEMENT_META_DESCRIPTION"];

        $filters->{$value}=(object)[
            "title"=>$h1,
            "h1"=>$h1,
            "meta"=>(object)[
                "title"=>$meta_title,
                "keywords"=>$meta_keywords,
                "description"=>$meta_description,
            ]
        ];
    }
}

$is_admin=$USER->IsAdmin();
$is_admin*=1;

$filter=$_GET["filter"];

if( !in_array($filter,["ALL","PPC","DEV","SEO"]) ) $filter="ALL";
$filter=$filters->{$filter};

if( $is_admin==1 ){
    $APPLICATION->SetTitle($filter->title?:"&nbsp;");
    $APPLICATION->SetPageProperty("h1",$filter->h1?:"&nbsp;");
    $APPLICATION->SetPageProperty("title",$filter->title?:"&nbsp;");
    $APPLICATION->SetPageProperty("keywords",$filter->meta->keywords?:"&nbsp;");
    $APPLICATION->SetPageProperty("description",$filter->meta->description?:"&nbsp;");
}

$filters_json=json_encode($filters);
echo "<script>var filters={$filters_json}||[]</script>";

echo "<script>var is_admin={$is_admin}</script>";

?>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?=$field["DETAIL_TEXT"]?:""?>
        </div>
    </div>
</div>
<div class="news-list test2">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
  <?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
  <br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>

<script>

    $(document).ready(function(){
        $("#filter label").on("click",function(e,data){

            let filter=$(this).attr("data-filter");

            let filter_url="";

            if( filter=="ALL" ){
                $(".element").show();
            }else{
                filter_url="?filter="+filter;
                $(".element").hide();
                $(".element[data-filter='"+filter+"']").show();
            }

            $("#filter label").removeClass("active");
            $(this).addClass("active");

            window.history.pushState('project', 'url', location.origin+location.pathname+filter_url);

            if( is_admin==1 ){
                if( data!=true ){
                    filter=filters[filter];
                    $("h1").html(filter.h1||"&nbsp;");
                    $("title").text(filter.title||"")
                    $("meta[name='description']").attr("content",filter.meta.description||"")
                    $("meta[name='keywords']").attr("content",filter.meta.keywords||"")
                }
            }
        });

        let filter_from_url="ALL";
        let filter=location.search.split("?")[1]||"";
        filter=filter.split("=")
        if( filter[0]=="filter" && filter[1]!="" ) filter_from_url=filter[1];

        $("#filter label[data-filter='"+filter_from_url+"']").addClass("active").trigger("click",[true]);
    });
</script>


<style>
    #filter{

    }
    #filter label{
        font-size: 19px;
        cursor: pointer;
        margin-right: 15px;
        display: inline-block;
        border-bottom: 1px solid #000;
    }

    #filter label:hover,#filter label.active{
        border-color: #1b7537;
        color: #1b7537;
    }
    .element{
        /*border:1px solid #f00;*/
        height: 260px;
    }
</style>
