<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="portfolio-page">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center ">
				<h2>Портфолио</h2>
			</div>
		</div>
		<?php
			$numOfCols = 3;
			$rowCount = 0;
			$bootstrapColWidth = 12 / $numOfCols;
		?>
		<div class="row">
			<?php foreach($arResult["ITEMS"] as $arItem):?>

				<div class="col-md-4 col-sm-6 text-center col-space row-sapce">
					<div class="portfolio-item text-center">
						<a href="<?= $arItem["PROPERTIES"]["LINK_TO_CASE"]['VALUE']; ?>">
							<img src="<?= $arItem['DETAIL_PICTURE']['SRC'];?>" alt="portfolio" class="img-responsive front">
							<div class="portfolio-image-block">
								<div class="portfolio-image-discription">
									<p class="pid-title"><?= $arItem['NAME']; ?></p>
									<p class="pid-text"><?= $arItem["~DETAIL_TEXT"]; ?></p>
								</div>
							</div>
						</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
