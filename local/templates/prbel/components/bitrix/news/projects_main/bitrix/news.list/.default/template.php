<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="portfolio">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-center ">
        <h2>Портфолио</h2>
      </div>
    </div>
    <?php
      $numOfCols = 3;
      $rowCount = 0;
      $bootstrapColWidth = 12 / $numOfCols;
    ?>
    <div class="row">
      <?php foreach($arResult["ITEMS"] as $arItem):?>
<div id="count<?php echo $rowCount?>">
      <?if ($rowCount >= 9) {break;}?>
        <div class="col-md-4 col-sm-6 text-center col-space row-sapce wow fadeInLeft">
          <div class="portfolio-item text-center">
            <a href="<?= $arItem["PROPERTIES"]["LINK_TO_CASE"]['VALUE']; ?>">
              <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'];?>" alt="portfolio" class="img-responsive front">
              <div class="portfolio-image-block">
            
              </div>
            </a>
          </div>
        </div>
      </div>
        <?php
        $rowCount++;
        if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
        ?>
      <?php endforeach; ?>
    </div>
    <div class="row">
      <div class="col-xs-12 text-center">
        <a href="/projects/" class="btn my-btn-3">ПОСМОТРЕТЬ ВСЕ РАБОТЫ</a>
      </div>
    </div>
  </div>
</div>
</div>
<?/*<!--<div class="blog-tale">-->
<!--  <div class="container">-->
<!--    --><?//
//      $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
//      $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
//    ?>
<!--    --><?php
//      $numOfCols = 3;
//      $rowCount = 0;
//      $bootstrapColWidth = 12 / $numOfCols;
//    ?>
<!--    <div class="row row-space">-->
<!--      --><?php //foreach($arResult["ITEMS"] as $arItem):?>
<!--        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">-->
<!--          <a href="--><?//echo $arItem["DETAIL_PAGE_URL"]?><!--">-->
<!--            <div class="img-blog">-->
<!--              --><?//if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])){?>
<!--                  <img-->
<!--                    src="--><?//=$arItem["PREVIEW_PICTURE"]["SRC"]?><!--"-->
<!--                    width="--><?//=$arItem["PREVIEW_PICTURE"]["WIDTH"]?><!--"-->
<!--                    height="--><?//=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?><!--"-->
<!--                    alt="--><?//=$arItem["PREVIEW_PICTURE"]["ALT"]?><!--"-->
<!--                    title="--><?//=$arItem["PREVIEW_PICTURE"]["TITLE"]?><!--"-->
<!--                  />-->
<!--              --><?php //} else { ?>
<!--                <img src="http://placehold.it/360x235" alt="">-->
<!--              --><?php //} ?>
<!--            </div>-->
<!--            <div class="post-date">-->
<!--              --><?//if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
<!--                --><?//echo $arItem["DISPLAY_ACTIVE_FROM"]?>
<!--              --><?//endif?>
<!--            </div>-->
<!--            <div class="post-title">-->
<!--              --><?//if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
<!--                --><?//echo $arItem["NAME"]?>
<!--              --><?//endif;?>
<!--            </div>-->
<!--            <div class="post-excerpt">-->
<!--              --><?//echo $arItem["PREVIEW_TEXT"];?>
<!--            </div>-->
<!--          </a>-->
<!--        </div>-->
<!--        --><?php
//        $rowCount++;
//        if($rowCount % $numOfCols == 0) echo '</div><div class="row row-space">';
//        ?>
<!--      --><?//endforeach;?>
<!--  </div>-->
<!--  </div>-->
<!--</div>-->*/?>
<div class="news-list test2">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
  <?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
  <br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
