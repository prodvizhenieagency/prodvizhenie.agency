<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()
$css = $APPLICATION->GetCSSArray();
if(!is_array($css) || !in_array("/bitrix/css/main/font-awesome.css", $css))
{
	$strReturn .= '<link href="'.CUtil::GetAdditionalFileURL("/bitrix/css/main/font-awesome.css").'" type="text/css" rel="stylesheet" />'."\n";
}

/*
<div class="bx-breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">

  <div class="bx-breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
    <a href="/" title="Главная" itemprop="item">
      <span itemprop="name">Главная</span>
    </a>
  </div>

  <div class="bx-breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
    <a href="/" title="Услуги" itemprop="item">
      <span itemprop="name">Услуги</span>
    </a>
  </div>

</div>	
*/	



$strReturn .= '<p itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	$nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> ""? ' itemref="bx_breadcrumb_'.($index+1).'"' : '');
	$child = ($index > 0? ' itemprop="child"' : '');
	$arrow = ($index > 0? '<i class="fa fa-angle-right" aria-hidden="true"></i>' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= ''.$arrow.'<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<meta itemprop="position" content="'.$index.'">
							<a itemprop="item" href="'.$arResult[$index]["LINK"].'" title="'.$title.'">
								<meta itemprop="name" content="'.$title.'">
								'.$title.'
							</a>
					</span>';
	}
	else
	{
		$strReturn .= $arrow.'<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<meta itemprop="position" content="'.$index.'">
								<span>
									<meta itemprop="name" content="' . $title . '">
									<meta itemprop="item" content="https://' .$_SERVER['HTTP_HOST'].$APPLICATION->GetCurPage(). '">
								'.$title.'
								</span>
							 </span>';
	}
}

$strReturn .= '</p>';

return $strReturn;
