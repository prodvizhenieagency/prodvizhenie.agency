<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><? $this->setFrameMode(true); ?>
<? if ($arResult): ?>
    <ul class="nav navbar-nav" itemscope itemtype="http://www.schema.org/SiteNavigationElement">
        <? foreach ($arResult as $key => &$arItem): ?>
            <? if ($arItem['DEPTH_LEVEL'] == 1 && !$arItem['IS_PARENT']): ?>
                <li itemprop="name"><a href="<?= $arItem["LINK"] ?>" itemprop="url"><?= $arItem["TEXT"] ?></a></li>
            <? else: ?>
                <li class="dropdown">
                    <a href="<?= $arItem["LINK"] ?>" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-haspopup="true"
                       aria-expanded="false">
                        <?= $arItem["TEXT"] ?><i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>

                    <ul class="dropdown-menu">
                        <? while ($arResult[++$key]['DEPTH_LEVEL'] == 2): ?>
                            <li itemprop="name"><a href="<?= $arResult[$key]['LINK']; ?>" itemprop="url"><?= $arResult[$key]['TEXT']; ?></a></li>
                            <? unset($arResult[$key]); ?>
                        <? endwhile ?>
                    </ul>
                </li>
            <? endif ?>
        <? endforeach; ?>
        <li class="hidden-md hidden-lg">
            <div class="with-icon">
                <i class="fa fa-phone fa-2x" aria-hidden="true"></i>#WF_PHONES#
            </div>
        </li>
        <li class="hidden-md hidden-lg">
			<!--<a href="/contacts/" class="block-li-title">Будьте на связи</a> <br>-->
            <div class="our-contacts">
                <p><i class="fa fa-map-marker" aria-hidden="true"></i><span>#WF_CITY_TVOR#</span>
                </p>
                <p><a href="mailto:go@pr-bel.ru"><i class="fa fa-envelope-o" aria-hidden="true"></i>go@pr-bel.ru</a></p>
                <p><a href="https://www.facebook.com/prodvizheniebel/" rel="nofollow" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i>Facebook</a></p>
                <p><a href="https://vk.com/prbel" rel="nofollow" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i>Вконтакте</a></p>
                <p><a href="https://www.instagram.com/prbel/" rel="nofollow" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>Instagram</a></p>
            </div>
        </li>
    </ul>
<? endif; ?>