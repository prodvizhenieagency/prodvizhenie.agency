</section>
<footer itemscope itemtype="http://schema.org/WPFooter">
  <div class="info row-sapce">
    <div class="container">
      <div class="col-md-4 col-xs-12 no-padding ">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footerLogo">
            <img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/logo_footer.svg" alt="logo">
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 addres-title">
            <div class="h4">Контакты</div>
          </div>
          <div class="col-lg-12  col-md-12 col-md-offset-0 col-sm-12  col-xs-12 our-contacts">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
              "AREA_FILE_SHOW" => "file",
              "PATH" => "/include/prbel/footer_contact.php",
              "EDIT_TEMPLATE" => ""
            ),
              false
            );?>
          </div>
          <div class="col-xs-12 hidden-sm hidden-xs">
            <div class="h4 h3Contacts">Мы в соцсетях</div>
          </div>
          <div class="col-sm-6 position-icon social-btn hidden-sm hidden-xs">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
              "AREA_FILE_SHOW" => "file",
              "PATH" => "/include/prbel/social_button.php",
              "EDIT_TEMPLATE" => ""
            ),
              false
            );?>
          </div>
        </div>
      </div>

      <div class="col-md-7 col-md-offset-1 col-xs-12">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-4 hidden">
            <div class="row hidden-xs">
              <div class="col-xs-12">
                <div class="h4">Клиентам</div>
              </div>
            </div>
            <div class="row hidden-xs icon-right-list">
              <div class="col-md-4 col-sm-12">
                <p>
                  <img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/pozitcii_icon.svg" alt="">
                  <a href="https://topvisor.ru/?inv=54716" rel="nofollow" target="_blank">Позиции<br/> сайтов</a>
                </p>
              </div>

              <div class="col-md-4 col-sm-12">
                <p>
                  <img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/statistika_icon.svg" alt="">
                  <a href="#" rel="nofollow" target="_blank">Статистика<br/> заявок</a></p>
              </div>

              <div class="col-md-4 col-sm-12">
                <p>
                  <img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/karta.svg" alt="">
                  <a href="#" rel="nofollow" target="_blank">Оплата<br/> картой</a>
                </p>
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row lists">
              <div class="col-md-6 col-sm-6">
                <div class="h4">Компания</div>
                <?
                  $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "bottom_column_menu",
                    array(
                      "ROOT_MENU_TYPE"         =>  "bottom1",
                      "MENU_CACHE_TYPE"        =>  "A",
                      "MENU_CACHE_TIME"        =>  "3600",
                      "MENU_CACHE_USE_GROUPS"  =>  "Y",
                      "MENU_CACHE_GET_VARS"    =>  "",
                      "MAX_LEVEL"              =>  "1",
                    )
                  );
                ?>
              </div>

              <div class="col-md-6 col-sm-6 no-padding">
                <div class="h4">Услуги</div>
                <!--            --><?php
                  //              $APPLICATION->IncludeComponent(
                  //                "bitrix:menu",
                  //                "bottom_column_menu",
                  //                array(
                  //                  "ROOT_MENU_TYPE"         =>  "bottom3",
                  //                  "MENU_CACHE_TYPE"        =>  "A",
                  //                  "MENU_CACHE_TIME"        =>  "3600",
                  //                  "MENU_CACHE_USE_GROUPS"  =>  "Y",
                  //                  "MENU_CACHE_GET_VARS"    =>  "",
                  //                  "MAX_LEVEL"              =>  "1",
                  //                )
                  //              );
                  //            ?>
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <ul>
                      <li><a href="/sozdanie-sajtov/">Создание сайтов</a></li>
                      <li><a href="/seo/">SEO продвижение сайтов</a></li>
                      <li><a href="/kontekstnaya-reklama/">Контекстная реклама</a></li>
                      <li><a href="/podderzhka-sajtov/">Техподдержка сайтов</a></li>
                      <li><a href="/audit/">Digital аудит</a></li>
                      <li><a href="/reklama-na-kartah/">Реклама на картах</a></li>
                    </ul>
                  </div>

                </div>
                <div class="row mobile-position hidden-xs hidden-sm">
                  <div class="col-xs-12">
<?/*SHNG<!--                    <div class="h4 h3RssNews">Подписка на новости</div>-->
<!--                    <script src="//static-login.sendpulse.com/apps/fc3/build/loader.js" sp-form-id="65e7dd55a78cad354def36f9497e02984d7feb0c15c1809cde422dc00a962695"></script>-->
*/?>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="row hidden-md hidden-lg dublicateForm">
        <div class="col-xs-12 text-center">
<?/*SHNG<!--          <div class="h4">Подписка на новости</div>-->
<!--          <script src="//static-login.sendpulse.com/apps/fc3/build/loader.js" sp-form-id="763ebc21549bed420b42e7c0f4c0e5e39522f6aad8387e2044c2f9006313481e"></script>-->
*/?>
        </div>
      </div>
    </div>
  </div>

  <div class="copy">
    <div class="container">
      <div class="col-lg-6 col-md-6 col-sm-12 position no-padding">
        <noindex><p><a class="politika" href="/politika-v-otnoshenii-obrabotki-personalnyx-dannyx.pdf" target="_blank" rel="nofollow">Политика обработки персональных данных</a></p></noindex>
          <p>&copy; 2012 – <?= date('Y'); ?>, «Продвижение»</p>
      </div>
    </div>
  </div>
  </div>
</footer>
<div class="overlay" style=""></div>
<div class="popup city-choice" id="city-choice">
  <div class="close_order"></div>
  <div class="body_pop_up"><noindex>
    <div class="title uppercase">
      <p>Выберите ваш город</p>
    </div>
    <ul class="cities">
      <li>
        <a href="https://prodvizhenie.agency<?= $APPLICATION->GetCurPage(); ?>" class="city active">Белгород</a>
      </li>
      <li>
        <a href="https://voronezh.prodvizhenie.agency<?= $APPLICATION->GetCurPage(); ?>" class="city">Воронеж</a>
      </li>
      <!--<li>
        <a href="https://kursk.prodvizhenie.agency" class="city">Курск</a>
      </li>-->
      <li>
        <a href="https://msk.prodvizhenie.agency<?= $APPLICATION->GetCurPage(); ?>" class="city">Москва</a>
      </li>
      <li>
        <a href="https://spb.prodvizhenie.agency<?= $APPLICATION->GetCurPage(); ?>" class="city">Санкт-Петербург</a>
      </li>
      <!--<li>
        <a href="https://krasnodar.prodvizhenie.agency" class="city">Краснодар</a>
      </li>-->
      <!--<li>
        <a href="https://tula.prodvizhenie.agency" class="city">Тула</a>
      </li>-->
      <!--<li>
        <a href="https://rostov.prodvizhenie.agency" class="city">Ростов-на-Дону</a>
      </li>-->
      <!--<li>
        <a href="https://ekaterinburg.prodvizhenie.agency" class="city">Екатеринбург</a>
      </li>-->
    </ul>
  </div></noindex>
</div>
<!--<div class="popup order-call" id="order-call">
  <div class="close_order"></div>
  <div class="body_pop_up">
    <div class="title uppercase">
      <p>Закажите звонок</p>-->
<!--      <form method="post">-->
<!--        <input type="text" name="name" class="form-input" placeholder="Ваше имя" required="">-->
<!--        <input type="text" name="phone" class="form-input maskphone" placeholder="Ваш телефон" required="">-->
<!--        <input type="submit" class="popup-btn" value="ЗАКАЗАТЬ ЗВОНОК">-->
<!--      </form>-->
<!--      <script id="bx24_form_inline" data-skip-moving="true">-->
<!--        (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;-->
<!--            (w[b].forms=w[b].forms||[]).push(arguments[0])};-->
<!--          if(w[b]['forms']) return;-->
<!--          var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());-->
<!--          var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);-->
<!--        })(window,document,'https://prbel.bitrix24.ru/bitrix/js/crm/form_loader.js','b24form');-->
<!---->
<!--        b24form({"id":"9","lang":"ru","sec":"3ru8up","type":"inline"});-->
<!--      </script>-->
<!--    </div>
  </div>
</div>-->
<!--<div class="popup order-call" id="order-call-service">
  <div class="close_order"></div>
  <div class="body_pop_up">
    <div class="title uppercase">
      <p>Отправьте свой запрос</p>
      <form method="post">
        <input type="text" name="name" class="form-input" placeholder="Ваше имя" required="">
        <input type="text" name="phone" class="form-input maskphone" placeholder="Ваш телефон" required="">
        <textarea name="" id="" cols="30" rows="10" class="form-input" placeholder="Комментарий"></textarea>
        <input type="submit" class="popup-btn" value="ОТПРАВИТЬ ЗАПРОС">
      </form>
    </div>
  </div>
</div>
<div class="popup order-call thnx" id="thnx-popup">
  <div class="close_order"></div>
  <div class="body_pop_up">
    <div class="title uppercase">
      <p>Спасибо</p>
    </div>
    <p>Менеджер свяжется<br>
      с вами в ближайшее время</p>
  </div>
</div>-->
<!-- <div id="toTop">
  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
       x="0px" y="0px" width="50px" height="50px" viewBox="0 0 48 48" enable-background="new 0 0 48 48"
       xml:space="preserve"> <g>
      <path style="fill:#f85831;"
            d="M24,3.125c11.511,0,20.875,9.364,20.875,20.875S35.511,44.875,24,44.875S3.125,35.511,3.125,24S12.489,3.125,24,3.125 M24,0.125C10.814,0.125,0.125,10.814,0.125,24S10.814,47.875,24,47.875S47.875,37.186,47.875,24S37.186,0.125,24,0.125L24,0.125z"></path>
    </g>
    <path style="fill:#f85831;"
          d="M25.5,36.033c0,0.828-0.671,1.5-1.5,1.5s-1.5-0.672-1.5-1.5V16.87l-7.028,7.061c-0.293,0.294-0.678,0.442-1.063,0.442 c-0.383,0-0.766-0.146-1.058-0.437c-0.587-0.584-0.589-1.534-0.005-2.121l9.591-9.637c0.281-0.283,0.664-0.442,1.063-0.442 c0,0,0.001,0,0.001,0c0.399,0,0.783,0.16,1.063,0.443l9.562,9.637c0.584,0.588,0.58,1.538-0.008,2.122 c-0.589,0.583-1.538,0.58-2.121-0.008l-6.994-7.049L25.5,36.033z"></path> </svg>
</div> -->


<!--<button id="ajaxButton" type="button" class="" data-toggle="modal" data-target="#ajaxWindow" hidden>
  Launch demo modal
</button>


<div class="modal fade" id="ajaxWindow" tabindex="-1" role="dialog" aria-labelledby="ajaxWindowLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content text-center">
      <div class="modal-header">
        <div class="close_order" data-dismiss="modal" aria-label="Close"></div>
      </div>
      <div class="modal-body">
        <img src="<?= SITE_TEMPLATE_PATH; ?>/prodaction/galochka.svg" alt="result" class="img-responsive">
        <h2>Ваша заявка принята. Спасибо!</h2>
        <span>В ближайшее время мы свяжемся с вами</span>
      </div>
    </div>
  </div>
</div>

<div class="preloaderLeft transitionPreloader"></div>
-->
<?
//  global $USER;
//  if ($USER->IsAdmin()):
?>
    <div class="cookies-eu" >
      <span class="cookies-eu-content-holder">
        Мы используем <a rel="nofollow" href="/ispolzovanie-failov-cookie.pdf" target="_blank">cookie</a>. Продолжая пользоваться сайтом, вы соглашаетесь с этим
        <span class="cookies-eu-button-holder">
        <button class="cookies-eu-ok">Ok</button>
      </span></div>
<?php //endif; ?>
<!--<div class="message transition">-->
<!--  <div class="container">-->
<!--    <div class="row">-->
<!--      <div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">-->
<!--        <noindex><p>-->
<!--            Мы используем <a rel="nofollow" href="/ispolzovanie-failov-cookie.pdf" target="_blank">cookie-файлы</a>. Продолжая пользоваться сайтом, вы соглашаетесь с этим условием.-->
<!--          </p></noindex>-->
<!--      </div>-->
<!--      <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">-->
<!--        <noindex> <button id="closemessage">-->
<!--            Понятно </button></noindex>-->
<!--      </div>-->
<!--      <div class="col-sm-2 col-md-1 hidden-xs hidden-lg">-->
<!--      </div>-->
<!--    </div>-->
<!--  </div>-->
<!--  <form>-->
<!--    <input type="checkbox" id="storage" onchange="funStorage()">-->
<!--  </form>-->
<!--</div>-->
<!-- / Cookie disclaimer -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>	
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/bootstrap.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/owl.carousel.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/wow.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/jquery.maskedinput.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/jquery.cookie.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/tilt.jquery.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/jquery.magnific-popup.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/jquery.fancybox.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/slick.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/script.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/chart.bundle.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/custom.js"></script>



<!-- Prodaction Js -->
<script src="<?= SITE_TEMPLATE_PATH; ?>/prodaction/prodaction.js"></script>


<script id="bx24_form_link" data-skip-moving="true">
  (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
      (w[b].forms=w[b].forms||[]).push(arguments[0])};
    if(w[b]['forms']) return;
    var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
  })(window,document,'https://prbel.bitrix24.ru/bitrix/js/crm/form_loader.js','b24form');

  b24form({"id":"9","lang":"ru","sec":"3ru8up","type":"link","click":""});
</script>

<script id="bx24_form_link" data-skip-moving="true">
  (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
      (w[b].forms=w[b].forms||[]).push(arguments[0])};
    if(w[b]['forms']) return;
    var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
  })(window,document,'https://prbel.bitrix24.ru/bitrix/js/crm/form_loader.js','b24form');

  b24form({"id":"15","lang":"ru","sec":"241nqa","type":"link","click":""});
</script>

<!-- <script data-skip-moving="true">
        (function(w,d,u){
                var s=d.createElement('script');s.async=1;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b5768447/crm/site_button/loader_3_16os19.js');
</script> -->

<script data-skip-moving="true">
        (function(w,d,u){
                var s=d.createElement('script');s.async=1;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b5768447/crm/site_button/loader_3_16os19.js');
</script>

<!--
Сеошник, разработчик, аналитик, спец в своём деле?
Приходи к нам в команду! Заполняй анкету: https://goo.gl/forms/F3GqvIhJmmCWoWhG2
-->
<?/*$APPLICATION->IncludeComponent(
    "webfly:meta.edit",
    "",
    Array(
    )
);*/?>

<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(25552757, "init", {
        id:25552757,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        trackHash:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/25552757" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=qOb7pYCHxJs2995AYnLPk0jHmWCV8t4LJwDk7LqGd8MrmcaLySVrBXuEDeoQDFgbiOB*rJLZ1hWy8NgBabRU*hCHq3/h5TdkLUnUskCl3dsUw9qkWejwoZht9wWmfXQ9t09hoQYC/M25mfPQWxofo/gOrRrryb3XvU4aZ1bt1fs-';</script>
  <script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 936864218;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/936864218/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript" src="//vk.com/js/api/openapi.js?133"></script>


<?$APPLICATION->IncludeComponent(
    "webfly:meta.edit",
    "",
    Array(
    )
);?>



<script type="text/javascript">
var _tmr = window._tmr || (window._tmr = []);
_tmr.push({id: "3030591", type: "pageView", start: (new Date()).getTime()});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script><noscript><div>
<img src="//top-fwz1.mail.ru/counter?id=3030591;js=na" style="border:0;position:absolute;left:-9999px;" alt="" />
</div></noscript>

<!-- calltouch
<script type="text/javascript">
/*
(function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]
["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]
["callbacks"]=[]}w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]
["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var
a=d.getElementsByTagName("script")
[0],s=d.createElement("script"),i=function()
{a.parentNode.insertBefore(s,a)};s.type="text/
javascript";s.async=true;s.src="https://mod.calltouch.ru/init.js?
id="+cId;if(w.opera=="[object Opera]")
{d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})
(window,document,"ct","2g0x5q23");
*/
</script>

<script type="text/javascript">
/* Вешаем обработчик по событию submit формы с идентификатором myform */
jQuery(document).on('submit', 'form#myform', function() {
/* Записываем в переменную form объект с формой, на которой сработало событие submit */
var form = jQuery(this);
/* Ищем на форме input для ввода номера телефона и получаем его значение, в примере поиск производится по наличию у input атрибута name со значением phone */
var phone = form.find('input[name="LEAD_PHONE"]').val();
/* Аналогично собираем другие введенные в поля формы данные, например, ФИО и email */
var fio = form.find('input[name="LEAD_NAME"]').val();
var mail = form.find('input[name="LEAD_EMAIL"]').val();
/* Указываем название формы */
var sub = 'Заявка на звонок';
/* Указываем ID сайта внутри Calltouch и адрес API-сервера */
var ct_node_id = '14';
var ct_site_id = '37863';
/* Формируем массив входных параметров запроса */
var ct_data = {
fio: fio,
phoneNumber: phone,
email: mail,
subject: sub,
sessionId: window.call_value
};
/* При необходимости делаем проверку на корректность собранных с формы данных */
/* Например, обязательным для заполнения на форме является поле с телефоном, проверяем его наличие и не пустое ли оно */
if (typeof(phone)!='undefined' && phone!=''){
/* Выполняем AJAX-запрос */
jQuery.ajax({
  url: 'https://api-node'+ct_node_id+'.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
  dataType: 'json',
  type: 'POST',
  data: ct_data,
  async: false /* Предположим, после отправки формы на сайте настроен редирект на другую страницу, поэтому используем параметр async: false для синхронной отправки запроса */
});
}
});
</script>
calltouch -->
<script>
  function Rem(){
    $(".sp-form-outer").remove();
    $(".sp-form-outer").remove();
    $(".sp-form-outer").remove();
  }
  $(document).ready(function(){
    setTimeout("Rem()", 1000);
    setTimeout("Rem()", 2000);
    setTimeout("Rem()", 3500);
    setTimeout("Rem()", 7000);
  });
</script>


<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn-ru.bitrix24.ru/b5768447/crm/site_button/loader_1_5ymeoj.js');
</script>


<style type="text/css">
  .sp-form-outer {display:none !important;}


.b24-form-state-container .b24-form-success {
    background-color: #fff ;
}
.b24-form-success-icon {
    background-image: url(data:image/svg+xml;charset=US-ASCII,%3Csvg%20viewBox%3D%220%200%20169%20169%22%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20xmlns%3Axlink%3D%22http%3A//www.w3.org/1999/xlink%22%3E%3Cdefs%3E%3Ccircle%20id%3D%22a%22%20cx%3D%2284.5%22%20cy%3D%2284.5%22%20r%3D%2265.5%22/%3E%3Cfilter%20x%3D%22-.8%25%22%20y%3D%22-.8%25%22%20width%3D%22101.5%25%22%20height%3D%22101.5%25%22%20filterUnits%3D%22objectBoundingBox%22%20id%3D%22b%22%3E%3CfeGaussianBlur%20stdDeviation%3D%22.5%22%20in%3D%22SourceAlpha%22%20result%3D%22shadowBlurInner1%22/%3E%3CfeOffset%20dx%3D%22-1%22%20dy%3D%22-1%22%20in%3D%22shadowBlurInner1%22%20result%3D%22shadowOffsetInner1%22/%3E%3CfeComposite%20in%3D%22shadowOffsetInner1%22%20in2%3D%22SourceAlpha%22%20operator%3D%22arithmetic%22%20k2%3D%22-1%22%20k3%3D%221%22%20result%3D%22shadowInnerInner1%22/%3E%3CfeColorMatrix%20values%3D%220%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200.0886691434%200%22%20in%3D%22shadowInnerInner1%22%20result%3D%22shadowMatrixInner1%22/%3E%3CfeGaussianBlur%20stdDeviation%3D%22.5%22%20in%3D%22SourceAlpha%22%20result%3D%22shadowBlurInner2%22/%3E%3CfeOffset%20dx%3D%221%22%20dy%3D%221%22%20in%3D%22shadowBlurInner2%22%20result%3D%22shadowOffsetInner2%22/%3E%3CfeComposite%20in%3D%22shadowOffsetInner2%22%20in2%3D%22SourceAlpha%22%20operator%3D%22arithmetic%22%20k2%3D%22-1%22%20k3%3D%221%22%20result%3D%22shadowInnerInner2%22/%3E%3CfeColorMatrix%20values%3D%220%200%200%200%201%200%200%200%200%201%200%200%200%200%201%200%200%200%200.292285839%200%22%20in%3D%22shadowInnerInner2%22%20result%3D%22shadowMatrixInner2%22/%3E%3CfeMerge%3E%3CfeMergeNode%20in%3D%22shadowMatrixInner1%22/%3E%3CfeMergeNode%20in%3D%22shadowMatrixInner2%22/%3E%3C/feMerge%3E%3C/filter%3E%3C/defs%3E%3Cg%20fill%3D%22none%22%20fill-rule%3D%22evenodd%22%3E%3Ccircle%20stroke-opacity%3D%22.05%22%20stroke%3D%22%23000%22%20fill-opacity%3D%22.07%22%20fill%3D%22%23000%22%20cx%3D%2284.5%22%20cy%3D%2284.5%22%20r%3D%2284%22/%3E%3Cuse%20fill%3D%22%23FFF%22%20xlink%3Ahref%3D%22%23a%22/%3E%3Cuse%20fill%3D%22%23000%22%20filter%3D%22url%28%23b%29%22%20xlink%3Ahref%3D%22%23a%22/%3E%3Cpath%20fill%3D%22%23f85831%22%20d%3D%22M76.853%20107L57%2087.651l6.949-6.771%2012.904%2012.576L106.051%2065%20113%2071.772z%22/%3E%3C/g%3E%3C/svg%3E) ;
}
.b24-window-panel-pos-right {
    right: calc(50% - 255px);
}
.b24-window-scrollable {
    overflow-y: hidden;
}
.b24-form-state-container .b24-form-success {
    display: flex;
    align-items: center;
    height: 100%;
}

.b24-window-panel {
    height: 700px;
    max-height: 700px;
    margin-top: 5%;
}


@media  (max-width: 510px){

.b24-window-panel-pos-right {
    right: 0;
}

}





</style>






<script>

$(document).ready(function () {           
  $('.crm-webform-submit-button').click(function () {         
    yaCounter25552757.reachGoal("1");   
  });     

});     


</script>



</body>
</html>