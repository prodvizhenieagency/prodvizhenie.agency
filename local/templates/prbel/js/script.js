$(function(){
	$('.link-testimonials a').magnificPopup({
		type: 'image'

	});
	// alert($(window).width());
	new WOW({
		offset: 100
	}).init();	
	
	$(document).ready(function(){
		$('.cookies-eu-ok').click(function (e) {
			$.cookie('cookie_ok', 'true',{ path: '/',expires: 30});
			$('.cookies-eu').hide();
			console.log('click');
		});
		if ($.cookie('cookie_ok') == 'true') {
			$('.cookies-eu').hide();
		} else {
			$('.cookies-eu').show();
		}
		setTimeout(function () {
			$.cookie('cookie_ok', 'true',{ path: '/',expires: 30});
			$('.cookies-eu').hide();
		},5000);
		$(".portfolio-item a").hover(function(){
			console.log('test');
			var $this = $(this);
			$this.next(".portfolio-image-block").addClass("fadeInDown", 1000);
		});
		var flag = false;
		$('#mobile-btn').click(function (e) {
			e.preventDefault();
			if (flag == false) {
				$('.mobile-menu').animate({
						'left': 0
					},{
						duration: 100
					}
				);
				$('.close-bg').css('display','block');
				flag =  true;
			} else {
				$('.mobile-menu').animate({
						'left': '-100%'
					},{
						duration: 100
					}
				);
				$('.close-bg').css('display','none');
				flag =  false;
			}
		});
		$('.close-bg').click(function (e) {
			$('.mobile-menu').animate({
					'left': '-100%'
				},{
					duration: 100
				}
			);
			$('.close-bg').css('display','none');
			flag =  false;
		});
		$("#owl-demo").owlCarousel({
		    slideSpeed : 300,
		    paginationSpeed : 400,
		 
		    items:1,
		    loop: true,
		    stagePadding:30,
    		smartSpeed:450,
    		itemsDesktop : false,
		    itemsDesktopSmall : false,
		    itemsTablet: false,
		    itemsMobile : false,
		    margin:30,
		    nav:true,
		    navText : ['<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40; width: 40px; border-radius: 50%" xml:space="preserve"> <style type="text/css"> .st0{fill:#FFFFFF;} </style> <g> <g><path class="st0" d="M20,40L20,40c-5.3,0-10.4-2.1-14.2-5.9c-7.8-7.8-7.8-20.5,0-28.3C9.6,2.1,14.7,0,20,0 c5.3,0,10.4,2.1,14.1,5.9C37.9,9.6,40,14.7,40,20s-2.1,10.4-5.9,14.1C30.4,37.9,25.3,40,20,40L20,40z M20,3.4 c-4.4,0-8.6,1.7-11.7,4.9c-6.5,6.5-6.5,17,0,23.5c3.1,3.1,7.3,4.9,11.7,4.9h0c4.4,0,8.6-1.7,11.7-4.9c3.1-3.1,4.9-7.3,4.9-11.7 s-1.7-8.6-4.9-11.7C28.6,5.1,24.4,3.4,20,3.4L20,3.4z"/></g><g><path class="st0" d="M22.4,29.9c-0.4,0-0.9-0.2-1.2-0.5L13,21.2c-0.3-0.3-0.5-0.8-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l8.2-8.2 c0.7-0.7,1.7-0.7,2.4,0c0.7,0.7,0.7,1.7,0,2.4l-7,7l7,7c0.7,0.7,0.7,1.7,0,2.4C23.3,29.8,22.8,29.9,22.4,29.9L22.4,29.9z"/></g></g></svg>'
,'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40; width: 40px; border-radius: 50%" xml:space="preserve"><style type="text/css">.st0{fill:#FFFFFF;}</style><g><g><path class="st0" d="M20,0L20,0c5.3,0,10.4,2.1,14.2,5.9c7.8,7.8,7.8,20.5,0,28.3C30.4,37.9,25.3,40,20,40 c-5.3,0-10.4-2.1-14.1-5.9C2.1,30.4,0,25.3,0,20S2.1,9.6,5.9,5.9C9.6,2.1,14.7,0,20,0L20,0z M20,36.6c4.4,0,8.6-1.7,11.7-4.9 c6.5-6.5,6.5-17,0-23.5C28.6,5.1,24.4,3.4,20,3.4h0c-4.4,0-8.6,1.7-11.7,4.9C5.1,11.4,3.4,15.6,3.4,20s1.7,8.6,4.9,11.7 C11.4,34.9,15.6,36.6,20,36.6L20,36.6z"/> </g><g><path class="st0" d="M17.6,10.1c0.4,0,0.9,0.2,1.2,0.5l8.2,8.2c0.3,0.3,0.5,0.8,0.5,1.2c0,0.5-0.2,0.9-0.5,1.2l-8.2,8.2 c-0.7,0.7-1.7,0.7-2.4,0c-0.7-0.7-0.7-1.7,0-2.4l7-7l-7-7c-0.7-0.7-0.7-1.7,0-2.4C16.7,10.2,17.2,10.1,17.6,10.1L17.6,10.1z"/></g></g></svg>']
		 
		});
		var fixOwl = function(){
			var $stage = $('.owl-stage'),
				stageW = $stage.width(),
				$el = $('.owl-item'),
				elW = 0;
			$el.each(function() {
				elW += $(this).width()+ +($(this).css("margin-right").slice(0, -2))
			});
			if ( elW > stageW ) {
				$stage.width(elW);
			};
		};
		
		$("#owl-about").owlCarousel({
		    slideSpeed : 300,
		    paginationSpeed : 400,
		    loop: true,
		    stagePadding:30,
    		smartSpeed:450,
    		itemsDesktop : false,
		    itemsDesktopSmall : false,
		    itemsTablet: false,
		    itemsMobile : false,
		    margin:30,
		    nav:true,
		    navText : ['<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40; width: 40px; border-radius: 50%" xml:space="preserve"> <style type="text/css"> .st0{fill:#FFFFFF;} </style> <g> <g><path class="st0" d="M20,40L20,40c-5.3,0-10.4-2.1-14.2-5.9c-7.8-7.8-7.8-20.5,0-28.3C9.6,2.1,14.7,0,20,0 c5.3,0,10.4,2.1,14.1,5.9C37.9,9.6,40,14.7,40,20s-2.1,10.4-5.9,14.1C30.4,37.9,25.3,40,20,40L20,40z M20,3.4 c-4.4,0-8.6,1.7-11.7,4.9c-6.5,6.5-6.5,17,0,23.5c3.1,3.1,7.3,4.9,11.7,4.9h0c4.4,0,8.6-1.7,11.7-4.9c3.1-3.1,4.9-7.3,4.9-11.7 s-1.7-8.6-4.9-11.7C28.6,5.1,24.4,3.4,20,3.4L20,3.4z"/></g><g><path class="st0" d="M22.4,29.9c-0.4,0-0.9-0.2-1.2-0.5L13,21.2c-0.3-0.3-0.5-0.8-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l8.2-8.2 c0.7-0.7,1.7-0.7,2.4,0c0.7,0.7,0.7,1.7,0,2.4l-7,7l7,7c0.7,0.7,0.7,1.7,0,2.4C23.3,29.8,22.8,29.9,22.4,29.9L22.4,29.9z"/></g></g></svg>'
,'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40; width: 40px; border-radius: 50%" xml:space="preserve"><style type="text/css">.st0{fill:#FFFFFF;}</style><g><g><path class="st0" d="M20,0L20,0c5.3,0,10.4,2.1,14.2,5.9c7.8,7.8,7.8,20.5,0,28.3C30.4,37.9,25.3,40,20,40 c-5.3,0-10.4-2.1-14.1-5.9C2.1,30.4,0,25.3,0,20S2.1,9.6,5.9,5.9C9.6,2.1,14.7,0,20,0L20,0z M20,36.6c4.4,0,8.6-1.7,11.7-4.9 c6.5-6.5,6.5-17,0-23.5C28.6,5.1,24.4,3.4,20,3.4h0c-4.4,0-8.6,1.7-11.7,4.9C5.1,11.4,3.4,15.6,3.4,20s1.7,8.6,4.9,11.7 C11.4,34.9,15.6,36.6,20,36.6L20,36.6z"/> </g><g><path class="st0" d="M17.6,10.1c0.4,0,0.9,0.2,1.2,0.5l8.2,8.2c0.3,0.3,0.5,0.8,0.5,1.2c0,0.5-0.2,0.9-0.5,1.2l-8.2,8.2 c-0.7,0.7-1.7,0.7-2.4,0c-0.7-0.7-0.7-1.7,0-2.4l7-7l-7-7c-0.7-0.7-0.7-1.7,0-2.4C16.7,10.2,17.2,10.1,17.6,10.1L17.6,10.1z"/></g></g></svg>'],
			responsive:{
					0:{
						items:1
					},
					600:{
						items:3
					},
					1000:{
						items:3
					}
				}
		 
		});

		$("#banner-main").owlCarousel({
			slideSpeed : 300,
			paginationSpeed : 400,
			items:1,
			singleItem:true,
			loop: false,
			center:true,
			// stagePadding:30,
			smartSpeed:450,
			margin:0,
			nav:true,
			navText : ['<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40; width: 40px; border-radius: 50%" xml:space="preserve"> <style type="text/css"> .st0{fill:#FFFFFF;} </style> <g> <g><path class="st0" d="M20,40L20,40c-5.3,0-10.4-2.1-14.2-5.9c-7.8-7.8-7.8-20.5,0-28.3C9.6,2.1,14.7,0,20,0 c5.3,0,10.4,2.1,14.1,5.9C37.9,9.6,40,14.7,40,20s-2.1,10.4-5.9,14.1C30.4,37.9,25.3,40,20,40L20,40z M20,3.4 c-4.4,0-8.6,1.7-11.7,4.9c-6.5,6.5-6.5,17,0,23.5c3.1,3.1,7.3,4.9,11.7,4.9h0c4.4,0,8.6-1.7,11.7-4.9c3.1-3.1,4.9-7.3,4.9-11.7 s-1.7-8.6-4.9-11.7C28.6,5.1,24.4,3.4,20,3.4L20,3.4z"/></g><g><path class="st0" d="M22.4,29.9c-0.4,0-0.9-0.2-1.2-0.5L13,21.2c-0.3-0.3-0.5-0.8-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l8.2-8.2 c0.7-0.7,1.7-0.7,2.4,0c0.7,0.7,0.7,1.7,0,2.4l-7,7l7,7c0.7,0.7,0.7,1.7,0,2.4C23.3,29.8,22.8,29.9,22.4,29.9L22.4,29.9z"/></g></g></svg>'
				,'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40; width: 40px; border-radius: 50%" xml:space="preserve"><style type="text/css">.st0{fill:#FFFFFF;}</style><g><g><path class="st0" d="M20,0L20,0c5.3,0,10.4,2.1,14.2,5.9c7.8,7.8,7.8,20.5,0,28.3C30.4,37.9,25.3,40,20,40 c-5.3,0-10.4-2.1-14.1-5.9C2.1,30.4,0,25.3,0,20S2.1,9.6,5.9,5.9C9.6,2.1,14.7,0,20,0L20,0z M20,36.6c4.4,0,8.6-1.7,11.7-4.9 c6.5-6.5,6.5-17,0-23.5C28.6,5.1,24.4,3.4,20,3.4h0c-4.4,0-8.6,1.7-11.7,4.9C5.1,11.4,3.4,15.6,3.4,20s1.7,8.6,4.9,11.7 C11.4,34.9,15.6,36.6,20,36.6L20,36.6z"/> </g><g><path class="st0" d="M17.6,10.1c0.4,0,0.9,0.2,1.2,0.5l8.2,8.2c0.3,0.3,0.5,0.8,0.5,1.2c0,0.5-0.2,0.9-0.5,1.2l-8.2,8.2 c-0.7,0.7-1.7,0.7-2.4,0c-0.7-0.7-0.7-1.7,0-2.4l7-7l-7-7c-0.7-0.7-0.7-1.7,0-2.4C16.7,10.2,17.2,10.1,17.6,10.1L17.6,10.1z"/></g></g></svg>'],
			onInitialized: fixOwl,
			onRefreshed: fixOwl

		});
		$("#slider-charts").owlCarousel({
			slideSpeed : 300,
			paginationSpeed : 400,
			items:1,
			singleItem:true,
			loop: false,
			center:true,
			// stagePadding:30,
			smartSpeed:450,
			margin:0,
			dots: true,
			onInitialized: fixOwl,
			onRefreshed: fixOwl,
			nav: true,
navText : ['<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40; width: 40px; border-radius: 50%" xml:space="preserve"> <style type="text/css"> .st0{fill:#5f2d91;} </style> <g> <g><path class="st0" d="M22.4,29.9c-0.4,0-0.9-0.2-1.2-0.5L13,21.2c-0.3-0.3-0.5-0.8-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l8.2-8.2 c0.7-0.7,1.7-0.7,2.4,0c0.7,0.7,0.7,1.7,0,2.4l-7,7l7,7c0.7,0.7,0.7,1.7,0,2.4C23.3,29.8,22.8,29.9,22.4,29.9L22.4,29.9z"/></g></g></svg>'
				,
'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40; width: 40px; border-radius: 50%" xml:space="preserve"><style type="text/css">.st0{fill:#5f2d91;}</style><g><<g><path class="st0" d="M17.6,10.1c0.4,0,0.9,0.2,1.2,0.5l8.2,8.2c0.3,0.3,0.5,0.8,0.5,1.2c0,0.5-0.2,0.9-0.5,1.2l-8.2,8.2 c-0.7,0.7-1.7,0.7-2.4,0c-0.7-0.7-0.7-1.7,0-2.4l7-7l-7-7c-0.7-0.7-0.7-1.7,0-2.4C16.7,10.2,17.2,10.1,17.6,10.1L17.6,10.1z"/></g></g></svg>'],


		});

		$("#result-company").owlCarousel({
			slideSpeed : 300,
			paginationSpeed : 400,

			items:1,
			loop: true,
			stagePadding:30,
			smartSpeed:450,
			itemsDesktop : false,
			itemsDesktopSmall : false,
			itemsTablet: false,
			itemsMobile : false,
			margin:50,
			dots:false,
			nav:true,
			navText : ['<svg style="display: block" viewBox="0 0 10.6 18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <style type="text/css"> #rec37073304 .t-slds__arrow_wrapper polyline { -webkit-transition: stroke ease-in-out .2s; -moz-transition: stroke ease-in-out .2s; -o-transition: stroke ease-in-out .2s; transition: stroke ease-in-out .2s; } #rec37073304 .t-slds__arrow_wrapper:hover polyline { stroke: #ffffff !important; } </style> <desc>Left</desc> <polyline fill="none" stroke="#000000" stroke-linejoin="butt" stroke-linecap="butt" stroke-width="2" points="1,1 9,9 1,17"></polyline> </svg>'
				,'<svg style="display: block" viewBox="0 0 10.6 18" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <desc>Right</desc> <polyline fill="none" stroke="#000000" stroke-linejoin="butt" stroke-linecap="butt" stroke-width="2" points="1,1 9,9 1,17"></polyline> </svg>']

		});

		$("#owl-demo1").owlCarousel({		 
		    slideSpeed : 300,
		    paginationSpeed : 400,
		    items:1,
		    loop: true,
		    stagePadding:30,
    		smartSpeed:450,
    		itemsDesktop : false,
		    itemsDesktopSmall : false,
		    itemsTablet: false,
		    itemsMobile : false,
		    margin:35,
		    nav:true,
		    navText : ['<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40; width: 40px; border-radius: 50%" xml:space="preserve"><style type="text/css"> .st0{fill:#989CA1;} </style> <g><g><path class="st0" d="M20,40L20,40c-5.3,0-10.4-2.1-14.2-5.9c-7.8-7.8-7.8-20.5,0-28.3C9.6,2.1,14.7,0,20,0 c5.3,0,10.4,2.1,14.1,5.9C37.9,9.6,40,14.7,40,20s-2.1,10.4-5.9,14.1C30.4,37.9,25.3,40,20,40L20,40z M20,3.4 c-4.4,0-8.6,1.7-11.7,4.9c-6.5,6.5-6.5,17,0,23.5c3.1,3.1,7.3,4.9,11.7,4.9h0c4.4,0,8.6-1.7,11.7-4.9c3.1-3.1,4.9-7.3,4.9-11.7 s-1.7-8.6-4.9-11.7C28.6,5.1,24.4,3.4,20,3.4L20,3.4z"/></g><g><path class="st0" d="M22.4,29.9c-0.4,0-0.9-0.2-1.2-0.5L13,21.2c-0.3-0.3-0.5-0.8-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l8.2-8.2 c0.7-0.7,1.7-0.7,2.4,0c0.7,0.7,0.7,1.7,0,2.4l-7,7l7,7c0.7,0.7,0.7,1.7,0,2.4C23.3,29.8,22.8,29.9,22.4,29.9L22.4,29.9z"/></g></g></svg>'
,'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40; width: 40px; border-radius: 50%" xml:space="preserve"> <style type="text/css"> .st0{fill:#989CA1;}</style><g><g><path class="st0" d="M20,0L20,0c5.3,0,10.4,2.1,14.2,5.9c7.8,7.8,7.8,20.5,0,28.3C30.4,37.9,25.3,40,20,40 c-5.3,0-10.4-2.1-14.1-5.9C2.1,30.4,0,25.3,0,20S2.1,9.6,5.9,5.9C9.6,2.1,14.7,0,20,0L20,0z M20,36.6c4.4,0,8.6-1.7,11.7-4.9 c6.5-6.5,6.5-17,0-23.5C28.6,5.1,24.4,3.4,20,3.4h0c-4.4,0-8.6,1.7-11.7,4.9C5.1,11.4,3.4,15.6,3.4,20s1.7,8.6,4.9,11.7 C11.4,34.9,15.6,36.6,20,36.6L20,36.6z"/> </g><g><path class="st0" d="M17.6,10.1c0.4,0,0.9,0.2,1.2,0.5l8.2,8.2c0.3,0.3,0.5,0.8,0.5,1.2c0,0.5-0.2,0.9-0.5,1.2l-8.2,8.2 c-0.7,0.7-1.7,0.7-2.4,0c-0.7-0.7-0.7-1.7,0-2.4l7-7l-7-7c-0.7-0.7-0.7-1.7,0-2.4C16.7,10.2,17.2,10.1,17.6,10.1L17.6,10.1z"/></g></g></svg>']
		 
		});

		$("#owl-demo2").owlCarousel({		 
		    slideSpeed : 300,
		    paginationSpeed : 400,
		    items:1,
		    loop: true,
		    stagePadding:30,
    		smartSpeed:450,
    		itemsDesktop : false,
		    itemsDesktopSmall : false,
		    itemsTablet: false,
		    itemsMobile : false,
		    margin:30 
		});

		$('.popup .close_order, .overlay').click(function (){
        $('.popup, .overlay').css('opacity','0');
        $('.popup, .overlay').css('visibility','hidden');
    });

    $('.pop-up').click(function(){
	     var modalId = $(this).attr('data-attr');
	     $('.popup').css('visibility', 'hidden').css('opacity','0');
	     $('#'+modalId).css('visibility', 'visible').css('opacity','1');
	     $('.overlay').css('opacity','1').css('visibility','visible');
	     return false;
    });
		$('a.location').html();
		$('.dropdown-toggle').dropdown();
		$('ul.nav li.dropdown').hover(function() {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		var flagAccordion = false;
		$('.accordion-item').click(function () {
			console.log('click');
			if (flagAccordion == false) {
				$(this).children().find('.btn-accordion').css('transform','rotate(45deg)');

				flagAccordion = true;
			} else {
				$(this).children().find('.btn-accordion').css('transform','rotate(0deg)');
				flagAccordion = false;
			}
			$(this).children().siblings('.accordion-body').slideToggle();
		});
	});
});
$(window).scroll(function() {

	if($(this).scrollTop() != 0) {

		$('#toTop').fadeIn();

	} else {

		$('#toTop').fadeOut();

	}

});

$('#toTop').click(function() {

	$('body,html').animate({scrollTop:0},800);

});
$(window).load(function () {
	function setEqualHeight(columns)
	{
		var tallestcolumn = 0;
		columns.each(
			function()
			{
				currentHeight = $(this).height();
				if(currentHeight > tallestcolumn)
				{
					tallestcolumn = currentHeight;
				}
			}
		);
		columns.height(tallestcolumn);
	}
		setEqualHeight($(".blog-tale .post-title"));
		setEqualHeight($(".more-posts .post-title"));
		setEqualHeight($(".more-posts .post-excerpt"));

		let interval=setInterval(function(){
			var carouselCaptionWidth = $('.portfolio-item a img').width();
			if( carouselCaptionWidth<1 ) return;
			clearInterval(interval);

			$('.portfolio-item img').each(function () {
			   $(this).attr('width', carouselCaptionWidth);
			});
		},1000)

	// $('.portfolio-item .portfolio-image-block').css('max-width', carouselCaptionWidth + 'px');
    // $('.portfolio-item .portfolio-image-block .portfolio-image-discription').css('max-width', carouselCaptionWidth + 'px');
});
    
$(".maskphone").mask("+7 (999) 999-99-99");

$(function() {
		$('.location').click(function() {
			var thisList = $(this).closest('.city-choice').find('.cities');
			thisList.toggle();
			var firstClick1=true;
		});
		$('.cities li a').click(function() {
			var contentLink = $(this).html();
			$(this).closest('body').find('.location').html(contentLink);
			$(this).closest('.city-choice').find('.cities li a').removeClass('active');
			$(this).addClass('active');
			location.href = $(this).attr('href');
			$('#city-choice, .overlay').css('opacity','0').css('visibility','hidden');
			return false;
		});
	});