/*
	© Front-end Developer: serjGonchar (13.11.20017);
	E-Mail: goncharserj1992@gmail.com;
	Phone: +3 8 (099) 63 70 886;
	Skype: g.l.serj;
	Website: serjGonchar.pro;
*/

// Hidden Link
$('.main-content a[href|="/blog/"]').addClass('hidden');
$('.krosh a[href|="/blog/"]').removeClass('hidden');
// Navbar
$(window).scroll(function () {
	if ($(this).scrollTop() > 1) {
		$('.navbar-default').addClass("navbar-fixed-top");
    $('.focusNav').removeClass("hidden");
	} else {
		$('.navbar-default').removeClass("navbar-fixed-top");
    $('.focusNav').addClass("hidden");
	}
});
// Add Class Link
$('.main-content a[href|="/vacancy/#vacancy"]').addClass('newEmployee transition');

// Social link
$('.position-icon').removeClass('hidden-xs hidden-sm col-sm-6');
$('.position-icon').addClass('col-sm-12');

$('.position-icon').addClass('focusTextAlign');

// Cookie Disclaimer
$("#closemessage").click(function() {
	$('#storage').click();
});

function funStorage() {
	if (storage.checked) {
		$('.message').addClass('opacity0');
		setTimeout (function(){
			$('.message').addClass('hidden');
		}, 1500);
	}
	else {

	}
}