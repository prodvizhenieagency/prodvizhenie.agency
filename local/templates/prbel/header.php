<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/WebPage" lang="ru">
<head>


<noindex>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-6409204-32"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-6409204-32');
</script>
</noindex>




	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><? $APPLICATION->ShowTitle(); ?></title><!--<title>--><?// $APPLICATION->ShowMeta('title'); ?><!--</title>-->
	<? $APPLICATION->ShowHead(); ?>
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon180x180.png" sizes="180x180" id="favicon_180">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon152x152.png" sizes="152x152" id="favicon_152">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon144x144.png" sizes="144x144" id="favicon_144">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon120x120.png" sizes="120x120" id="favicon_120">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon114x114.png" sizes="114x114" id="favicon_114">

		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon76x76.png" sizes="76x76" id="favicon_76">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon72x72.png" sizes="72x72" id="favicon_72">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon60x60.png" sizes="60x60" id="favicon_60">
		<link rel="apple-touch-icon" type="image/png" href="/favicons/favicon57x57.png" sizes="57x57" id="favicon_57">
        <link rel="icon" type="image/png" href="/favicons/favicon32x32.png" sizes="32x32" id="favicon_32">
        <link rel="icon" type="image/png" href="/favicons/favicon96x96.png" sizes="96x96" id="favicon_96">
		<link rel="icon" type="image/png" href="/favicons/favicon16x16.png" sizes="16x16" id="favicon_16">
		<meta name="apple-mobile-web-app-title" content="DA Продвижение">
		<meta name="application-name" content="DA Продвижение">
		<meta name="msapplication-TileColor" content="#FFFFFF">

	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/owl.carousel.min.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/owl.theme.default.min.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/animate.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/magnific-popup.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH; ?>/css/jquery.fancybox.min.css" rel="stylesheet">

<link rel="canonical" href="https://<?php  echo $_SERVER['HTTP_HOST'];?><? echo $APPLICATION->GetCurDir(); ?>" />


	<?
	global $USER;
	if ($USER->IsAdmin()):;
		?>

<!--        <link href="--><?//= SITE_TEMPLATE_PATH; ?><!--/css/style.css" rel="stylesheet">-->
<!--        <link href="--><?//= SITE_TEMPLATE_PATH; ?><!--/prodaction/prodaction.css" rel="stylesheet">-->

        <link href="<?= SITE_TEMPLATE_PATH; ?>/css/style-test.css" rel="stylesheet">


        <link href="<?= SITE_TEMPLATE_PATH; ?>/prodaction/prodaction-test.css" rel="stylesheet">
	<? else : ?>
        <link href="<?= SITE_TEMPLATE_PATH; ?>/css/style-test.css" rel="stylesheet">


        <link href="<?= SITE_TEMPLATE_PATH; ?>/prodaction/prodaction-test.css" rel="stylesheet">
	<? endif; ?>

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<meta name='yandex-verification' content='703e95b6b470bc80' />
	<meta name="yandex-verification" content="3962e787208df875" />
	<meta name="google-site-verification" content="aKreIVv7U3Sjqig4R-Bzyn5hIkBo7HIVn7OIAMfOEQA" />


    <? if ($_SERVER['HTTP_HOST'] == 'spb.prodvizhenie-bel.ru') : ?>
        <style>
            .dropdown-menu>li:nth-child(3) a {
                position: relative;
                padding-left: 40px;
            }
            .dropdown-menu>li:nth-child(3) a:after {
                content: '';
                position: absolute;
                top:8px;
                left: 20px;
                width: 8px;
                height: 8px;
                border-radius: 50%;
                background: #F34026;
            }
        </style>
    <? endif; ?>
<!-- calltouch -->
<script type="text/javascript">(function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]["callbacks"]=[]}w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var a=d.getElementsByTagName("script")[0],s=d.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)};s.type="text/javascript";s.async=true;s.src="https://mod.calltouch.ru/init.js?id="+cId;if(w.opera=="[object Opera]"){d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})(window,document,"ct","2g0x5q23");</script>
<!-- calltouch -->

<script>(function(w, c){(w[c]=w[c]||[]).push(function(){new zTracker({"id":"9671109155a1950cf9b1f44b789c9fff419","metrics":{"metrika":"25552757","ga":"UA-6409204-32"}});});})(window, "zTrackerCallbacks");</script>
<script async id="zd_ct_phone_script" src="https://my.zadarma.com/js/ct_phone.min.js"></script>

<script type="text/javascript">
(function ct_load_script() {
var ct = document.createElement('script'); ct.type = 'text/javascript';
ct.src = document.location.protocol+'//cc.calltracking.ru/phone.3cfdf.10422.async.js?nc='+Math.floor(new Date().getTime()/300000);
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ct, s);
})();
</script>

</head>
<body>



<?$APPLICATION->ShowPanel();?>
<header itemscope itemtype="http://schema.org/WPHeader">
	<nav class="navbar navbar-default">
		<div class="container">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" aria-expanded="false" id="mobile-btn">
					<img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/burger.png" alt="">
				</button>
				<? if ($APPLICATION->GetCurPage(false) === '/'): ?>
					<a class="navbar-brand" >
<!--						<img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/ng-1.gif" alt="logo" class="gif-ny">-->
						<img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/logo-nd.svg" alt="logo" class="desktop-logo">
						<img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/logo-nd.svg" alt="logo" class="mobile-logo">
					</a>
				<? endif; ?>
				<? if ($APPLICATION->GetCurPage(false) !== '/'): ?>
					<a class="navbar-brand" href="/">
<!--						<img src="--><?//= SITE_TEMPLATE_PATH; ?><!--/img/main/ng-1.gif" alt="logo" class="gif-ny">-->
						<img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/logo-nd.svg" alt="logo" class="desktop-logo">
						<img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/logo-nd.svg" alt="logo" class="mobile-logo">
					</a>
				<? endif; ?>

			</div>

			<div class="collapse navbar-collapse mobile-menu" id="bs-example-navbar-collapse-1">
				<? $APPLICATION->IncludeComponent(
					"bitrix:menu",
					"top",
					Array(
						"ALLOW_MULTI_SELECT"    => "N",
						"CHILD_MENU_TYPE"       => "left",
						"COMPONENT_TEMPLATE"    => "top",
						"COUNT_ITEM"            => "6",
						"DELAY"                 => "N",
						"MAX_LEVEL"             => "4",
						"MENU_CACHE_GET_VARS"   => array(),
						"MENU_CACHE_TIME"       => "3600000",
						"MENU_CACHE_TYPE"       => "A",
						"MENU_CACHE_USE_GROUPS" => "N",
						"ROOT_MENU_TYPE"        => "top",
						"USE_EXT"               => "Y",
					)
				); ?>
				<?
					$cities = array(
						'prodvizhenie.agency'                  => 'Белгород',
						'voronezh.prodvizhenie.agency'         => 'Воронеж',
						'msk.prodvizhenie.agency'              => 'Москва',
						//'kursk.prodvizhenie-bel.ru'            => 'Курск',
						'spb.prodvizhenie.agency' => 'Санкт-Петербург',
						//'krasnodar.prodvizhenie-bel.ru' => 'Краснодар',
						//'tula.prodvizhenie-bel.ru' => 'Тула',
						//'rostov.prodvizhenie-bel.ru' => 'Ростов-на-Дону',
						//'ekaterinburg.prodvizhenie-bel.ru' => 'Екатеринбург',
					)
				?>
				<ul class="nav navbar-nav navbar-right">
					<li style="position: relative;">
						<i class="fa fa-location-arrow mobile-location-arrow"></i>

<? if( $APPLICATION->GetCurPage() == '/seo-sankt-peterburg/' ){ ?>
		<a href="#" class="location pop-up 11" data-attr="city-choice">Санкт-Петербург</a>
	<? } else { ?>
		<a href="#" class="location pop-up" data-attr="city-choice"><?= $cities[$_SERVER['SERVER_NAME']]; ?></a>
<? } ?>

						<!-- <span class="header-address"> 
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => "/include/prbel/header_address.php",
								"EDIT_TEMPLATE" => ""
							),
								false
							);?>
						</span> -->
					</li>

					<li>
						<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => "/include/prbel/header_phone.php",
							"EDIT_TEMPLATE" => ""
						),
							false
						);?>

<!--						<p class="call-back"><a class="b24-web-form-popup-btn-9">Заказать звонок</a></p></li>-->
				</ul>

			</div>
			<div class="close-bg"></div>
		</div>
	</nav>
	<div class="focusNav hidden"></div>

</header>
<section class="main-content">
