<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$APPLICATION->SetTitle("SEO-продвижение сайтов");
?>
		<div class="banner-about banner-with-img">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-12">
						<h1>SEO-продвижение сайтов</h1>
						<p class="baner-disc">
							Продвижение сайтов предоставляется по разным схемам, цель которых привлечение максимально заинтересованных посетителей на сайт, постоянный рост объема посетителей, следствие чего увеличение продаж. Выводим сайты на первые позиции в Яндекс и Google. Увеличиваем трафик из поисковых систем в среднем на 30%
						</p>
					</div>
					<div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
						<img src="<?= SITE_TEMPLATE_PATH; ?>/img/page/seo.png" alt="" class="img-responsive">
					</div>
				</div>

				<div class="row krosh">
					<p>
						<a href="/">Главная</a> <i class="fa fa-angle-right" aria-hidden="true"></i>
						<a href="/services/">Услуги</a> <i class="fa fa-angle-right" aria-hidden="true"></i>
						<span>О компании</span></p>
				</div>
			</div>
		</div>
		<div class="direction">
			<div class="container">

				<div class="row">
					<div class="col-lg-offset-2 col-lg-10">
						<div class="row">
							<div class="col-lg-6">
								<p>
									За 50 шагов подготовим сайт к продвижению в поисковых системах, поднимем в выдаче и увеличим целевой трафик без дополнительного рекламного бюджета. Выводим сайты на первые позиции в Яндекс и Google. Увеличиваем трафик из поисковых систем в среднем на 30%.
								</p>
							</div>
							<div class="col-lg-6">
								<h2>
									Компания «Продвижение» — это команда джедаев своего напрaвления
								</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-10">
								<p>
									Привлекаем клиентов из социальных сетей. Заявки из SMM обходятся в 1,5–2 дешевле, чем из контекстной рекламы, SEO или проплаченных публикаций. В нашей практике были случаи, когда заявка и покупка с таргетированной рекламы обходились в 5 раз дешевле, чем из контекста.
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-10">
								<p>
									Для быстрого старта продаж настроим контекстную рекламу. За счет продуманных креативов, тщательной подгонки под интересы аудитории и высоких показателей CTR вы получите больше кликов, больше заказов, больше продаж.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="cta-banner">
			<div class="container">
				<div class="col-xs-12">
					За время существования наша команда подготовила тысячи маркетинговых исследований рынков и бизнес-планов. Разработала порядка 700 сайтов, протестировала 374 приложения и внедрила сотню CRM-систем.
				</div>
			</div>
		</div>
		<div class="why-profit">
			<div class="container">
				<div class="row text-center">
					<h2>
						Что даёт поисковое SEO-продвижение
					</h2>
				</div>
				<div class="row">
					<div class="col-lg-10 col-lg-offset-2">
						<div class="row">
							<div class="col-lg-1 col-md-1 col-sm-2 col-xs-12">
								<div class="img-wrap">
									<img src="<?= SITE_TEMPLATE_PATH; ?>/img/page/check.svg" alt="" class="img-responsive">
								</div>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="profit-title">
									Долгосрочный результат
								</div>
								<div class="profit-text">
									Мы являемся ведущей компанией в Белгороде, обеспечивая качество и ценность для наших клиентов. Все наши специалисты имеют более 5 лет профессионального опытв. Нам нравится, то что мы делаем.
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-1 col-md-1 col-sm-2">
								<div class="img-wrap">
									<img src="<?= SITE_TEMPLATE_PATH; ?>/img/page/check.svg" alt="" class="img-responsive">
								</div>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9">
								<div class="profit-title">
									Повышение количества продаж
								</div>
								<div class="profit-text">
									Наши менеджеры всегда готовы ответить на Ваши вопросы. Вы можете позвонить нам по выходным и ночью. Также вы можете посетить наш офис для личной консультации.
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-1 col-md-1 col-sm-2">
								<div class="img-wrap">
									<img src="<?= SITE_TEMPLATE_PATH; ?>/img/page/check.svg" alt="" class="img-responsive">
								</div>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9">
								<div class="profit-title">
									Целевых посетителей
								</div>
								<div class="profit-text">
									Наша компания работает по принципу индивидуального подхода к каждому клиенту. Этот метод позволяет нам добиваться успеха.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="step-to-step">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<h2>Что входит в продвижение сайта</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-12 number-col">
								<div class="wrap">1</div>
							</div>
							<div class="col-lg-10 col-ms-10 col-sm-12">
								<div class="step-title">
									Работа с целевыми запросами
								</div>
								<div class="step-text">
									Доработка, актуализация, расширение списка продвигаемых запросов
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm1-2 number-col">
								<div class="wrap">2</div>
							</div>
							<div class="col-lg-10 col-ms-10 col-sm-12">
								<div class="step-title">
									Работа над контентом
									и текстами
								</div>
								<div class="step-text">
									Все действия по оптимизациии и развитию содержания сайтов
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-12 number-col">
								<div class="wrap">3</div>
							</div>
							<div class="col-lg-10 col-ms-10 col-sm-12">
								<div class="step-title">
									Работа с ссылками
								</div>
								<div class="step-text">
									Постоянное корректное увеличение внешней ссылочной массы
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-12 number-col">
								<div class="wrap">4</div>
							</div>
							<div class="col-lg-10 col-ms-10 col-sm-12">
								<div class="step-title">
									Улучшение продающих свойств
								</div>
								<div class="step-text">
									Доработка коммерческих факторов для лучшей конверсии
									и юзабилити
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-12 number-col">
								<div class="wrap">5</div>
							</div>
							<div class="col-lg-10 col-ms-10 col-sm-12">
								<div class="step-title">
									Аналитика поведения пользователей
								</div>
								<div class="step-text">
									Анализ поведения пользователей на сайте и доработки интерфейса
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-12 number-col">
								<div class="wrap">6</div>
							</div>
							<div class="col-lg-10 col-ms-10 col-sm-12">
								<div class="step-title">
									Анализ конкурентов и эффективности запросов
								</div>
								<div class="step-text">
									Предложения по улучшению рекламных каналов
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="why-we-the-one">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<h2>
							Почему мы первые?
						</h2>
						<div class="description">
							Уже несколько лет подряд мы занимаем первое место в<br /> рейтинге SEO-компаний Белгорода
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs img-wrap">
						<img src="<?= SITE_TEMPLATE_PATH; ?>/img/page/first.png" alt="">
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="why-wrap">
							<div class="why-title">
								Гарантируем
							</div>
							<div class="why-text">
								We are the leading firm by delivering quality and value to our clients. We like what we do. You can call us at the weekends and at night.
							</div>
						</div>
						<div class="why-wrap">
							<div class="why-title">
								Увеличиваем
							</div>
							<div class="why-text">
								Our managers are always ready to answer your questions. You can call us at the weekends and at night.
							</div>
						</div>
						<div class="why-wrap">
							<div class="why-title">
								Продвигаем
							</div>
							<div class="why-text">
								All our professionals have more than 5 years of legal experiences. They use their knowledge to make our clients life better.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/result_slider.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>
		<div class="services">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center ">
						<h2>Наши услуги</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-2 col-sm-8">
						<div class="description">
							Для достижения наилучшего результата мы объединяем эффективные инструменты в рабочие связки. Помимо SEO-продвижения, мы предлагаем следующие услуги
						</div>
					</div>
				</div>
				<div class="row row-space ">
					<div class="col-md-4 col-sm-6 col-space text-center">
						<a href="#">
							<div href="#" class="info-block">
								<div class="service-title">Разработка сайтов</div>
								<p>Здесь будет вводное описание. В три строчки нужно вместить основной смысл услуги</p>
								<a href="#" class="more-link">Подробнее&nbsp;&#8594;</a>
							</div>
						</a>
					</div>


					<div class="col-md-4 col-sm-6 col-space text-center">
						<a href="">
							<div href="#" class="info-block">
								<div class="service-title">Контекстная реклама</div>
								<p>Здесь будет вводное описание. В три строчки нужно вместить основной смысл услуги</p>
								<a href="#" class="more-link">Подробнее&nbsp;&#8594;</a>
							</div>
						</a>
					</div>
					<div class="col-md-4 col-sm-6 col-sm-offset-3 col-lg-offset-0 col-md-offset-0 col-space text-center">
						<a href="#">
							<div href="#" class="info-block">
								<div class="service-title">Консалтинг и аналитика</div>
								<p>Здесь будет вводное описание. В три строчки нужно вместить основной смысл услуги</p>
								<a href="#" class="more-link">Подробнее&nbsp;&#8594;</a>
							</div>
						</a>
					</div>
				</div>

				<div class="row ">

					<div class="col-md-4 col-sm-6 col-space text-center">
						<a href="#">
							<div href="#" class="info-block">
								<div class="service-title">Автоматизация продаж</div>
								<p>Здесь будет вводное описание. В три строчки нужно вместить основной смысл услуги</p>
								<a href="#" class="more-link">Подробнее&nbsp;&#8594;</a>
							</div>
						</a>
					</div>

					<div class="col-md-4 col-sm-6 col-space text-center">
						<a href="#">
							<div href="#" class="info-block">
								<div class="service-title">Дизайн сайтов</div>
								<p>Здесь будет вводное описание. В три строчки нужно вместить основной смысл услуги</p>
								<a href="#" class="more-link">Подробнее&nbsp;&#8594;</a>
							</div>
						</a>
					</div>
					<div class="col-md-4 col-sm-6 col-sm-offset-3 col-lg-offset-0 col-md-offset-0 col-space text-center blank-element">
						<div href="#" class="info-block">
							<a href="#" class="pop-up" data-attr="order-call-service">
								<div class="service-title">Не нашли нужную услугу?</div>
								<p>Опишите какую задачу нужно решить и мы постараемся вам помочь</p>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/trust.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/contact_us.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>