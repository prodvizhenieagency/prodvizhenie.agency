<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "SEO продвижение сайтов в #WF_CITY_PRED# недорого. Цены на услуги СЕО-продвижение сайта в топ по запросам в Яндексе и Google");
$APPLICATION->SetPageProperty("title", "SEO продвижение сайтов в  #WF_CITY_PRED# 🔝 – Услуги SEO (СЕО) оптимизации сайтов");
$APPLICATION->SetTitle("SEO продвижение сайта ");

$APPLICATION->AddChainItem("Услуги", "/services/");
$APPLICATION->AddChainItem("SEO-продвижение");
?>

<section>

	

<div itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">									
	<div itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">								
		<meta itemprop="url" content="https://prodvizhenie.agency/local/templates/prbel/img/main/logo.png">					  		
	</div>								
	<link itemprop="url" href="https://prodvizhenie.agency/">																
	<div itemprop="address" itemscope="" itemtype="https://schema.org/PostalAddress">					  			
		<meta itemprop="postalCode" content="308033">							
		<meta itemprop="addressCountry" content="Россия">							
		<meta itemprop="addressRegion" content="Белгородская обл.">							
		<meta itemprop="addressLocality" content="г. Белгород,">							
		<meta itemprop="streetAddress" content="ул. Королева, 2а, корп. 2, офис 704">					  		
	</div>								
	<meta itemprop="telephone" content="+7 (4722) 777-431">
	<meta itemprop="telephone" content="+7 (800) 200-55-31">	
	<meta itemprop="email" content="go@pr-bel.ru">	
	<meta itemprop="image" content="https://prodvizhenie.agency/upload/medialibrary/775/7750e3c3025e78dd7fd439b58c50ba92.jpg">																
	<meta itemprop="brand" content="Digital-агенство Продвижение">								
	<meta itemprop="legalName" content="Digital-агенство Продвижение">								
	<meta itemprop="name" content="<?$APPLICATION->ShowTitle()?>">								
	<meta itemprop="description" content="<?$APPLICATION->ShowProperty('description');?>">								
</div>									

<div class="banner-about banner-with-img">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12">
				<h1 itemprop="headline">SEO-продвижение сайтов в #WF_CITY_PRED#</h1>
				 #WF_SEO_TEXT_2#
			</div>
			<div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
				 <!--<img src="/local/templates/prbel/img/page/seo.png" alt="Продвижение сайтов в #WF_CITY_PRED#" class="img-responsive">-->
			</div>
		</div>
		<div class="row krosh 111">
			<?$APPLICATION->IncludeComponent(
			"bitrix:breadcrumb",
			"schema-1",
			Array()
			);?>
		</div>

<div class="row krosh 222">
	<span itemscope="" itemtype="http://schema.org/BreadcrumbList">
		<span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<meta itemprop="name" content="Главная">
			<meta itemprop="position" content="0">
			<meta itemprop="item" content="https://prodvizhenie.agency/">
		</span>
		<i class="fa fa-angle-right" aria-hidden="true"></i>
		<span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<meta itemprop="name" content="SEO-продвижение ⭐️⭐️⭐️⭐️⭐️">
			<meta itemprop="position" content="1">
			<meta itemprop="item" content="https://prodvizhenie.agency/seo/">
		</span>								
	</span>		
</div>

	</div>
</div>
<div class="direction">
	<div class="container">
		<div class="row">
			<div class="col-lg-1">
			</div>
			 #WF_SEO_TEXT_3#
			<div class="col-lg-1">
			</div>
		</div>
	</div>
</div>
<div class="cta-banner" style="padding-bottom: 0px;">
	<div class="container">
		<div class="col-12 col-md-12 col-lg-12 banner_seo_txt">
			 Продвинули более 100 сайтов: интернет-магазины, корпоративные сайты. Чаще это B2B, сайты производителей и дилеров, торговых компаний и компаний, оказывающих услуги. <br>


 <img src="/images/valera_square2-min.png" class="img-responsive" style="border-radius: 50%; width: 80px; margin-top: 20px; float: left; margin-right: 20px; margin-bottom: 30px;">



			<p class="name_seo">
				 Валерий Калиничев
			</p>
			<p class="name_job">
				 Руководитель направления поисковой оптимизации
			</p>
			 <script id="bx24_form_button" data-skip-moving="true">
        (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
                (w[b].forms=w[b].forms||[]).push(arguments[0])};
                if(w[b]['forms']) return;
                var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://prbel.bitrix24.ru/bitrix/js/crm/form_loader.js','b24form');

        b24form({"id":"11","lang":"ru","sec":"f88ex0","type":"button","click":""});
</script>
			<p class="button_align">
				 <a class="b24-web-form-popup-btn-11" ><button class="webform-services" style="margin-top: 0px;">Оставить заявку</button></a>
			</p>
		</div>
		<div class="col-12 col-md-5 col-lg-5 mt-auto mb-0 block_photo" style="margin-bottom: -14px;">
 <img alt="Валерий Калиничев - Руководитель направления поисковой оптимизации" src="/images/Valera_3.png" title="Валерий Калиничев - Руководитель направления поисковой оптимизации" class="seo_photo">
		</div>
	</div>
</div>
<div class="why-profit"  >
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
			<h2>Что даёт поисковое SEO-продвижение </h2>
			</div>
		</div>
		<div class="row">
			 #WF_SEO_TEXT_4#
		</div>
	</div>
</div>
<div class="step-to-step"  >
	<div class="container">
		 #WF_SEO_TEXT_5#
	</div>
</div>
<div class="why-we-the-one" >
	<div class="container">
		 #WF_SEO_TEXT_6# <!-- <div class="row">
            <div class="col-xs-12 text-center">
                <img src="/images/diplom.png" width="600px">
            </div>
        </div> -->
	</div>
</div>

		 #WF_SEO_TEXT_10#





 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/result_slider.php"
	)
);?> #WF_SEO_TEXT_7# #WF_SEO_TEXT_9#
<div class="faq"  style="padding: 75px 0 50px; background-color: #efefef;">
	<div class="container">
		 #WF_SEO_TEXT_8#
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/trust.php"
	)
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/contact_us_seo.php"
	)
);?>
<div class="services context-service"  style="background-color: white;">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center ">
				<h2 style="margin-bottom: 25px; padding-top: 10px;">Наши услуги</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2">
				<div class="description">
					 Для наилучшего результата&nbsp;мы объединяем инструменты. Кроме SEO, мы предлагаем:
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-10 col-md-offset-1">
			<div class="row row-space ">
 <a href="/sozdanie-sajtov/">
				<div class="col-md-6 col-sm-6 col-space text-center row-space">
					<div class="info-block alt">
						<div class="service-title">
							 Разработка сайтов
						</div>
						<p>
							 Разрабатываем продающие и оптимизированные сайты для максимальной эффективности интернет-маркетинга
						</p>
						 <!--                               <a href="#" class="more-link">Подробнее →</a>-->
						<div class="arrow_link">
						</div>
					</div>
				</div>
 </a> <a href="/kontekstnaya-reklama/">
				<div class="col-md-6 col-sm-6 col-space text-center row-space">
					<div class="info-block alt">
						<div class="service-title">
							 Контекстная реклама
						</div>
						<p>
							 Запускаем эффективные рекламные кампании в Яндекс и Google, ориентированные на конверсии, ROI и целевые показатели
						</p>
						<div class="arrow_link">
						</div>
					</div>
				</div>
 </a>
				<!-- / Development --> <!-- Services --> <a href="/podderzhka-sajtov/">
				<div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
					<div class="info-block alt">
						<div class="service-title">
							 Техподдержка сайтов
						</div>
						<p>
							 Большой опыт позволяет нам осуществлять поддержку и сопровождение сайтов на Bitrix любой сложности на долгосрочной основе
						</p>
						<div class="arrow_link">
						</div>
					</div>
				</div>
 </a> <a href="/audit/">
				<div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
					<div class="info-block alt">
						<div class="service-title">
							 Аудит
						</div>
						<p>
							 Проконсультируем, какие действия предпринять, чтобы потенциальные клиенты находили вас быстрее
						</p>
						<div class="arrow_link">
						</div>
					</div>
				</div>
 </a>
			</div>
			 <!-- / Services   <a href="/services/analytics/">--> <!-- / ??? -->
		</div>
	</div>
	<div class="row">
		<div class="col-lg-offset-0 col-md-12 col-sm-12 col-space text-center blank-element">
 <noindex>
			<div href="#">
				 <a class="b24-web-form-popup-btn-15" >
                         <h2 style="padding-top: 30px;  margin-bottom: 25px;">Не нашли нужную услугу?</h2>
                         <div class="description" style="margin-bottom:0px">
                    <span style="color:#333">Опишите какую задачу нужно решить и мы найдем оптимальное решение</span>
                </div>
<button class="webform-services" name="nuzhnaya-usluga">
Написать</button>
</a>
			</div>
 </noindex>
		</div>
	</div>
</div>
</section>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>