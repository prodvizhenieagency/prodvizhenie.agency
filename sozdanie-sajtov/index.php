<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "&#11088; Разработка и создание сайтов в #WF_CITY_PRED#, поддержка и продвижение. Создание сайтов на Битрикс - #WF_CITY_VIN#");
$APPLICATION->SetPageProperty("title", "Создание сайтов в #WF_CITY_PRED# – Разработка сайта под ключ по недорогой цене");
$APPLICATION->SetTitle("Создание сайтов в #WF_CITY_PRED#");


$APPLICATION->AddChainItem("Создание сайтов️");
?>

<section>
	

<div itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">									
	<div itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">								
		<meta itemprop="url" content="https://prodvizhenie.agency/local/templates/prbel/img/main/logo.png">					  		
	</div>								
	<link itemprop="url" href="https://prodvizhenie.agency/">																
	<div itemprop="address" itemscope="" itemtype="https://schema.org/PostalAddress">					  			
		<meta itemprop="postalCode" content="308033">							
		<meta itemprop="addressCountry" content="Россия">							
		<meta itemprop="addressRegion" content="Белгородская обл.">							
		<meta itemprop="addressLocality" content="г. Белгород,">							
		<meta itemprop="streetAddress" content="ул. Королева, 2а, корп. 2, офис 704">					  		
	</div>								
	<meta itemprop="telephone" content="+7 (4722) 777-431">
	<meta itemprop="telephone" content="+7 (800) 200-55-31">	
	<meta itemprop="email" content="go@pr-bel.ru">	
	<meta itemprop="image" content="https://prodvizhenie.agency/upload/medialibrary/775/7750e3c3025e78dd7fd439b58c50ba92.jpg">																
	<meta itemprop="brand" content="Digital-агенство Продвижение">								
	<meta itemprop="legalName" content="Digital-агенство Продвижение">								
	<meta itemprop="name" content="<?$APPLICATION->ShowTitle()?>">								
	<meta itemprop="description" content="<?$APPLICATION->ShowProperty('description');?>">								
</div>	

<div class="banner-about banner-with-img">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h1 itemprop="headline" ><? $APPLICATION->ShowTitle(false);?></h1>
        #WF_SEO_TEXT_2#
      </div>
      <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
         <!--<img src="/local/templates/prbel/img/page/kotly.png" alt="" class="img-responsive">-->
      </div>
    </div>
    <div class="row krosh">
      <?$APPLICATION->IncludeComponent( "bitrix:breadcrumb", "schema-1", Array(), false );?>
    </div>

<script type="application/ld+json">		
    {		
        "@context": "https://schema.org",		
        "@type": "BreadcrumbList",		
        "itemListElement": [{		
    "@type": "ListItem",		
    "name": "Главная",	
	"position": 0,	
    "item": "https://prodvizhenie.agency/"		
},{		
    "@type": "ListItem",				
    "name": "Создание сайтов ⭐️⭐️⭐️⭐️⭐",
    "position": 1,	
    "item": "https://prodvizhenie.agency/sozdanie-sajtov/"				
}]		
    }		
</script>	




  </div>
</div>
<div class="direction">
  <div class="container">
    <div class="row">
      #WF_SEO_TEXT_3#
    </div>
  </div>
</div>
<div class="why-dev">
  <div class="container">
    #WF_SEO_TEXT_4#
  </div>
</div>
<div class="why-profit">
  <div class="container">
    #WF_SEO_TEXT_5#
  </div>
</div>
<div class="word-lead">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
        <div class="word-wrap">
          #WF_SEO_TEXT_10#
           
        </div>
        <div class="bottom-line">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
        <div class="lead-wrap">
          <div class="row">
            <div class="author-img">
 <img src="/images/Sveta-min.jpg" alt=" Светлана Бочарникова - Ведущий менеджер проектов" title=" Светлана Бочарникова - Ведущий менеджер проектов" class="img-responsive">
            </div>
            <div class="author-name">
              <div class="name">
                Светлана Бочарникова
              </div>
              <div class="status">
                 Ведущий менеджер проектов
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="step-by-dev">
  <div class="container">
    #WF_SEO_TEXT_6#
  </div>
</div>

#WF_SEO_TEXT_7#




<div class="cta-banner">
  <div class="container">
    <div class="col-xs-12">
       Высокий уровень клиентского сервиса и качества продукта помогает добиваться команда высококвалифицированных специалистов.
    </div>
  </div>
</div>
 <?$APPLICATION->IncludeComponent(
  "bitrix:news", 
  "projects_main", 
  array(
    "ADD_ELEMENT_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "BROWSER_TITLE" => "-",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "N",
    "CACHE_TIME" => "100000",
    "CACHE_TYPE" => "A",
    "CHECK_DATES" => "Y",
    "COMPONENT_TEMPLATE" => "projects_main",
    "COMPOSITE_FRAME_MODE" => "A",
    "COMPOSITE_FRAME_TYPE" => "AUTO",
    "DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",
    "DETAIL_BRAND_PROP_CODE" => array(
      0 => "TIZERS",
      1 => "",
    ),
    "DETAIL_BRAND_USE" => "Y",
    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
    "DETAIL_DISPLAY_TOP_PAGER" => "N",
    "DETAIL_FIELD_CODE" => array(
      0 => "NAME",
      1 => "PREVIEW_TEXT",
      2 => "DETAIL_TEXT",
      3 => "DETAIL_PICTURE",
      4 => "",
    ),
    "DETAIL_PAGER_SHOW_ALL" => "Y",
    "DETAIL_PAGER_TEMPLATE" => "",
    "DETAIL_PAGER_TITLE" => "Страница",
    "DETAIL_PROPERTY_CODE" => array(
      0 => "",
      1 => "FORM_ORDER",
      2 => "FORM_QUESTION",
      3 => "ORDERER",
      4 => "SITE",
      5 => "DATA",
      6 => "AUTHOR",
      7 => "LINK_PROJECTS",
      8 => "TASK_PROJECT",
      9 => "LINK_COMPANY",
      10 => "LINK_REVIEWS",
      11 => "LINK_GOODS",
      12 => "LINK_SERVICES",
      13 => "FORM_PROJECT",
      14 => "DOCUMENTS",
      15 => "PHOTOS",
      16 => "",
    ),
    "DETAIL_SET_CANONICAL_URL" => "N",
    "DETAIL_STRICT_SECTION_CHECK" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_NAME" => "N",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "DISPLAY_TOP_PAGER" => "N",
    "ELEMENT_TYPE_VIEW" => "element_1",
    "FILTER_FIELD_CODE" => array(
      0 => "",
      1 => "",
    ),
    "FILTER_NAME" => "arProjectFilter",
    "FILTER_PROPERTY_CODE" => array(
      0 => "",
      1 => "",
    ),
    "FORM_ID" => "14",
    "FORM_ID_ORDER_SERVISE" => "14",
    "GALLERY_TYPE" => "big",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "IBLOCK_ID" => "35",
    "IBLOCK_TYPE" => "aspro_digital_content",
    "IMAGE_POSITION" => "left",
    "IMAGE_WIDE" => "Y",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "LINE_ELEMENT_COUNT" => "2",
    "LINE_ELEMENT_COUNT_LIST" => "3",
    "LIST_ACTIVE_DATE_FORMAT" => "j F Y",
    "LIST_FIELD_CODE" => array(
      0 => "NAME",
      1 => "PREVIEW_TEXT",
      2 => "DETAIL_PICTURE",
      3 => "",
    ),
    "LIST_PROPERTY_CODE" => array(
      0 => "LINK_TO_CASE",
      1 => "",
    ),
    "MESSAGE_404" => "",
    "META_DESCRIPTION" => "-",
    "META_KEYWORDS" => "-",
    "NEWS_COUNT" => "50",
    "PAGER_BASE_LINK_ENABLE" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => ".default",
    "PAGER_TITLE" => "Новости",
    "PREVIEW_TRUNCATE_LEN" => "",
    "SECTION_ELEMENTS_TYPE_VIEW" => "FROM_MODULE",
    "SEF_FOLDER" => "/projects/",
    "SEF_MODE" => "Y",
    "SET_LAST_MODIFIED" => "N",
    "SET_STATUS_404" => "N",
    "SET_TITLE" => "N",
    "SHARE_HANDLERS" => array(
      0 => "facebook",
      1 => "mailru",
      2 => "delicious",
      3 => "lj",
      4 => "twitter",
      5 => "vk",
    ),
    "SHARE_HIDE" => "N",
    "SHARE_SHORTEN_URL_KEY" => "",
    "SHARE_SHORTEN_URL_LOGIN" => "",
    "SHARE_TEMPLATE" => "",
    "SHOW_404" => "N",
    "SHOW_DETAIL_LINK" => "Y",
    "SHOW_NEXT_ELEMENT" => "Y",
    "SHOW_SECTION_DESCRIPTION" => "Y",
    "SHOW_SECTION_PREVIEW_DESCRIPTION" => "Y",
    "SORT_BY1" => "SORT",
    "SORT_BY2" => "ID",
    "SORT_ORDER1" => "ASC",
    "SORT_ORDER2" => "DESC",
    "S_ASK_QUESTION" => "",
    "S_ORDER_PROJECT" => "Заказать проект",
    "S_ORDER_SERVISE" => "Заказать проект",
    "T_CHARACTERISTICS" => "",
    "T_CLIENTS" => "",
    "T_DOCS" => "",
    "T_GALLERY" => "",
    "T_GOODS" => "",
    "T_NEXT_LINK" => "Следующий проект",
    "T_PREV_LINK" => "",
    "T_PROJECTS" => "Похожие проекты",
    "T_REVIEWS" => "Отзыв клиента",
    "T_SERVICES" => "",
    "USE_CATEGORIES" => "N",
    "USE_FILTER" => "N",
    "USE_PERMISSIONS" => "N",
    "USE_RATING" => "N",
    "USE_REVIEW" => "N",
    "USE_RSS" => "N",
    "USE_SEARCH" => "N",
    "USE_SHARE" => "Y",
    "STRICT_SECTION_CHECK" => "N",
    "SEF_URL_TEMPLATES" => array(
      "news" => "",
      "section" => "#SECTION_CODE_PATH#/",
      "detail" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
    )
  ),
  false
);?>



<div class="step-to-step stages"  id="#stages">
  <div class="container">
    #WF_SEO_TEXT_8#
  </div>
</div> 


<div class="faq">
  <div class="container">
  #WF_SEO_TEXT_9#
</div>
</div>



<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/prbel/trust.php"
  )
);?>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/prbel/contact_us_sozdanie.php"
  )
);?>



<div class="services context-service" style="background-color: white;">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-center ">
        <h2 style="margin-bottom: 25px; padding-top: 10px;">Кроме создания и разработки сайтов</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="description">
           Для достижения наилучшего результата мы объединяем эффективные инструменты в рабочие связки
        </div>
      </div>
    </div>
      <div class="col-xs-12 col-md-10 col-md-offset-1">
    <div class="row row-space ">
       <!-- Seo --> <a href="/seo/">
      <div class="col-md-6 col-sm-6 col-space text-center row-space">
        <div class="info-block alt">
          <div class="service-title">
             SEO-продвижение
          </div>
          <p>
             Привлекаем трафик из поисковых систем. Работаем на рост посещаемости сайта и увеличение продаж в долгосрочной перспективе
          </p>
          <div class="arrow_link">
          </div>
        </div>
      </div>
 </a>
      <!-- / Context --> <!-- Services --> <a href="/kontekstnaya-reklama/">
      <div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
        <div class="info-block alt">
          <div class="service-title">
             Контекстная реклама
          </div>
          <p>
             Запускаем эффективные рекламные кампании в Яндекс и Google, ориентированные на конверсии, ROI и целевые показатели
          </p>
          <div class="arrow_link">
          </div>
        </div>
      </div>
 </a>
      <!-- / Development --> <!-- Services --> <a href="/podderzhka-sajtov/">
      <div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
        <div class="info-block alt">
          <div class="service-title">
             Техподдержка сайтов
          </div>
          <p>
             Большой опыт позволяет нам осуществлять поддержку и сопровождение сайтов на Bitrix любой сложности на долгосрочной основе
          </p>
          <div class="arrow_link">
          </div>
        </div>
      </div>
 </a>
<a href="/audit/">
      <div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
        <div class="info-block alt">
          <div class="service-title">
             Аудит
          </div>
          <p>
             Проконсультируем, какие действия предпринять, чтобы потенциальные клиенты находили вас быстрее
          </p>
          <div class="arrow_link">
          </div>
        </div>
      </div>
 </a>
    </div>
    </div>
  </div>
<div class="row">
      <div class="col-lg-offset-0 col-md-12 col-sm-12 col-space text-center blank-element">
        <div href="#">
           <a class="b24-web-form-popup-btn-15" >
             <h2 style="padding-top: 30px;  margin-bottom: 25px;">Не нашли нужную услугу?</h2>
             <div class="description" style="margin-bottom:0px">
          <span style="color:#333">Опишите задачу, которую нужно решить, и мы найдём оптимальное решение</span>
        </div>
<button class="webform-services">
Написать</button>
                </a>
        </div>
      </div>
  </div>
</div>
</section>

<style>

</style>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>