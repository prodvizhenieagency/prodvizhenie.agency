<?
global $USER;
if ($USER->IsAdmin()) {
    ?>
<?php } else { ?>
    <div class="baner">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-xs-12">
                    <h1 class="wow fadeInLeft" data-wow-delay="1s"> Помогаем компаниям <br> развиваться</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 col-sm-12">
                    <div class="row">
                        <div class="col-xs-12 wow fadeInLeft" data-wow-delay="1s">
                            <p class="baner-disc">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => "/include/prbel/main_banner.php",
                                    "EDIT_TEMPLATE" => ""
                                ),
                                    false
                                );?>
                            </p>
                        </div>

                        <div class="col-xs-12 text-center visible-xs-block">
                            <img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/leptop-bg.png" alt="leptop" class="img-responsive">
                        </div>
                    </div>

                    <div class="row number-wrap wow fadeInLeft" data-wow-delay="1s">
                        <div class="col-xs-4 text-center count-1">
                            <div class="row">
                                <div class="col-number">
                                    <p class="count">1</p>
                                </div>
                                <div class="col-text">
                                    <p>Место среди <br>SEO-компаний <br>в Белгороде</p>
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-5 text-center " >
                            <div class="row">
                                <div class="col-number">
                                    <p class="count-2">98</p>
                                </div>
                                <div class="col-text">
                                    <p>
                                        Успешно<br />
                                        запущенных <br />
                                        проектов
                                    </p>
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-3 text-center" >
                            <div class="row">
                                <div class="col-number">
                                    <p class="count">5</p>
                                </div>
                                <div class="col-text">
                                    <p>Лет<br/> успешной <br>работы</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-5 bg-laptop col-sm-12 hidden-xs ">

                </div>
            </div>
        </div>
    </div>
<?php } ?>