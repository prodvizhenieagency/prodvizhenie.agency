<?
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "⭐️Digital-агентство Продвижение в #WF_CITY_PRED# занимается комплексным интернет-маркетингом. Мы разрабатываем сайты, оптимизируем их и настраиваем контекстную рекламу");
$APPLICATION->SetPageProperty("title", "О компании — Digital-агентство «Продвижение» – #WF_CITY_VIN#");
    $APPLICATION->SetTitle("О компании");
?><div class="banner-about">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1><? $APPLICATION->ShowTitle(false);?>&nbsp;</h1>
                <p class="baner-disc">
                     Сильная компания — команда сильных людей. Решаем задачи бизнеса и увеличиваем продажи с помощью интернет-маркетинга. Следуем концепции Performance: прозрачный и измеримый результат
                </p>
            </div>
        </div>
        <div class="row krosh">
             <?$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "schema",
Array()
);?>
        </div>
    </div>
</div>
<div class="direction">
    <div class="container">
        <div class="row row-sapce">
            <div class="col-sm-6 text-center">
 <img alt="Команда на фотосъемке в студии" src="/upload/medialibrary/775/7750e3c3025e78dd7fd439b58c50ba92.jpg" class="img-responsive">
            </div>
            <div class="col-sm-6 text-center">
 <img alt="Празднуем Новый 2018-ый год в офисе" src="/upload/medialibrary/0de/0dee0b0f9b3f086e32abf76f5104296a.jpg" class="img-responsive">
            </div>
            <div class="col-sm-6 text-center">
 <img alt="Флаг на вершине Эльбрус" src="/local/templates/prbel/img/page/team-elbrus-2018.jpg" class="img-responsive">
            </div>
            <div class="col-sm-6 text-center">
 <img alt="Стикер на горе Ай-Петри " src="/local/templates/prbel/img/page/team-krime-2018.jpg" class="img-responsive">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-offset-2 col-lg-10">
                 <?$APPLICATION->IncludeComponent(
    "bitrix:news.detail",
    "contact",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_ELEMENT_CHAIN" => "Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "BROWSER_TITLE" => "-",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "contact",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "ELEMENT_CODE" => "",
        "ELEMENT_ID" => "135",
        "FIELD_CODE" => array(0=>"",1=>"",),
        "IBLOCK_ID" => "27",
        "IBLOCK_TYPE" => "aspro_digital_content",
        "IBLOCK_URL" => "",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "MESSAGE_404" => "",
        "META_DESCRIPTION" => "-",
        "META_KEYWORDS" => "-",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Страница",
        "PROPERTY_CODE" => array(0=>"",1=>"",),
        "SET_BROWSER_TITLE" => "Y",
        "SET_CANONICAL_URL" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "USE_PERMISSIONS" => "N",
        "USE_SHARE" => "N"
    )
);?>
            </div>
        </div>
    </div>
</div>
<div class="cta-banner">
    <div class="container">
        <div class="col-xs-12">
             Сертификаты подтверждают высокий уровень знаний и навыков для профессионального ведения рекламных кампаний в Яндекс.Директ и Google Ads <br>
 <section class="certificate text-center">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
 <img width="110" alt="Сертифицированное агентство" src="https://avatars.mds.yandex.net/get-adv/114583/2a0000015d60261ab2a5916f9d25bcdd552b/orig" height="110" border="0">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
 <a href="https://www.google.com/partners/agency?id=2113442239&roistat_visit=61346" rel="nofollow" target="_blank"> <img src="/local/templates/prbel/img/sert/google-min.png" srcset="/local/templates/prbel/img/sert/google-min.png" alt=""> </a>
            </div>
 </section>
            <!--     <script src="https://apis.google.com/js/platform.js" async defer></script>
            <div class="g-partnersbadge" data-agency-id="2113442239">
            </div>-->
        </div>
    </div>
</div>
<div class="why-we-the-one">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2>
                Наши достижения в 2020 году</h2>
            </div>
            <br>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 img-wrap">
 <img width="75%" src="/markup/img/page/first.png" alt="">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 <br>
                <div class="why-wrap">
                    <div class="why-title">
                         «Рейтинг ведущих SEO-компаний Белгорода»
                    </div>
                    <div class="why-text">
                    </div>
                </div>
                <div class="why-wrap">
                    <div class="why-title">
                         «Рейтинг агентств контекстной рекламы в Белгороде»
                    </div>
                    <div class="why-text">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    Array(
        "AREA_FILE_SHOW" => "file",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/prbel/slider_about.php"
    )
);?>

 <?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    Array(
        "AREA_FILE_SHOW" => "file",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/prbel/service_tale.php"
    )
);?>
<div class="why-profit">
    <div class="container">
        <div class="row text-center">
            <h2>
            Чем мы можем быть вам полезны </h2>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-2 col-md-10 col-md-offset-2 col-sm-10 col-sm-offset-2">
                <div class="row">
                     <!--<div class="col-lg-1 col-md-1 col-sm-2">
                        <div class="img-wrap">
 <img src="/markup/img/page/check.svg" alt="" class="img-responsive">
                        </div>
                    </div>-->
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        <div class="profit-title">
                             Высокое качество услуг
                        </div>
                        <div class="profit-text">
                             В направлении интернет-маркетинга мы — ведущая компания #WF_CITY_ROD#. Нас ценят за профессионализм, результативность и опыт
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        <div class="profit-title">
                             Поддержка проектов на всех этапах
                        </div>
                        <div class="profit-text">
                             Мы проконсультируем вас при личной встрече, в нашем офисе, по телефону или онлайн. Готовы курировать проект даже после выполнения своих обязанностей.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        <div class="profit-title">
                             Индивидуальный подход
                        </div>
                        <div class="profit-text">
                             Наша команда работает по принципу индивидуального подхода к каждому клиенту. Мы полностью погружаемся в специфику бизнеса, знаем ваши проблемы и желания, благодаря чему достигаем лучший результат.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    Array(
        "AREA_FILE_SHOW" => "file",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/prbel/word_lead.php"
    )
);?>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    Array(
        "AREA_FILE_SHOW" => "file",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/prbel/trust.php"
    )
);?>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    Array(
        "AREA_FILE_SHOW" => "file",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/prbel/contact_us.php"
    )
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>