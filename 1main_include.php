<div>
		<div class="owl-carousel owl-theme" id="banner-main">
			<div class="item">
				<div class="baner">
					<div class="container">
						<div class="row">
							<div class="col-md-10 col-sm-12 col-xs-120 " 
">
								<span claas="banerr">Развиваем ваш <br> диджитал-маркетинг</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8 col-sm-12">
								<div class="row">
									<div class="col-xs-12" >
										<p class="baner-disc">
											<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
												"AREA_FILE_SHOW" => "file",
												"PATH" => "/include/prbel/main_banner.php",
												"EDIT_TEMPLATE" => ""
											),
												false
											);?>
										</p>
									</div>

									<div class="col-xs-12 text-center visible-xs-block">
										<img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/leptop-bg.png" alt="leptop" class="img-responsive">
									</div>
								</div>
								
								<div class="row number-wrap">
									<div class="col-xs-6 text-center count-1">
										<div class="row">
											<div class="col-number">
												<p class="count">17</p>
											</div>
											<div class="col-text">
												<p>место в рейтинге <br> SEO <br>глазами клиентов</p>
											</div>
										</div>
									</div>

									<div class="col-xs-6 text-center " >
										<div class="row">
											<div class="col-number">
												<p class="count-2">50</p>
											</div>
											<div class="col-text">
												<p>рейтинг известности <br>бренда SEO-компаний<br> России</p>
											</div>
										</div>
									</div>


									<!--<div class="col-xs-3 text-center" >
										<div class="row">
											<div class="col-number">
												<p class="count">7</p>
											</div>
											<div class="col-text">
												<p>Лет<br/> роста <br>и развития</p>
											</div>
										</div>
									</div>-->
								</div>
							</div>

							<div class="col-md-4 bg-laptop col-sm-12 hidden-xs ">

							</div>
						</div>
					</div>
				</div>

			</div>
<!--
			<div class="item audit-site">
				<div class="baner">
					<div class="container">
						<div class="row">
							<div class="col-md-10 col-sm-12 col-xs-12">
								<span>Аудит эффективности сайта</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-7 col-sm-12">
								<div class="row">
									<div class="col-xs-12" >
										<p class="baner-disc">
											Аудит эффективности показывает, насколько сайт продающий, удобный для пользователей, содержит ли он технические ошибки и почему не попадает в ТОП. Мы бесплатно сделаем анализ и разработаем пути развития.
										</p>
									</div>
									<div class="col-xs-12">
										<a class="btn my-btn-3" href="/services/audit-sayta/">Заказать аудит сайта</a>
									</div>

									<div class="col-xs-12 text-center visible-xs-block">
										<img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/leptop-bg.png" alt="leptop" class="img-responsive">
									</div>
								</div>
							</div>
							<div class="col-md-6 bg-laptop col-sm-12 hidden-xs "></div>
						</div>
					</div>
				</div>

			</div>
			<div class="item audit-reklama">
				<div class="baner">
					<div class="container">
						<div class="row">
							<div class="col-md-10 col-sm-12 col-xs-12">
								<span >Аудит контекстной  рекламы</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-7 col-sm-12">
								<div class="row">
									<div class="col-xs-12">
										<p class="baner-disc">
											Аудит жизненно необходим, чтобы проанализировать опыт прошлых рекламных кампаний и определить новый вектор работы. Мы бесплатно проведем исследование и дадим рекомендации по улучшению.
										</p>
									</div>
								<div class="col-xs-12">
									<a class="btn my-btn-3" href="/services/audit-reklamy/">Оставить заявку на аудит</a>
								</div>
									<div class="col-xs-12 text-center visible-xs-block">
										<img src="<?= SITE_TEMPLATE_PATH; ?>/img/main/leptop-bg.png" alt="leptop" class="img-responsive">
									</div>
								</div>
							</div>

							<div class="col-md-6 bg-laptop col-sm-12 hidden-xs ">

							</div>
						</div>
					</div>
				</div>

			</div>
-->
		</div>
	</div>


<div class="advantage">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<h2>Диджитал-агентство, которое рекомендуют друзьям</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 col-sm-4 text-center adv-item wow fadeInLeft" data-wow-delay="0.5s">
				<div class="adv-icon">
					<p>1</p>
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					     viewBox="0 0 95 95" style="enable-background:new 0 0 95 95;" xml:space="preserve">
							<style type="text/css">
								.st0{fill:#F75831;}
								.st1{fill:#FFFFFF;}
							</style>
						<g>
							<circle class="st0" cx="47.5" cy="47.5" r="47.5"/>
							<g>
								<path class="st1" d="M61.8,45.7V21.5c0-0.6-0.4-1-1-1h-9.1c-0.6,0-1,0.4-1,1v28.8c-1.5,1.5-2.6,3.3-3.4,5.3v-23c0-0.6-0.4-1-1-1
										h-9.1c-0.6,0-1,0.4-1,1v33.1h-3.4V42.9c0-0.6-0.4-1-1-1h-9.1c-0.6,0-1,0.4-1,1v22.9h-0.8c-0.6,0-1,0.4-1,1s0.4,1,1,1h27
										c2.5,5.1,7.8,8.7,13.8,8.7c8.5,0,15.4-6.9,15.4-15.4C77.1,52.6,70.2,45.8,61.8,45.7z M59.8,22.5v23.3c-2.6,0.3-5.1,1.3-7.1,2.8
										V22.5H59.8z M38.2,33.7h7.1v32.1h-7.1V33.7z M23.7,43.9h7.1v21.9h-7.1V43.9z M61.7,74.5c-7.4,0-13.4-6-13.4-13.4s6-13.4,13.4-13.4
										s13.4,6,13.4,13.4S69.1,74.5,61.7,74.5z"/>
								<path class="st1" d="M68.2,54.6c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1c0-0.1-0.1-0.1-0.1-0.2
										C67.8,54,67.8,54,67.7,54c0,0-0.1-0.1-0.1-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0h0c0,0,0,0,0,0H57c-0.6,0-1,0.4-1,1s0.4,1,1,1
										h7.6c-1.5,1.4-3.7,3.8-7.3,7.6c-1.2,1.3-2.2,2.3-2.6,2.7c-0.4,0.4-0.4,1,0,1.4c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3
										c0.4-0.4,1.4-1.4,2.6-2.7c1.9-2.1,5.5-5.8,7.5-7.8v8c0,0.6,0.4,1,1,1s1-0.4,1-1V54.8c0,0,0,0,0,0C68.2,54.7,68.2,54.6,68.2,54.6z"
								/>
							</g>
						</g>
							</svg>
				</div>
				<p>Используем оптимальные стратегии и&nbsp;эффективные инструменты</p>
			</div>
			<div class="col-md-4 col-sm-4 text-center adv-item wow fadeInLeft" data-wow-delay="0.5s">
				<div class="adv-icon">
					<p>2</p>
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					     viewBox="0 0 95 95" style="enable-background:new 0 0 95 95;" xml:space="preserve">
							<style type="text/css">
								.st0{fill:#F75831;}
								.st1{fill:#FFFFFF;}
							</style>
						<g>
							<circle class="st0" cx="47.5" cy="47.5" r="47.5"/>
							<path class="st1" d="M70.6,29.7C69,28,67,26.4,65,25.2c-2.9-1.6-11.2-3.5-16.6,1.7L46.2,29l-1.7-1.7c-6.4-6.2-15.2-4-20.6,1.5
									c-5.1,5.2-7.7,13.7-1.3,20.2l3.2,3.2l-0.8,0.8c-1.7,1.7-1.7,4.3,0,6l0.4,0.4c1,1,2.4,1.4,3.8,1.2c0,0.2,0,0.3,0,0.5
									c0,1.1,0.5,2.2,1.3,3l0.4,0.4c0.8,0.8,1.8,1.3,2.9,1.3c0.3,0,0.6,0,1-0.1c0,0.1,0,0.1,0,0.2c0,1.1,0.5,2.2,1.3,3l0.4,0.4
									c1.1,1.1,2.5,1.5,3.8,1.2c0,0.1,0,0.2,0,0.3c0,1.1,0.5,2.2,1.3,3l0.4,0.4c0.8,0.8,1.8,1.3,2.9,1.3c0,0,0.1,0,0.1,0
									c1.1,0,2.2-0.5,3-1.3l3.3-3.3c0.5-0.5,0.9-1.1,1.1-1.8c0.8,0.8,1.9,1.3,3,1.3c0.4,0,0.7,0,1.1-0.1c1.6-0.4,3-1.6,3.4-3.2
									c0.1-0.3,0.1-0.7,0.1-1c0.3,0.1,0.6,0.1,0.9,0.1c0.4,0,0.7,0,1.1-0.1c1.7-0.4,3.1-1.7,3.5-3.3c0.1-0.5,0.2-1,0.1-1.4
									c0.1,0,0.2,0,0.3,0c0.3,0,0.6,0,0.9-0.1c1.5-0.4,2.8-1.7,3.3-3.3c0.4-1.5,0.1-3-0.9-4.1l3.2-3.2C78.7,44.4,76.1,35.6,70.6,29.7z
									 M30.1,58.1c-0.9,0.9-2.3,0.9-3.2,0l-0.4-0.4c-0.9-0.9-0.9-2.3,0-3.2l4.9-4.9c0.5-0.4,1-0.6,1.6-0.6c0.6,0,1.2,0.2,1.6,0.6L35,50
									c0.4,0.4,0.7,1.1,0.7,1.7c0,0.6-0.2,1.1-0.7,1.5L30.1,58.1z M35.5,63.2c-0.4,0.4-1,0.7-1.7,0.7c-0.6,0-1.1-0.2-1.5-0.7l-0.4-0.4
									c-0.4-0.4-0.7-1-0.7-1.6s0.2-1.2,0.7-1.6l7.7-7.7c0.4-0.4,0.9-0.6,1.5-0.7c0,0,0,0,0,0c0.6,0,1.2,0.2,1.6,0.7c0,0,0,0,0,0l0.4,0.4
									c0.4,0.4,0.7,1.1,0.7,1.7c0,0.6-0.2,1.1-0.7,1.5L35.5,63.2z M41,68c-0.9,0.9-2.3,0.9-3.2,0l-0.4-0.4c-0.4-0.4-0.7-1-0.7-1.6
									s0.2-1.2,0.7-1.6l5.3-5.3c0.4-0.4,1-0.7,1.6-0.7c0,0,0,0,0,0c0.6,0,1.1,0.2,1.5,0.7c0,0,0,0,0,0l0.4,0.4c0.9,0.9,0.9,2.3,0,3.2
									L41,68z M49.8,69.6l-3.3,3.3c-0.4,0.4-1,0.7-1.7,0.7c-0.6,0-1.1-0.2-1.5-0.7l-0.4-0.4c-0.4-0.4-0.7-1-0.7-1.6
									c0-0.6,0.2-1.2,0.7-1.6l3.3-3.3c0.4-0.4,0.9-0.6,1.5-0.7c0,0,0,0,0,0c0.3,0,0.6,0.1,0.9,0.2c0,0.1,0.1,0.1,0.1,0.2l1.5,1.5
									c0.1,0.3,0.2,0.6,0.2,0.8C50.5,68.7,50.2,69.2,49.8,69.6z M67.9,52.4l-12.4-13c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4
									l13.6,14.3c0.8,0.8,0.7,1.8,0.5,2.3c-0.3,1-1,1.7-1.8,1.9c-0.9,0.2-1.7-0.3-2.2-0.9l-6.4-6.6c0,0,0,0,0,0l-0.5-0.5
									c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4l6.8,7l0.1,0.1c0,0,0,0,0,0l0,0c0.7,0.8,1,1.6,0.8,2.4c-0.2,0.9-1.1,1.7-2.1,1.9
									c-0.5,0.1-1.4,0.2-2.2-0.6l-7-7.2c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4l6,6.2c0,0.1,0.1,0.2,0.2,0.3c0.8,0.8,1,1.6,0.8,2.3
									c-0.2,0.8-1,1.5-2,1.8c-0.5,0.1-1.4,0.2-2.2-0.6L52,66c-0.2-0.4-0.4-0.7-0.7-1l-0.4-0.4c-0.7-0.7-1.6-1.1-2.5-1.2
									c1.1-1.6,0.9-3.8-0.6-5.3l-0.4-0.4c-0.6-0.7-1.4-1.1-2.3-1.2c0.5-0.7,0.9-1.5,0.9-2.5c0-1.2-0.4-2.3-1.3-3.1l-0.4-0.4
									c-0.8-0.8-2-1.3-3.1-1.3c-1.1,0-2.2,0.5-2.9,1.3l-0.6,0.6c-0.1-0.9-0.6-1.8-1.2-2.5L36,48.2c0,0,0,0,0,0c-1.7-1.5-4.3-1.5-6,0
									l-2.7,2.7l-3.2-3.2c-5.5-5.5-3.2-12.9,1.3-17.4c4.6-4.6,12.1-7.1,17.8-1.5l1.6,1.6l-7,7c-1.1,1.1-1,2.1-0.9,2.6
									c0.3,1.1,1.5,2.1,3.1,2.5c2,0.5,5.1,0.4,7.2-1.7l2.7-2.6c1.3-1.2,2.7-0.9,5.1-0.1c2.8,0.8,6.2,1.9,10-1.1c0.4-0.3,0.5-1,0.2-1.4
									c-0.3-0.4-1-0.5-1.4-0.2c-3,2.3-5.5,1.6-8.2,0.8c-2.4-0.7-4.8-1.5-7,0.6l-2.7,2.6c-1.5,1.5-3.8,1.6-5.3,1.2c-1-0.3-1.6-0.8-1.7-1.1
									c-0.1-0.2,0.2-0.5,0.4-0.7l7.5-7.5c0.1,0,0.2-0.1,0.3-0.2s0.2-0.2,0.2-0.3l2.6-2.6c5-4.8,12.6-2.2,14.2-1.4
									c1.8,1.1,3.7,2.5,5.2,4.1c4.4,4.8,7.6,12.5,1.8,18.2L67.9,52.4z"/>
						</g>
							</svg>
				</div>
				<p>Нам доверяют самые требовательные клиенты&nbsp;&mdash; мы&nbsp;гордимся этим</p>
			</div>
			<div class="col-md-4 col-sm-4 text-center adv-item wow fadeInLeft" data-wow-delay="0.5s">
				<div class="adv-icon">
					<p>3</p>
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					     viewBox="0 0 95 95" style="enable-background:new 0 0 95 95;" xml:space="preserve">
						<style type="text/css">
							.st0{fill:#F75831;}
							.st1{fill:#FFFFFF;}
						</style>
						<g>
							<circle class="st0" cx="47.5" cy="47.5" r="47.5"/>
							<g>
								<path class="st1" d="M74.1,68.5L72,60.2c-0.2-1.2-1.2-2.1-2.3-2.1h-4.6c0-0.2,0-0.4,0-0.6L63,49.2c-0.2-1.2-1.2-2.1-2.3-2.1H56
									c0-0.2,0-0.4,0-0.6l-2.1-8.3c-0.2-1.2-1.2-2.1-2.3-2.1h-9.4c-1.1,0-1.9,0.7-2.3,2.1l-2.1,8.3c-0.1,0.2-0.1,0.5,0,0.7h-3.9
									c-1.1,0-2,0.8-2.2,2.1l-2.1,8.2c-0.1,0.2-0.1,0.5,0,0.7h-3.9c-1.3,0-2.1,1.1-2.3,2.1l-2.1,8.3c-0.2,0.6,0,1.1,0.3,1.6
									c0.4,0.6,1.2,1,1.9,1h13.5c0.7,0,1.5-0.4,1.9-0.9c0.4,0.6,1.2,0.9,1.9,0.9h13.6c0.7,0,1.4-0.3,1.9-0.9c0.4,0.5,1.2,0.9,1.9,0.9
									h13.6c0.7,0,1.5-0.4,1.9-1C74.1,69.6,74.2,69,74.1,68.5z M56.4,66.8l-1.7-6.6c0-0.1,0-0.1,0-0.2h3.4c0,0.1,0,0.1,0,0.2L56.4,66.8z
									 M39,67l-1.7-6.8c0-0.1,0-0.1,0-0.2h3.5c0,0.1,0,0.1,0,0.2L39,67z M47.3,56l-1.7-6.8c0-0.1,0-0.1,0-0.2h3.5c0,0.1,0,0.1,0,0.2
									L47.3,56z M61,49.5c0,0,0,0.1,0,0.1l2.1,8.2C63,57.9,62.9,58,62.8,58H49.2c-0.1,0-0.2-0.1-0.3-0.1l2.1-8.2c0,0,0-0.1,0-0.1
									c0.1-0.4,0.2-0.4,0.3-0.4h9.4C60.9,49.1,61,49.3,61,49.5z M41.9,38.7c0.1-0.3,0.2-0.6,0.3-0.6h9.4c0.2,0,0.3,0.2,0.3,0.4
									c0,0,0,0.1,0,0.1l2.1,8.2c-0.1,0.1-0.2,0.1-0.3,0.1H40.1c-0.1,0-0.2-0.1-0.3-0.1L41.9,38.7z M33.7,49.6c0,0,0-0.1,0-0.1
									c0-0.2,0.1-0.4,0.2-0.4h9.4c0.2,0,0.3,0.2,0.3,0.4c0,0,0,0.1,0,0.1l2.1,8.2c-0.1,0.1-0.2,0.1-0.3,0.1H31.9c-0.1,0-0.2-0.1-0.3-0.1
									L33.7,49.6z M37.1,69H23.6c-0.1,0-0.2-0.1-0.3-0.1l2.1-8.3c0,0,0.1-0.5,0.3-0.5H35c0.2,0,0.3,0.2,0.3,0.4c0,0,0,0.1,0,0.1l2.1,8.2
									C37.3,68.9,37.2,69,37.1,69z M54.5,69H40.9c-0.1,0-0.2-0.1-0.3-0.1l2.1-8.3c0,0,0.1-0.5,0.3-0.5h9.4c0.2,0,0.3,0.2,0.3,0.4
									c0,0,0,0.1,0,0.1l2.1,8.2C54.7,68.9,54.6,69,54.5,69z M71.8,69H58.2c-0.1,0-0.2-0.1-0.3-0.1l2.1-8.3c0-0.2,0.2-0.5,0.3-0.5h9.4
									c0.2,0,0.3,0.2,0.3,0.4c0,0,0,0.1,0,0.1l2.1,8.2C72,68.9,71.9,69,71.8,69z"/>
								<path class="st1" d="M47,30.2c0.6,0,1-0.4,1-1v-6.9c0-0.6-0.4-1-1-1s-1,0.4-1,1v6.9C46,29.8,46.4,30.2,47,30.2z"/>
								<path class="st1" d="M57.7,32.9C57.8,33,58,33,58.2,33c0.3,0,0.7-0.2,0.9-0.5l3.5-5.9c0.3-0.5,0.1-1.1-0.4-1.4
									c-0.5-0.3-1.1-0.1-1.4,0.3l-3.5,5.9C57.1,32,57.2,32.6,57.7,32.9z"/>
								<path class="st1" d="M66.5,41.1c0.2,0,0.3,0,0.5-0.1l6-3.4c0.5-0.3,0.6-0.9,0.4-1.4c-0.3-0.5-0.9-0.6-1.4-0.4l-6,3.4
									c-0.5,0.3-0.6,0.9-0.4,1.4C65.8,40.9,66.2,41.1,66.5,41.1z"/>
								<path class="st1" d="M35,32.8c0.2,0.3,0.5,0.5,0.9,0.5c0.2,0,0.3,0,0.5-0.1c0.5-0.3,0.6-0.9,0.4-1.4l-3.4-5.9
									c-0.3-0.5-0.9-0.6-1.4-0.4c-0.5,0.3-0.6,0.9-0.4,1.4L35,32.8z"/>
								<path class="st1" d="M27.3,41.5c0.2,0.1,0.3,0.1,0.5,0.1c0.3,0,0.7-0.2,0.9-0.5c0.3-0.5,0.1-1.1-0.4-1.4l-5.9-3.4
									c-0.5-0.3-1.1-0.1-1.4,0.4c-0.3,0.5-0.1,1.1,0.4,1.4L27.3,41.5z"/>
							</g>
						</g>
						</svg>
				</div>
				<p>Увеличиваем продажи и&nbsp;возвращаем инвестиции в&nbsp;маркетинг</p>
			</div>

		</div>
	</div>
</div>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/service_tale.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/result_slider.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news", 
	"projects_main", 
	array(
		"IBLOCK_TYPE" => "aspro_digital_content",
		"IBLOCK_ID" => "35",
		"NEWS_COUNT" => "6",
		"USE_SEARCH" => "N",
		"USE_RSS" => "N",
		"USE_RATING" => "N",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"FILTER_NAME" => "arProjectFilter",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ID",
		"SORT_ORDER2" => "DESC",
		"CHECK_DATES" => "Y",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/projects/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "100000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"USE_PERMISSIONS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"LIST_ACTIVE_DATE_FORMAT" => "j F Y",
		"LIST_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "DETAIL_PICTURE",
			3 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "LINK_TO_CASE",
			1 => "",
		),
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"DISPLAY_NAME" => "N",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",
		"DETAIL_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "DETAIL_TEXT",
			3 => "DETAIL_PICTURE",
			4 => "",
		),
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "FORM_ORDER",
			2 => "FORM_QUESTION",
			3 => "ORDERER",
			4 => "SITE",
			5 => "DATA",
			6 => "AUTHOR",
			7 => "LINK_PROJECTS",
			8 => "TASK_PROJECT",
			9 => "LINK_COMPANY",
			10 => "LINK_REVIEWS",
			11 => "LINK_GOODS",
			12 => "LINK_SERVICES",
			13 => "FORM_PROJECT",
			14 => "DOCUMENTS",
			15 => "PHOTOS",
			16 => "",
		),
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"IMAGE_POSITION" => "left",
		"USE_SHARE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"USE_REVIEW" => "N",
		"ADD_ELEMENT_CHAIN" => "N",
		"SHOW_DETAIL_LINK" => "Y",
		"S_ASK_QUESTION" => "",
		"S_ORDER_PROJECT" => "Заказать проект",
		"T_GALLERY" => "",
		"T_DOCS" => "",
		"T_PROJECTS" => "Похожие проекты",
		"T_CHARACTERISTICS" => "",
		"COMPONENT_TEMPLATE" => "projects_main",
		"SET_LAST_MODIFIED" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_SET_CANONICAL_URL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"FORM_ID" => "14",
		"GALLERY_TYPE" => "big",
		"T_GOODS" => "",
		"T_SERVICES" => "",
		"T_REVIEWS" => "Отзыв клиента",
		"SECTION_ELEMENTS_TYPE_VIEW" => "FROM_MODULE",
		"ELEMENT_TYPE_VIEW" => "element_1",
		"LINE_ELEMENT_COUNT" => "2",
		"LINE_ELEMENT_COUNT_LIST" => "3",
		"SHOW_SECTION_PREVIEW_DESCRIPTION" => "Y",
		"SHOW_SECTION_DESCRIPTION" => "Y",
		"S_ORDER_SERVISE" => "Заказать проект",
		"SHOW_NEXT_ELEMENT" => "Y",
		"T_NEXT_LINK" => "Следующий проект",
		"T_PREV_LINK" => "",
		"FORM_ID_ORDER_SERVISE" => "14",
		"IMAGE_WIDE" => "Y",
		"DETAIL_STRICT_SECTION_CHECK" => "N",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_BRAND_USE" => "Y",
		"DETAIL_BRAND_PROP_CODE" => array(
			0 => "TIZERS",
			1 => "",
		),
		"T_CLIENTS" => "",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"SHARE_HIDE" => "N",
		"SHARE_TEMPLATE" => "",
		"SHARE_HANDLERS" => array(
			0 => "delicious",
			1 => "mailru",
			2 => "facebook",
			3 => "twitter",
			4 => "lj",
			5 => "vk",
		),
		"SHARE_SHORTEN_URL_LOGIN" => "",
		"SHARE_SHORTEN_URL_KEY" => "",
		"STRICT_SECTION_CHECK" => "N",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "#SECTION_CODE_PATH#/",
			"detail" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
		)
	),
	false
);?>
	<section class="blog-main">
		<div class="orange-title">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h2 class="text-center">События и статьи для самообразования</h2>
					</div>
				</div>
			</div>
		</div>
		<?$APPLICATION->IncludeComponent(
	"bitrix:news", 
	"blog_main", 
	array(
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_ACTIVE_DATE_FORMAT" => "f j, Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "TAGS",
			5 => "SORT",
			6 => "PREVIEW_TEXT",
			7 => "PREVIEW_PICTURE",
			8 => "DETAIL_TEXT",
			9 => "DETAIL_PICTURE",
			10 => "DATE_ACTIVE_FROM",
			11 => "ACTIVE_FROM",
			12 => "DATE_ACTIVE_TO",
			13 => "ACTIVE_TO",
			14 => "SHOW_COUNTER",
			15 => "SHOW_COUNTER_START",
			16 => "IBLOCK_TYPE_ID",
			17 => "IBLOCK_ID",
			18 => "IBLOCK_CODE",
			19 => "IBLOCK_NAME",
			20 => "IBLOCK_EXTERNAL_ID",
			21 => "DATE_CREATE",
			22 => "CREATED_BY",
			23 => "CREATED_USER_NAME",
			24 => "TIMESTAMP_X",
			25 => "MODIFIED_BY",
			26 => "USER_NAME",
			27 => "",
		),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "BLOG_POST_ID",
			1 => "BIG_BLOCK",
			2 => "BLOG_COMMENTS_CNT",
			3 => "POSITION_BLOCK",
			4 => "PHOTOPOS",
			5 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "24",
		"IBLOCK_TYPE" => "aspro_digital_content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"LIST_ACTIVE_DATE_FORMAT" => "f j, Y",
		"LIST_FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "TAGS",
			5 => "SORT",
			6 => "PREVIEW_TEXT",
			7 => "PREVIEW_PICTURE",
			8 => "DETAIL_TEXT",
			9 => "DETAIL_PICTURE",
			10 => "DATE_ACTIVE_FROM",
			11 => "ACTIVE_FROM",
			12 => "DATE_ACTIVE_TO",
			13 => "ACTIVE_TO",
			14 => "SHOW_COUNTER",
			15 => "SHOW_COUNTER_START",
			16 => "IBLOCK_TYPE_ID",
			17 => "IBLOCK_ID",
			18 => "IBLOCK_CODE",
			19 => "IBLOCK_NAME",
			20 => "IBLOCK_EXTERNAL_ID",
			21 => "DATE_CREATE",
			22 => "CREATED_BY",
			23 => "CREATED_USER_NAME",
			24 => "TIMESTAMP_X",
			25 => "MODIFIED_BY",
			26 => "USER_NAME",
			27 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "BLOG_POST_ID",
			1 => "BIG_BLOCK",
			2 => "BLOG_COMMENTS_CNT",
			3 => "POSITION_BLOCK",
			4 => "PHOTOPOS",
			5 => "",
		),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "6",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_FOLDER" => "/blog/",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "blog_main",
		"FILE_404" => "",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "#SECTION_CODE_PATH#/",
			"detail" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
		)
	),
	false
);?>
	</section>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/feedback_slider.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/trust.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/contact_us.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/certificate.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>