<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "⭐️ Вакансии digital-агентства Продвижение. Разрабатываем сайты, оптимизируем их и настраиваем контекстную рекламу");
$APPLICATION->SetPageProperty("title", "Вакансии — Digital-агентство «Продвижение» – #WF_CITY_VIN#");
	$APPLICATION->SetTitle("Вакансии");
?><div class="banner-about">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<h1><? $APPLICATION->ShowTitle(false);?></h1>
				<p class="baner-disc">
					 Привет! Мы развиваемся. Собираем команду сильных специалистов с амбициями и ценностями. Ниже ты узнаешь о наших подходах к работе.
				</p>
			</div>
		</div>
		<div class="row krosh">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"schema",
Array()
);?>
		</div>
	</div>
</div>
<div class="photo-office">
	<div class="container">
		
		<div class="row row-sapce">
			<div class="col-lg-6 col-md-6 col-md-6 col-xs-12 vertical">
 <img src="/upload/medialibrary/e73/e7359ddbd2bfa8aaf2207e6f6ae6c890.JPG" alt="">
			</div>
			<div class="col-lg-6 col-md-6 col-md-6 col-xs-12 extra-padding">
				<div class="row">
					<div class="col-xs-12">
 <img src="/upload/medialibrary/c66/c66ae2d48faad58c03d9508b6a0fb924.JPG" alt="" class="img-responsive">
					</div>
					<div class="col-xs-12">
 <img src="/upload/medialibrary/444/44465c3ffee10e55c90003935cb1abd8.jpg" alt="" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"vacancy",
	Array(
		"51" => fals,
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"DETAIL_TEXT",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "aspro_digital_content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	)
);?>
<div class="word-lead">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
				<div class="word-wrap">
					 Мы ищем эрудированных, вдохновлённых и трудолюбивых людей, которые хотят строить технологии, помогающие решать реальные проблемы людей.
				</div>
				<div class="bottom-line">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
				<div class="lead-wrap">
					<div class="row">
						<div class="author-img">
 <img src="/images/Natasha-min.png" alt="" class="img-responsive">
						</div>
						<div class="author-name">
							<div class="name">
								 Наталья Кудлаева
							</div>
							<div class="status">
								 HR-специалист
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="advantage-for-work">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h2>Работать в агентстве — интересно</h2>
			</div>
		</div>
		<div class="row row-space">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 icon-wrap">
 <img src="/local/templates/prbel/img/page/adv-icon-1.svg" alt="">
					</div>
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
						<div class="icon-title">
							 Развитие, рост и опыт
						</div>
						<div class="icon-text">
							 Получайте знания и опыт работы в ведущем digital-агентстве города. Много интересных задач.
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 icon-wrap">
 <img src="/local/templates/prbel/img/page/adv-icon-2.svg" alt="">
					</div>
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
						<div class="icon-title">
							 Комфортное рабочее место и график
						</div>
						<div class="icon-text">
							 Мощный компьютер, платные сервисы, все нужные ресурсы. На кухне — всегда вкусные мелочи.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row row-space">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 icon-wrap">
 <img src="/local/templates/prbel/img/page/adv-icon-3.svg" alt="">
					</div>
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
						<div class="icon-title">
							 Поддержка и хорошее настроение
						</div>
						<div class="icon-text">
							 В офисе всегда весело, а если нужно перевезти шкаф — мы тоже рядом
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 icon-wrap">
 <img src="/local/templates/prbel/img/page/adv-icon-4.svg" alt="">
					</div>
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
						<div class="icon-title">
							 Культура Modern Agile
						</div>
						<div class="icon-text">
							 Коллеги достигают выдающихся результатов. <br>
							 Подробнее <a href="/blog/research/modern-agile/" target="_blank"> в блоге</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row row-space">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 icon-wrap">
 <img src="/local/templates/prbel/img/page/adv-icon-5.svg" alt="">
					</div>
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
						<div class="icon-title">
							 Праздники и печеньки
						</div>
						<div class="icon-text">
							 Мы не только вместе работаем, но и отдыхаем. Подписывайся на <a href="https://www.instagram.com/prbel/" target="_blank">Инстаграм</a>, <a href="https://t.me/prbelnews" target="_blank">Телеграм</a>, <a href="https://www.facebook.com/prodvizheniebel/" target="_blank">FB</a> или <a href="https://vk.com/prbel" target="_blank">ВК</a>!
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 icon-wrap">
 <img src="/local/templates/prbel/img/page/adv-icon-6.svg" alt="">
					</div>
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
						<div class="icon-title">
							 Сверхбыстрый интернет
						</div>
						<div class="icon-text">
							 300 Мбит. Всё летает! Вы не будете скучать, пока ждёте загрузки
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	 <?$APPLICATION->IncludeComponent(
	"zaiv:instagramgallerylite",
	"",
Array()
);?>
</div>
<div class="ready">
	<div class="container">
		<div class="col-xs-12 text-center">
			 <script id="bx24_form_inline" data-skip-moving="true">
        (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
                (w[b].forms=w[b].forms||[]).push(arguments[0])};
                if(w[b]['forms']) return;
                var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://prbel.bitrix24.ru/bitrix/js/crm/form_loader.js','b24form');

        b24form({"id":"13","lang":"ru","sec":"tks34m","type":"inline"});
</script> <!--<div class="title-ready">
				 Готовы присоединиться к нам?
			</div>
			<div class="description-ready">
				 Присылайте своё резюме на почту и мы свяжемся с вами, чтобы обсудить детали и назначить интервью
			</div>
			<div class="btn-ready">
				<div class="arrow">
				</div>
 <a href="mailto:team@pr-bel.ru">team@pr-bel.ru</a>
			</div>-->
		</div>
	</div>
</div>
	<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/certificate.php"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>