<?
	require( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php" );
	$APPLICATION->SetTitle( "Для прессы" );
?>
	<div class="banner-about">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<h1>Для прессы</h1>
				</div>
			</div>

			<div class="row krosh">
				<p><a href="#">Главная</a> <i class="fa fa-angle-right" aria-hidden="true"></i> <span>Для прессы</span>
				</p>
			</div>
		</div>
	</div>
	<div class="accordion">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<ul>
						<li class="accordion-item">
							<div class="title-accordion">
								Логотип агентства
								<div class="btn-accordion">
									<svg width="24px" height="24px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"
									     xmlns:xlink="http://www.w3.org/1999/xlink">
										<g stroke="none" stroke-width="1px" fill="none" fill-rule="evenodd"
										   stroke-linecap="square">
											<g transform="translate(1.000000, 1.000000)" stroke="#222222">
												<path d="M0,11 L22,11"></path>
												<path d="M11,0 L11,22"></path>
											</g>
										</g>
									</svg>
								</div>
							</div>
							<div class="accordion-body">
								Text
							</div>
						</li>
						<li class="accordion-item">
							<div class="title-accordion">
								Логотип агентства
								<div class="btn-accordion">
									<svg width="24px" height="24px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"
									     xmlns:xlink="http://www.w3.org/1999/xlink">
										<g stroke="none" stroke-width="1px" fill="none" fill-rule="evenodd"
										   stroke-linecap="square">
											<g transform="translate(1.000000, 1.000000)" stroke="#222222">
												<path d="M0,11 L22,11"></path>
												<path d="M11,0 L11,22"></path>
											</g>
										</g>
									</svg>
								</div>
							</div>
							<div class="accordion-body">
								Text
							</div>
						</li>
						<li class="accordion-item">
							<div class="title-accordion">
								Логотип агентства
								<div class="btn-accordion">
									<svg width="24px" height="24px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"
									     xmlns:xlink="http://www.w3.org/1999/xlink">
										<g stroke="none" stroke-width="1px" fill="none" fill-rule="evenodd"
										   stroke-linecap="square">
											<g transform="translate(1.000000, 1.000000)" stroke="#222222">
												<path d="M0,11 L22,11"></path>
												<path d="M11,0 L11,22"></path>
											</g>
										</g>
									</svg>
								</div>
							</div>
							<div class="accordion-body">
								Text
							</div>
						</li>
						<li class="accordion-item">
							<div class="title-accordion">
								Логотип агентства
								<div class="btn-accordion">
									<svg width="24px" height="24px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"
									     xmlns:xlink="http://www.w3.org/1999/xlink">
										<g stroke="none" stroke-width="1px" fill="none" fill-rule="evenodd"
										   stroke-linecap="square">
											<g transform="translate(1.000000, 1.000000)" stroke="#222222">
												<path d="M0,11 L22,11"></path>
												<path d="M11,0 L11,22"></path>
											</g>
										</g>
									</svg>
								</div>
							</div>
							<div class="accordion-body">
								Text
							</div>
						</li>
					</ul>
				</div>
			</div>

		</div>
	</div>

<? $APPLICATION->IncludeComponent( "bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH"           => "/include/prbel/contact_us.php",
	"EDIT_TEMPLATE"  => ""
),
	false
); ?>

<? require( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php" ); ?>