<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Разработка и создание сайтов для медицины");
?>
<div class="banner-about banner-with-img">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1><? $APPLICATION->ShowTitle(false);?></h1>
				<p class="baner-disc">
					 Сайт компании — фундамент построения бизнеса в интернете. Все дальнейшие манипуляции с рекламой и продвижением будут опираться на сам сайт: его дизайн, удобство использования, технические возможности.
				</p>
			</div>
			<div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
				 <!--<img src="/local/templates/prbel/img/page/kotly.png" alt="" class="img-responsive">-->
			</div>
		</div>
		<div class="row krosh">
			<p>
 <a href="/">Главная</a> <i class="fa fa-angle-right" aria-hidden="true"></i>
				Разработка и создание сайтов для медицины
			</p>
		</div>
	</div>
</div>
<div class="direction">
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-2 col-lg-10">
				<div class="row">
					<div class="col-lg-6">
						<p>
							Сегодня ни одна современная медицинская организация не может себе позволить отсутствие сайта. Это уже не просто инструмент имиджа, а обязательный элемент ее развития.</p> <p>Пользователи интернета при первых симптомах заболевания сразу же идут в интернет и ищут способы и виды лечения. Соответственно, они должны находить ваш сайт и иметь возможность записаться на прием к врачу сразу же на сайте онлайн.
						</p>
					</div>
					<div class="col-lg-6">
						<h2>
						100+ запущенных сайтов в различных отраслях </h2>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="why-dev">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h2>Мы делаем правильные сайты для:</h2>
			</div>
		</div>
		<div class="row" style="margin-left: 15%; text-align: center;">
			<div class="col-lg-2 dev-item" style="text-align: center;">
				<div class="dev-icon"  style="text-align: center;">
 <img src="https://ground-c.dev-prbel.ru/images/med1.png" alt="">
				</div>
				<div class="dev-text">
					 Медицинских центров
				</div>
			</div>
			<div class="col-lg-2 dev-item"  style="text-align: center;">
				<div class="dev-icon"  style="text-align: center;">
 <img src="https://ground-c.dev-prbel.ru/images/med2.png" alt="">
				</div>
				<div class="dev-text">
					 Стоматологий
				</div>
			</div>
			<div class="col-lg-2 dev-item"  style="text-align: center;">
				<div class="dev-icon"  style="text-align: center;">
 <img src="https://ground-c.dev-prbel.ru/images/med3.png" alt="">
				</div>
				<div class="dev-text">
					 Аптек
				</div>
			</div>
			<div class="col-lg-2 dev-item"  style="text-align: center;">
				<div class="dev-icon"  style="text-align: center;">
 <img src="https://ground-c.dev-prbel.ru/images/med4.png" alt="">
				</div>
				<div class="dev-text">
					 Ветеринарных клиник
				</div>
			</div>
			<div class="col-lg-2 dev-item"  style="text-align: center;">
				<div class="dev-icon"  style="text-align: center;">
 <img src="https://ground-c.dev-prbel.ru/images/med5.png" alt="" >
				</div>
				<div class="dev-text">
					 Больниц
				</div>
			</div>
		</div>
	</div>
</div>
<div class="why-profit">
	<div class="container">
		<div class="row text-center">
			<h2>
			Принципы разработки сайтов </h2>
		</div>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-2">
				<div class="row">
					 <!--<div class="col-lg-1 col-md-1 col-sm-2 col-xs-12">
						<div class="img-wrap">
 <img src="/local/templates/prbel/img/page/check.svg" alt="" class="img-responsive">
						</div>
					</div>-->
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="profit-title">
							 Просто
						</div>
						<div class="profit-text">
							 Сайт легко продвинуть, и он приносит прибыль. Весь процесс разработки подчинён идее эффективности и целесообразности.
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="profit-title">
							 Удобно
						</div>
						<div class="profit-text">
							 Мы разрабатываем продающие и SEO-оптимизированные сайты, а вы экономите на поисковом продвижении. При разработке сайта мы всегда помним о цели — сайт должен приносить прибыль бизнесу.
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="profit-title">
							 Выгодно
						</div>
						<div class="profit-text">
							 Мы разработали более 100 проектов, которые окупили затраты на их разработку.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="word-lead">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
				<div class="word-wrap">
					 За все время работы мы запустили 100+ проектов в различных отраслях бизнеса. Каждый второй клиент приходит к нам по рекомендации.
				</div>
				<div class="bottom-line">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
				<div class="lead-wrap">
					<div class="row">
						<div class="author-img">
 <img src="/local/templates/prbel/img/page/limonov.jpg" alt="" class="img-responsive">
						</div>
						<div class="author-name">
							<div class="name">
								 Дмитрий Л.
							</div>
							<div class="status">
								 Руководитель компании
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/trust.php"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/contact_us.php"
	)
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>