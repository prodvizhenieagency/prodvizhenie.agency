<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "⭐️Заказать настройку контекстной рекламы в #WF_CITY_PRED#. Цены на услуги ведения контекстной рекламы в Google Adwords и Яндекс Директе");
$APPLICATION->SetPageProperty("title", "Настройка контекстной рекламы в #WF_CITY_PRED# – Заказать ведение контекстно-медийной рекламы");
$APPLICATION->SetTitle("Контекстная реклама");

$APPLICATION->AddChainItem("Услуги", "/services/");
$APPLICATION->AddChainItem("Контекстная реклама");
?>

<section>


<div itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">									
	<div itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">								
		<meta itemprop="url" content="https://prodvizhenie.agency/local/templates/prbel/img/main/logo.png">					  		
	</div>								
	<link itemprop="url" href="https://prodvizhenie.agency/">																
	<div itemprop="address" itemscope="" itemtype="https://schema.org/PostalAddress">					  			
		<meta itemprop="postalCode" content="308033">							
		<meta itemprop="addressCountry" content="Россия">							
		<meta itemprop="addressRegion" content="Белгородская обл.">							
		<meta itemprop="addressLocality" content="г. Белгород,">							
		<meta itemprop="streetAddress" content="ул. Королева, 2а, корп. 2, офис 704">					  		
	</div>								
	<meta itemprop="telephone" content="+7 (4722) 777-431">
	<meta itemprop="telephone" content="+7 (800) 200-55-31">	
	<meta itemprop="email" content="go@pr-bel.ru">	
	<meta itemprop="image" content="https://prodvizhenie.agency/upload/medialibrary/775/7750e3c3025e78dd7fd439b58c50ba92.jpg">																
	<meta itemprop="brand" content="Digital-агенство Продвижение">								
	<meta itemprop="legalName" content="Digital-агенство Продвижение">								
	<meta itemprop="name" content="<?$APPLICATION->ShowTitle()?>">								
	<meta itemprop="description" content="<?$APPLICATION->ShowProperty('description');?>">								
</div>									




<div class="banner-about banner-with-img">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<h1 itemprop="headline">Контекстная реклама в #WF_CITY_PRED#</h1>
				 #WF_SEO_TEXT_2#
			</div>
			<div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
				 <!--<img src="/local/templates/prbel/img/page/direct.png" alt="Контекстная реклама  в #WF_CITY_PRED#" class="img-responsive">-->
			</div>
		</div>
		<div class="row krosh">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"schema-1",
Array()
);?>

<div class="row krosh 222">
	<span itemscope="" itemtype="http://schema.org/BreadcrumbList">
		<span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<meta itemprop="name" content="Главная">
			<meta itemprop="position" content="0">
			<meta itemprop="item" content="https://prodvizhenie.agency/">
		</span>
		<i class="fa fa-angle-right" aria-hidden="true"></i>
		<span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <meta itemprop="name" content="Контекстная реклама ⭐️⭐️⭐️⭐️⭐️">
			<meta itemprop="position" content="1">
			<meta itemprop="item" content="https://prodvizhenie.agency/kontekstnaya-reklama/">
		</span>								
	</span>		
</div>


		</div>
	</div>
</div>
<div class="direction">
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-2 col-lg-10">
				<div class="row">
					 #WF_SEO_TEXT_3#
				</div>
			</div>
		</div>
	</div>
	 #WF_SEO_TEXT_10#
	<div class="right-context">
		<div class="container">
			 #WF_SEO_TEXT_4#
		</div>
	</div>
	
	<div class="why-profit" style="background-color: #efefef !important;">
		<div class="container">
			 #WF_SEO_TEXT_5#
		</div>
	</div>
	 #WF_SEO_TEXT_6# <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/result_slider.php"
	)
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/result_company.php"
	)
);?>

	<div class="why-profit contex" style="padding-top: 75px;">
		<div class="container">
			 #WF_SEO_TEXT_7#
		</div>
	</div>


	<div class="word-lead">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
					<div class="word-wrap">
						 Контекстная реклама подходит любому бизнесу, имеющему спрос — как новому для быстрого и успешного старта, так и уже существующему — для увеличения конверсий.
					</div>
					<div class="bottom-line">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
					<div class="lead-wrap">
						<div class="row">
							<div class="author-img">
 <img alt="Юлия Федоровская - Руководитель направления контекстной рекламы" src="/images/Julia_square3-min.png" title="Юлия Федоровская - Руководитель направления контекстной рекламы" class="img-responsive">
							</div>
							<div class="author-name">
								<div class="name">
									 Юлия Федоровская
								</div>
								<div class="status">
									 Руководитель направления контекстной рекламы
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="step-to-step stages" style="background-color: white;" id="#stages">
		<div class="container">
			 #WF_SEO_TEXT_8#
		</div>
	</div>
	<div class="faq" style="padding: 75px 0 50px; background-color: #efefef;">
		<div class="container">
			 #WF_SEO_TEXT_9#
		</div>
	</div>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/trust.php"
	)
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/contact_us_seo.php"
	)
);?>
	<div class="services context-service" style="background-color: white;">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center ">
					<h2 style="margin-bottom: 25px; padding-top: 10px;">Кроме контекстной рекламы</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
					<div class="description">
						 Для достижения наилучшего результата мы объединяем эффективные инструменты. Кроме контекстной рекламы, мы предлагаем:
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-10 col-md-offset-1">
				<div class="row row-space ">
 <a href="/sozdanie-sajtov/">
					<div class="col-md-6 col-sm-6 col-space text-center row-space">
						<div class="info-block alt">
							<div class="service-title">
								 Разработка сайтов
							</div>
							<p>
								 Разрабатываем продающие и оптимизированные сайты для максимальной эффективности интернет-маркетинга
							</p>
							<div class="arrow_link">
							</div>
						</div>
					</div>
 </a>
					<!-- Seo --> <a href="/seo/">
					<div class="col-md-6 col-sm-6 col-space text-center row-space">
						<div class="info-block alt">
							<div class="service-title">
								 SEO-продвижение
							</div>
							<p>
								 Привлекаем трафик из поисковых систем. Работаем на рост посещаемости сайта и увеличение продаж в долгосрочной перспективе
							</p>
							<div class="arrow_link">
							</div>
						</div>
					</div>
 </a>
					<!-- / Development --> <!-- Services --> <a href="/podderzhka-sajtov/">
					<div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
						<div class="info-block alt">
							<div class="service-title">
								 Техподдержка сайтов
							</div>
							<p>
								 Большой опыт позволяет нам осуществлять поддержку и сопровождение сайтов на Bitrix любой сложности на долгосрочной основе
							</p>
							<div class="arrow_link">
							</div>
						</div>
					</div>
 </a> <a href="/audit/">
					<div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
						<div class="info-block alt">
							<div class="service-title">
								 Аудит
							</div>
							<p>
								 Проконсультируем, какие действия предпринять, чтобы потенциальные клиенты находили вас быстрее
							</p>
							<div class="arrow_link">
							</div>
						</div>
					</div>
 </a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-offset-0 col-md-12 col-sm-12 col-space text-center blank-element">
				<div href="#">
					 <a class="b24-web-form-popup-btn-15" >
             <h2 style="padding-top: 30px;  margin-bottom: 25px;">Не нашли нужную услугу?</h2>
             <div class="description" style="margin-bottom:0px">
          <span style="color:#333">Опишите какую задачу нужно решить и мы найдем оптимальное решение</span>
        </div>
<button class="webform-services">
Написать</button>
                </a>
				</div>
			</div>
		</div>
	</div>
	 <style>
.result-slider{
  display: none;
}

 </style>
</div>

</section>

 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>