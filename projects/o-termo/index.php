<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "O-termo— кейсы digital-агентства Продвижение. Разрабатываем сайты, оптимизируем их и настраиваем контекстную рекламу");
$APPLICATION->SetPageProperty("title", "O-termo — Проекты — Digital-агентство «Продвижение»");
$APPLICATION->SetTitle("O-termo");

$APPLICATION->AddChainItem("Проекты", "/projects/");
$APPLICATION->AddChainItem("O’Termo");	
?>	<div class="banner-about">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<h1>O’Termo</h1>
					<p class="baner-disc">
						Интернет-магазин одежды для активного отдыха, Санкт-Петербург
					</p>
				</div>
			</div>

			<div class="row krosh">
				<?$APPLICATION->IncludeComponent( "bitrix:breadcrumb", "schema", Array(), false );?>
			</div>
		</div>
	</div>

	<div class="info-project o-termo-views">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="icon-wrap row">
						<div class="col-xs-3">
							<img src="/local/templates/prbel/img/case/date-icon.svg" alt="">
						</div>
						<div class="col-xs-9 icon-text">
							Одношаговая корзина<br /> и онлайн-оплата
						</div>
					</div>
					<div class="icon-wrap row">
						<div class="col-xs-3">
							<img src="/local/templates/prbel/img/case/seo-icon.svg" alt="">
						</div>
						<div class="col-xs-9 icon-text">
							Семантическое проектирование структуры сайта

						</div>
					</div>
					<div class="icon-wrap row">
						<div class="col-xs-3">
							<img src="/local/templates/prbel/img/case/docs-icon.svg" alt="">
						</div>
						<div class="col-xs-9 icon-text">
							Быстрый запуск проекта <br /> 2 недели
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="task-block">
		<div class="container">
			<div class="row task-wrap">
				<div class="col-lg-offset-1 col-lg-5">
					<div class="title">
						Задача
					</div>
					<div class="text">
						Привлечение новых клиентов, увеличение продаж 
					</div>
				</div>
			</div>
			<div class="row task-wrap">
				<div class="col-lg-offset-1 col-lg-4">
					<div class="title">
						Сфера
					</div>
					<div class="text">
						Интернет-магазин одежды для активного отдыха
					</div>
				</div>
			</div>
			<div class="row task-wrap">
				<div class="col-lg-offset-1 col-lg-4">
					<div class="title">
						Ссылка на сайт
					</div>
					<div class="text">
						o-termo.ru
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="placeholder-bg o-termo-placeholder">
		<div class="container">
			<div class="pages">
				<img src="/local/templates/prbel/img/page/o-termo-pages.png" alt="">
			</div>
		</div>
	</div>
	<div class="result-section o-termo-result">
		<div class="container">
			<div class="row">
				<div class="float-img">
					<img src="/local/templates/prbel/img/page/o-termo-pad.png" alt="">
				</div>
				<div class="col-lg-offset-6 col-lg-6">
					<div class="title">
						Результат
					</div>
					<div class="text">
						Разработка интернет-магазина с простой навигацией, удобной системой заказа, полезной информацией по выбору товаров. Окупаемость затрат на разработку в течение 1 месяца.
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pager-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 arrow arrow-left">
					<a href="/projects/shield-lk/">
						<img src="/local/templates/prbel/img/case/arrow-left-icon.svg" alt=""><span>Предыдущий проект</span>
					</a>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
					<a href="/projects/"><img src="/local/templates/prbel/img/case/list-icon.svg" alt=""></a>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right arrow-right arrow">
					<a href="/projects/balance-bel/">
						<span>Следующий проект</span><img src="/local/templates/prbel/img/case/arrow-right-icon.svg" alt="">
					</a>
				</div>
			</div>
		</div>
	</div>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/contact_us.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/certificate.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>