<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Как достичь роста поискового трафика в 20 раз за год и пика переходов в кризис. Кейсы digital-агентства Продвижение. Разрабатываем сайты, оптимизируем их и настраиваем контекстную рекламу");
$APPLICATION->SetPageProperty("title", "Как достичь роста поискового трафика в 20 раз за год и пика переходов в кризис — Проекты — Digital-агентство «Продвижение»");
	$APPLICATION->SetTitle("Как достичь роста поискового трафика в 20 раз за год и пика переходов в кризис");
?><?$APPLICATION->IncludeComponent(
	"uplab:tilda",
	"",
	Array(
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"PAGE" => "13063119",
		"PROJECT" => "1775842",
		"STOP_CACHE" => "N"
	)
);?>
	<div class="pager-section no-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 arrow arrow-left">
					<a href="/projects/urozaj/"><img src="/local/templates/prbel/img/case/arrow-left-icon.svg" alt=""><span>Предыдущий проект</span></a>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
					<a href="/projects/"><img src="/local/templates/prbel/img/case/list-icon.svg" alt=""></a>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right arrow-right arrow">
					<a href="/projects/texnodrev/"><span>Следующий проект</span><img src="/local/templates/prbel/img/case/arrow-right-icon.svg" alt=""></a>
				</div>
			</div>
		</div>
	</div>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/contact_us.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/certificate.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>