<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Max Interior —  кейсы digital-агентства Продвижение. Разрабатываем сайты, оптимизируем их и настраиваем контекстную рекламу");
$APPLICATION->SetPageProperty("title", "Max Interior — Digital-агентство «Продвижение»");
$APPLICATION->SetTitle("Max Interior");
	
$APPLICATION->AddChainItem("Проекты", "/projects/");
$APPLICATION->AddChainItem("Max Interior");	
?><div class="banner-about">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<h1>Max Interior</h1>
				<p class="baner-disc">
					 Производитель мебели класса премиум, Белгород
				</p>
			</div>
		</div>
		<div class="row krosh">
			<?$APPLICATION->IncludeComponent( "bitrix:breadcrumb", "schema", Array(), false );?>
		</div>
	</div>
</div>
<div class="info-project max-views">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="icon-wrap row">
					<div class="col-xs-3">
 <img src="/local/templates/prbel/img/case/date-icon.svg" alt="">
					</div>
					<div class="col-xs-9 icon-text">
						 Сайт-каталог <br>
						 с ассортиментом продукции
					</div>
				</div>
				<div class="icon-wrap row">
					<div class="col-xs-3">
 <img src="/local/templates/prbel/img/case/seo-icon.svg" alt="">
					</div>
					<div class="col-xs-9 icon-text">
						 Современный адаптивный дизайн<br>
					</div>
				</div>
				<div class="icon-wrap row">
					<div class="col-xs-3">
 <img src="/local/templates/prbel/img/case/docs-icon.svg" alt="">
					</div>
					<div class="col-xs-9 icon-text">
						 Портфолио выполненных<br>
						 проектов
					</div>
				</div>
			</div>
		</div>
		<div class="float-img">
 <img src="/local/templates/prbel/img/page/max-views.png" alt="">
		</div>
	</div>
</div>
<div class="task-block">
	<div class="container">
		<div class="row task-wrap">
			<div class="col-lg-offset-1 col-lg-5">
				<div class="title">
					 Задача
				</div>
				<div class="text">
					 Формирование привлекательного имиджа компании в интернете, увеличение узнаваемости, демонстрация каталога и примеров работ.
				</div>
			</div>
		</div>
		<div class="row task-wrap">
			<div class="col-lg-offset-1 col-lg-4">
				<div class="title">
					 Сфера
				</div>
				<div class="text">
					 Производитель эксклюзивной мебели, дизайн интерьеров
				</div>
			</div>
		</div>
	</div>
</div>
<div class="placeholder-bg max-placeholder">
	<div class="container">
		<div class="pages">
 <img src="/local/templates/prbel/img/page/max-pages.png" alt="">
		</div>
	</div>
</div>
<div class="result-section max-result">
	<div class="container">
		<div class="row">
			<div class="float-img">
 <img src="/local/templates/prbel/img/page/max-pad.png" alt="">
			</div>
			<div class="col-lg-offset-6 col-lg-6">
				<div class="title">
					 Результат
				</div>
				<div class="text">
					 Создан корпоративный сайт производственной компании с удобной навигацией, каталогом товаров и примерами работ. Современный дизайн подчеркивает имидж компании «Макс Интерьер» как производителя эксклюзивной мебели под заказ.
				</div>
			</div>
		</div>
	</div>
</div>
<div class="pager-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 arrow arrow-left">
 <a href="/projects/bts-kotly/"><img src="/local/templates/prbel/img/case/arrow-left-icon.svg" alt="">Предыдущий проект</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
 <a href="/projects/"><img src="/local/templates/prbel/img/case/list-icon.svg" alt=""></a>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right arrow-right arrow">
 <a href="/projects/urozaj/">Следующий проект<img src="/local/templates/prbel/img/case/arrow-right-icon.svg" alt=""></a>
			</div>
		</div>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/contact_us.php"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/certificate.php"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>