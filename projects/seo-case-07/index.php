<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "БАНКРОТЭКСПЕРТ — Проекты — Digital-агентство «Продвижение»");
	$APPLICATION->SetTitle("БАНКРОТЭКСПЕРТ");
?>
	<div class="direction no-padding">
		<div>
			<div class="row">
				<div class="col-lg-12">
					<div class="row">
						<div class="col-xs-12">
							<?$APPLICATION->IncludeComponent("bitrix:news.detail", "tilda", Array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
								"ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации
								"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
								"AJAX_MODE" => "N",	// Включить режим AJAX
								"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
								"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
								"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
								"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
								"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
								"CACHE_GROUPS" => "Y",	// Учитывать права доступа
								"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
								"CACHE_TYPE" => "A",	// Тип кеширования
								"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
								"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
								"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
								"DISPLAY_DATE" => "Y",	// Выводить дату элемента
								"DISPLAY_NAME" => "Y",	// Выводить название элемента
								"DISPLAY_PICTURE" => "Y",	// Выводить детальное изображение
								"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
								"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
								"ELEMENT_CODE" => "",	// Код новости
								"ELEMENT_ID" => "442",	// ID новости
								"FIELD_CODE" => array(	// Поля
									0 => "",
									1 => "",
								),
								"IBLOCK_ID" => "35",	// Код информационного блока
								"IBLOCK_TYPE" => "aspro_digital_content",	// Тип информационного блока (используется только для проверки)
								"IBLOCK_URL" => "",	// URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
								"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
								"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
								"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
								"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
								"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
								"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
								"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
								"PAGER_TITLE" => "Страница",	// Название категорий
								"PROPERTY_CODE" => array(	// Свойства
									0 => "",
									1 => "",
								),
								"SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
								"SET_CANONICAL_URL" => "N",	// Устанавливать канонический URL
								"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
								"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
								"SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
								"SET_STATUS_404" => "N",	// Устанавливать статус 404
								"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
								"SHOW_404" => "N",	// Показ специальной страницы
								"USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
								"USE_SHARE" => "N",	// Отображать панель соц. закладок
								"COMPONENT_TEMPLATE" => ".default",
								"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа элемента
								"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
								"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
							),
								false
							);?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pager-section no-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 arrow arrow-left">
					<a href="/projects/urozaj/"><img src="/local/templates/prbel/img/case/arrow-left-icon.svg" alt=""><span>Предыдущий проект</span></a>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
					<a href="/projects/"><img src="/local/templates/prbel/img/case/list-icon.svg" alt=""></a>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right arrow-right arrow">
					<a href="/projects/texnodrev/"><span>Следующий проект</span><img src="/local/templates/prbel/img/case/arrow-right-icon.svg" alt=""></a>
				</div>
			</div>
		</div>
	</div>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/contact_us.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/certificate.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>