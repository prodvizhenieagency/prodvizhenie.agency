<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Баланс —  кейсы digital-агентства Продвижение. Разрабатываем сайты, оптимизируем их и настраиваем контекстную рекламу");
$APPLICATION->SetPageProperty("title", "Баланс — Проекты — Digital-агентство «Продвижение»");
	$APPLICATION->SetTitle("Баланс");
	
$APPLICATION->AddChainItem("Проекты", "/projects/");
$APPLICATION->AddChainItem("Баланс");
?>	<div class="banner-about">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<h1>Баланс</h1>
					<p class="baner-disc">
						Оказание услуг в сфере управления финансами, ЦФО
					</p>
				</div>
			</div>

			<div class="row krosh">
				<?$APPLICATION->IncludeComponent( "bitrix:breadcrumb", "schema", Array(), false );?>
			</div>
		</div>
	</div>

	<div class="info-project balance-views">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="icon-wrap row">
						<div class="col-xs-3">
							<img src="/local/templates/prbel/img/case/date-icon.svg" alt="">
						</div>
						<div class="col-xs-9 icon-text">
							Семантическое проектирование<br/>
							структуры сайта
						</div>
					</div>
					<div class="icon-wrap row">
						<div class="col-xs-3">
							<img src="/local/templates/prbel/img/case/seo-icon.svg" alt="">
						</div>
						<div class="col-xs-9 icon-text">
							Современный и<br />
							адаптивный дизайн
						</div>
					</div>
					<div class="icon-wrap row">
						<div class="col-xs-3">
							<img src="/local/templates/prbel/img/case/docs-icon.svg" alt="">
						</div>
						<div class="col-xs-9 icon-text">
							Калькулятор расчета стоимости<br />
							бухгалтерских услуг
						</div>
					</div>
				</div>
			</div>
			<div class="float-img">
				<img src="/local/templates/prbel/img/page/balanc-views.png" alt="">
			</div>
		</div>
	</div>
	<div class="task-block">
		<div class="container">
			<div class="row task-wrap">
				<div class="col-lg-offset-1 col-lg-5">
					<div class="title">
						Задача
					</div>
					<div class="text">
						Привлечение новых клиентов, формирование презентабельного имиджа компании в интернете
					</div>
				</div>
			</div>
			<div class="row task-wrap">
				<div class="col-lg-offset-1 col-lg-4">
					<div class="title">
						Сфера
					</div>
					<div class="text">
						Бухгалтерские, юридические услуги, финансовый консалтинг
					</div>
				</div>
			</div>
			<div class="row task-wrap">
				<div class="col-lg-offset-1 col-lg-4">
					<div class="title">
						Ссылка на сайт
					</div>
					<div class="text">
						<a href="http://www.balans-bel.ru/">balans-bel.ru </a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="placeholder-bg balance-placeholder">
		<div class="container">
			<div class="pages">
				<img src="/local/templates/prbel/img/page/balance-pages.png" alt="">
			</div>
		</div>
	</div>
	<div class="result-section balance-result">
		<div class="container">
			<div class="row">
				<div class="float-img">
					<img src="/local/templates/prbel/img/page/balance-pad.png" alt="">
				</div>
				<div class="col-lg-offset-6 col-lg-6">
					<div class="title">
						Результат
					</div>
					<div class="text">
						Разработка сайта с современным дизайном взамен устаревшего сайта компании. Структура разделов меню проработана под поисковый спрос для привлечения трафика из Яндекс и Google. Для корректного отображения страниц на мобильных устройствах реализован адаптивный дизайн. Онлайн-калькулятор расчета стоимости бухгалтерских услуг для повышения вовлеченности пользователей. 
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pager-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 arrow arrow-left">
					<a href="/projects/o-termo/"><img src="/local/templates/prbel/img/case/arrow-left-icon.svg" alt=""><span>Предыдущий проект</span></a>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
					<a href="/projects/"><img src="/local/templates/prbel/img/case/list-icon.svg" alt=""></a>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right arrow-right arrow">
					<a href="#"><span>Следующий проект</span><img src="/local/templates/prbel/img/case/arrow-right-icon.svg" alt=""></a>
				</div>
			</div>
		</div>
	</div>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/contact_us.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/certificate.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>