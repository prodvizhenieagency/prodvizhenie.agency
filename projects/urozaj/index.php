<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Урожай — кейсы digital-агентства Продвижение. Разрабатываем сайты, оптимизируем их и настраиваем контекстную рекламу");
$APPLICATION->SetPageProperty("title", "Урожай — Проекты — Digital-агентство «Продвижение»");
$APPLICATION->SetTitle("Урожай");

$APPLICATION->AddChainItem("Проекты", "/projects/");
$APPLICATION->AddChainItem("Урожай");	
?><div class="banner-about">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<h1>Урожай</h1>
				<p class="baner-disc">
					 Производитель колбас и мясных полуфабрикатов, Белгород
				</p>
			</div>
		</div>
		<div class="row krosh">
			<?$APPLICATION->IncludeComponent( "bitrix:breadcrumb", "schema", Array(), false );?>
		</div>
	</div>
</div>
<div class="info-project ur-views">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="icon-wrap row">
					<div class="col-xs-3">
 <img src="/local/templates/prbel/img/case/date-icon.svg" alt="">
					</div>
					<div class="col-xs-9 icon-text">
						 Карта со списком магазинов<br>
						 адресами и режимом работы
					</div>
				</div>
				<div class="icon-wrap row">
					<div class="col-xs-3">
 <img src="/local/templates/prbel/img/case/seo-icon.svg" alt="">
					</div>
					<div class="col-xs-9 icon-text">
						 Каталог продукции<br>
						 с фото и описанием
					</div>
				</div>
				<div class="icon-wrap row">
					<div class="col-xs-3">
 <img src="/local/templates/prbel/img/case/docs-icon.svg" alt="">
					</div>
					<div class="col-xs-9 icon-text">
						 Сайт оптимизирован<br>
						 под поисковые системы
					</div>
				</div>
			</div>
		</div>
		<div class="float-img">
 <img src="/local/templates/prbel/img/page/ur-views.png" alt="">
		</div>
	</div>
</div>
<div class="task-block">
	<div class="container">
		<div class="row task-wrap">
			<div class="col-lg-offset-1 col-lg-5">
				<div class="title">
					 Задача
				</div>
				<div class="text">
					 Укрепление имиджа компании на белгородском рынке колбас и мясных полуфабрикатов, демонстрация каталога продукции партнёрам и клиентам.
				</div>
			</div>
		</div>
		<div class="row task-wrap">
			<div class="col-lg-offset-1 col-lg-4">
				<div class="title">
					 Сфера
				</div>
				<div class="text">
					 Производство колбас и мясных полуфабрикатов
				</div>
			</div>
		</div>
		<div class="row task-wrap">
			<div class="col-lg-offset-1 col-lg-4">
				<div class="title">
					 Ссылка на сайт
				</div>
				<div class="text">
					 urojai31.ru
				</div>
			</div>
		</div>
	</div>
</div>
<div class="placeholder-bg ur-placeholder">
	<div class="container">
		<div class="pages">
 <img src="/local/templates/prbel/img/page/ur-pages.png" alt="">
		</div>
	</div>
</div>
<div class="result-section ur-result">
	<div class="container">
		<div class="row">
			<div class="float-img">
 <img src="/local/templates/prbel/img/page/ur-pad.png" alt="">
			</div>
			<div class="col-lg-offset-6 col-lg-6">
				<div class="title">
					 Результат
				</div>
				<div class="text">
					 Создание корпоративного сайта производственной компании с перечнем розничных магазинов, контактами компании и каталогом для партнёров и клиентов.
				</div>
			</div>
		</div>
	</div>
</div>
<div class="pager-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 arrow arrow-left">
 <a href="/projects/max-interier/"><img src="/local/templates/prbel/img/case/arrow-left-icon.svg" alt="">Предыдущий проект</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
 <a href="/projects/"><img src="/local/templates/prbel/img/case/list-icon.svg" alt=""></a>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right arrow-right arrow">
 <a href="/projects/goodcup/">
				Следующий проект<img src="/local/templates/prbel/img/case/arrow-right-icon.svg" alt=""> </a>
			</div>
		</div>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/contact_us.php"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/certificate.php"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>