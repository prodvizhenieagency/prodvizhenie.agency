<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Евростоматология — Проекты — Digital-агентство «Продвижение»");
$APPLICATION->SetTitle("Евростоматология");
?>
	<div class="direction">
        <div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"tilda", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => "313",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"IBLOCK_ID" => "35",
		"IBLOCK_TYPE" => "aspro_digital_content",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "Y",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "tilda",
		"STRICT_SECTION_CHECK" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"FILE_404" => ""
	),
	false
);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="pager-section no-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 arrow arrow-left">
					<a href="/projects/max-interier/"><img src="/local/templates/prbel/img/case/arrow-left-icon.svg" alt=""><span>Предыдущий проект</span></a>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
					<a href="/projects/"><img src="/local/templates/prbel/img/case/list-icon.svg" alt=""></a>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right arrow-right arrow">
					<a href="/projects/urozaj/"><span>Следующий проект</span><img src="/local/templates/prbel/img/case/arrow-right-icon.svg" alt=""></a>
				</div>
			</div>
		</div>
	</div>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
    "AREA_FILE_SHOW" => "file",
    "PATH" => "/include/prbel/contact_us.php",
    "EDIT_TEMPLATE" => ""
),
    false
);?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
    "AREA_FILE_SHOW" => "file",
    "PATH" => "/include/prbel/certificate.php",
    "EDIT_TEMPLATE" => ""
),
    false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>