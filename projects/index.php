<?
	require( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php" );
$APPLICATION->SetPageProperty("description", "⭐️ Кейсы digital-агентства Продвижение. Разрабатываем сайты, оптимизируем их и настраиваем контекстную рекламу");
$APPLICATION->SetPageProperty("title", "Проекты — Digital-агентство «Продвижение»");
	$APPLICATION->SetTitle("Проекты");
?><div class="banner-about">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
<!--				<h1>--><?// $APPLICATION->ShowTitle(false);?><!--</h1>-->
				<h1><? $APPLICATION->ShowProperty("h1")?:"&nbsp;";?></h1>
			</div>
		</div>
		<div class="row krosh">
			<?$APPLICATION->IncludeComponent( "bitrix:breadcrumb", "schema", Array(), false );?>
		</div>
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"bitrix:news", 
	"projects_page1", 
	array(
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "100000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "projects_page1",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",
		"DETAIL_BRAND_PROP_CODE" => array(
			0 => "TIZERS",
			1 => "",
		),
		"DETAIL_BRAND_USE" => "Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "DETAIL_TEXT",
			3 => "DETAIL_PICTURE",
			4 => "",
		),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "FORM_ORDER",
			2 => "FORM_QUESTION",
			3 => "ORDERER",
			4 => "SITE",
			5 => "DATA",
			6 => "AUTHOR",
			7 => "LINK_PROJECTS",
			8 => "TASK_PROJECT",
			9 => "LINK_COMPANY",
			10 => "LINK_REVIEWS",
			11 => "LINK_GOODS",
			12 => "LINK_SERVICES",
			13 => "FORM_PROJECT",
			14 => "DOCUMENTS",
			15 => "PHOTOS",
			16 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_STRICT_SECTION_CHECK" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_TYPE_VIEW" => "element_1",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "arProjectFilter",
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"FORM_ID" => "14",
		"FORM_ID_ORDER_SERVISE" => "14",
		"GALLERY_TYPE" => "big",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "35",
		"IBLOCK_TYPE" => "aspro_digital_content",
		"IMAGE_POSITION" => "left",
		"IMAGE_WIDE" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"LINE_ELEMENT_COUNT" => "2",
		"LINE_ELEMENT_COUNT_LIST" => "3",
		"LIST_ACTIVE_DATE_FORMAT" => "j F Y",
		"LIST_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "DETAIL_PICTURE",
			3 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "LINK_TO_CASE",
			1 => "",
		),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "40",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SECTION_ELEMENTS_TYPE_VIEW" => "FROM_MODULE",
		"SEF_FOLDER" => "/projects/",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHARE_HANDLERS" => array(
			0 => "facebook",
			1 => "mailru",
			2 => "delicious",
			3 => "lj",
			4 => "twitter",
			5 => "vk",
		),
		"SHARE_HIDE" => "N",
		"SHARE_SHORTEN_URL_KEY" => "",
		"SHARE_SHORTEN_URL_LOGIN" => "",
		"SHARE_TEMPLATE" => "",
		"SHOW_404" => "N",
		"SHOW_DETAIL_LINK" => "Y",
		"SHOW_NEXT_ELEMENT" => "Y",
		"SHOW_SECTION_DESCRIPTION" => "Y",
		"SHOW_SECTION_PREVIEW_DESCRIPTION" => "Y",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"STRICT_SECTION_CHECK" => "N",
		"S_ASK_QUESTION" => "",
		"S_ORDER_PROJECT" => "Заказать проект",
		"S_ORDER_SERVISE" => "Заказать проект",
		"T_CHARACTERISTICS" => "",
		"T_CLIENTS" => "",
		"T_DOCS" => "",
		"T_GALLERY" => "",
		"T_GOODS" => "",
		"T_NEXT_LINK" => "Следующий проект",
		"T_PREV_LINK" => "",
		"T_PROJECTS" => "Похожие проекты",
		"T_REVIEWS" => "Отзыв клиента",
		"T_SERVICES" => "",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_REVIEW" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "Y",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "#SECTION_CODE_PATH#/",
			"detail" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
		)
	),
	false
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/result_company.php"
	)
);?>
<div class="cta-banner">
	<div class="container">
		<div class="col-xs-12">
			Мы понимаем, что ваш сайт — это инструмент, благодаря которому вы сможете увеличить онлайн-конверсии. Именно поэтому мы создаем веб-сайты с основой для интеграции интернет-маркетинга.
		</div>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/certificate.php"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/contact_us.php"
	)
);?><? require( $_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php" ); ?>