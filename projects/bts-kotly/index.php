<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "БТС Котлы — кейсы digital-агентства Продвижение. Разрабатываем сайты, оптимизируем их и настраиваем контекстную рекламу");
$APPLICATION->SetPageProperty("title", "БТС Котлы — Проекты — Digital-агентство «Продвижение»");
	$APPLICATION->SetTitle("БТС Котлы");
	
$APPLICATION->AddChainItem("Проекты", "/projects/");
$APPLICATION->AddChainItem("БТС Котлы");
?><div class="banner-about">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<h1>БТС Котлы</h1>
				<p class="baner-disc">
					 Интернет-магазин топливного оборудования
				</p>
			</div>
		</div>
		<div class="row krosh">
			<?$APPLICATION->IncludeComponent( "bitrix:breadcrumb", "schema", Array(), false );?>
		</div>
	</div>
</div>
<div class="info-project">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="icon-wrap row">
					<div class="col-xs-3">
 <img src="/local/templates/prbel/img/case/date-icon.svg" alt="">
					</div>
					<div class="col-xs-9 icon-text">
						 Проектирование, запуск<br>
						 за 40 дней
					</div>
				</div>
				<div class="icon-wrap row">
					<div class="col-xs-3">
 <img src="/local/templates/prbel/img/case/seo-icon.svg" alt="">
					</div>
					<div class="col-xs-9 icon-text">
						 Сайт оптимизирован<br>
						 для SEO-продвижения
					</div>
				</div>
				<div class="icon-wrap row">
					<div class="col-xs-3">
 <img src="/local/templates/prbel/img/case/docs-icon.svg" alt="">
					</div>
					<div class="col-xs-9 icon-text">
						 Разработан конфигуратор <br>
						 моделей
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="task-block">
	<div class="container">
		<div class="row task-wrap">
			<div class="col-lg-offset-1 col-lg-5">
				<div class="title">
					 Задача
				</div>
				<div class="text">
					 Перенести устаревший интернет-магазин и повысить продажи в онлайне. А если проще – сделать эффективный сайт для продвижения нового бренда на рынок.
				</div>
			</div>
		</div>
		<div class="row task-wrap">
			<div class="col-lg-offset-1 col-lg-4">
				<div class="title">
					 Сфера
				</div>
				<div class="text">
					 Производство топливного оборудования
				</div>
			</div>
		</div>
		<div class="row task-wrap">
			<div class="col-lg-offset-1 col-lg-4">
				<div class="title">
					 Ссылка на сайт
				</div>
				<div class="text">
					 <a href="http://tt-kotly.ru/" target="_blank">tt-kotly.ru</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="placeholder-bg ">
	<div class="container">
		<div class="pages">
 <img src="/local/templates/prbel/img/page/pages.png" alt="">
		</div>
	</div>
</div>
<div class="result-section ">
	<div class="container">
		<div class="row">
			<div class="float-img">
 <img src="/local/templates/prbel/img/page/i-pad.png" alt="">
			</div>
			<div class="col-lg-offset-6 col-lg-6">
				<div class="title">
					 Результат
				</div>
				<div class="text">
					 Перенесли устаревший интернет-магазин и повысили продажи в онлайне.
				</div>
			</div>
		</div>
	</div>
</div>
<div class="pager-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 arrow arrow-left">
				<a href="#"><img src="/local/templates/prbel/img/case/arrow-left-icon.svg" alt=""><span>Предыдущий проект</span></a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
 <a href="/projects/"><img src="/local/templates/prbel/img/case/list-icon.svg" alt=""></a>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right arrow-right arrow">
				<a href="/projects/max-interier/"><span>Следующий проект</span><img src="/local/templates/prbel/img/case/arrow-right-icon.svg" alt=""></a>
			</div>
		</div>
	</div>
</div>
	<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/contact_us.php"
	)
);?>
	<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/certificate.php"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>