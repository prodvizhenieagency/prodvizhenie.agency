<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "⭐️Контактные данные digital-агентства Продвижение: номер телефона, почта для связи, режим работы и адрес в #WF_CITY_PRED#");
$APPLICATION->SetPageProperty("title", "Контакты — Digital-агентство «Продвижение» – #WF_CITY_VIN#");
	$APPLICATION->SetTitle("Контакты");
?>
<div class="banner-about banner-with-img">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center"><? $APPLICATION->ShowTitle(false); ?></h1>
			</div>
		</div>
		<div class="row krosh">
			<p itemscope="" itemtype="http://schema.org/BreadcrumbList">
				<span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
					<meta itemprop="position" content="0">
					<a itemprop="item" href="/" title="Главная">
						<meta itemprop="name" content="Главная">
						Главная
					</a>
				</span>
				<i class="fa fa-angle-right" aria-hidden="true"></i>
				<span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
					<meta itemprop="position" content="1">
					<span>
						<meta itemprop="name" content="Контакты">
						Контакты
					</span>
				</span>		
			</p>
		</div>
	</div>
</div>
<div class="direction">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-xs-12">
				<?$APPLICATION->IncludeComponent("bitrix:news.detail", "contact", Array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
						"ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации
						"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
						"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
						"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
						"DISPLAY_DATE" => "Y",	// Выводить дату элемента
						"DISPLAY_NAME" => "Y",	// Выводить название элемента
						"DISPLAY_PICTURE" => "Y",	// Выводить детальное изображение
						"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"ELEMENT_CODE" => "",	// Код новости
						"ELEMENT_ID" => "198",	// ID новости
						"FIELD_CODE" => array(	// Поля
							0 => "",
							1 => "",
						),
						"IBLOCK_ID" => "27",	// Код информационного блока
						"IBLOCK_TYPE" => "aspro_digital_content",	// Тип информационного блока (используется только для проверки)
						"IBLOCK_URL" => "",	// URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
						"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
						"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
						"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
						"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
						"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
						"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
						"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
						"PAGER_TITLE" => "Страница",	// Название категорий
						"PROPERTY_CODE" => array(	// Свойства
							0 => "",
							1 => "",
						),
						"SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
						"SET_CANONICAL_URL" => "N",	// Устанавливать канонический URL
						"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
						"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
						"SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
						"SET_STATUS_404" => "N",	// Устанавливать статус 404
						"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
						"SHOW_404" => "N",	// Показ специальной страницы
						"USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
						"USE_SHARE" => "N",	// Отображать панель соц. закладок
					),
					false
				);?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/contact_us.php"
	)
);?>

<style>
h2 {
	font-size: 22px !important;
}
</style>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>