<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$APPLICATION->SetTitle("Статья");
?>
	<div class="banner-about banner-with-img">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="text-center">Статья</h1>
				</div>
			</div>

			<div class="row krosh">
				<p><a href="#">Главная</a> <i class="fa fa-angle-right" aria-hidden="true"></i> <a href="#">Блог</a> <i class="fa fa-angle-right" aria-hidden="true"></i> <span>Статья</span></p>
			</div>
		</div>
	</div>
	<div class="direction">
		<div class="container">

			<div class="row">
				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg10 col-lg-offset-2 col-md-10 col-md-offset-2 col-sm-12 col-xs-12">
							<p>Возмущение плотности когерентно. Расслоение, как следует из совокупности экспериментальных наблюдений, синхронизует фонон. Расслоение стабилизирует кристалл как при нагреве, так и при охлаждении.</p>

							<p>Течение среды самопроизвольно. Излучение оптически однородно. Еще в ранних работах Л.Д.Ландау показано, что струя стохастично возбуждает экситон.</p>

							<p>Вещество кумулятивно. Кристаллическая решетка бифокально сжимает тахионный погранслой. Силовое поле тормозит расширяющийся электрон. Жидкость притягивает гидродинамический удар. Изолируя область наблюдения от посторонних шумов, мы сразу увидим, что галактика мгновенно поглощает объект. При облучении инфракрасным лазером эксимер индуцирует ускоряющийся эксимер.</p>
							<p>Возмущение плотности когерентно. Расслоение, как следует из совокупности экспериментальных наблюдений, синхронизует фонон. Расслоение стабилизирует кристалл как при нагреве, так и при охлаждении.</p>

							<p>Течение среды самопроизвольно. Излучение оптически однородно. Еще в ранних работах Л.Д.Ландау показано, что струя стохастично возбуждает экситон.</p>

							<p>Вещество кумулятивно. Кристаллическая решетка бифокально сжимает тахионный погранслой. Силовое поле тормозит расширяющийся электрон. Жидкость притягивает гидродинамический удар. Изолируя область наблюдения от посторонних шумов, мы сразу увидим, что галактика мгновенно поглощает объект. При облучении инфракрасным лазером эксимер индуцирует ускоряющийся эксимер.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="more-posts">
		<div class="row">
			<div class="container">
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 posts-item">
					<a href="#">
						<div class="post-img">
							<img src="/local/templates/prbel/img/page/blob.png" alt="" class="img-responsive">
						</div>
						<div class="post-title">
							20 facts about hydrogel that will make you question reality
						</div>
						<div class="post-excerpt">
							Bad news: you can't eat it. Good news: you can do just about anything else with it
						</div>
					</a>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 posts-item">
					<a href="#">
						<div class="post-img">
							<img src="/local/templates/prbel/img/page/blob.png" alt="" class="img-responsive">
						</div>
						<div class="post-title">
							20 facts about hydrogel that will make you question reality
						</div>
						<div class="post-excerpt">
							Bad news: you can't eat it. Good news: you can do just about anything else with it
						</div>
					</a>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 posts-item">
					<a href="#">
						<div class="post-img">
							<img src="/local/templates/prbel/img/page/blob.png" alt="" class="img-responsive">
						</div>
						<div class="post-title">
							20 facts about hydrogel that will make you question reality
						</div>
						<div class="post-excerpt">
							Bad news: you can't eat it. Good news: you can do just about anything else with it
						</div>
					</a>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 posts-item">
					<a href="#">
						<div class="post-img">
							<img src="/local/templates/prbel/img/page/blob.png" alt="" class="img-responsive">
						</div>
						<div class="post-title">
							20 facts about hydrogel that will make you question reality
						</div>
						<div class="post-excerpt">
							Bad news: you can't eat it. Good news: you can do just about anything else with it
						</div>
					</a>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 posts-item">
					<a href="#">
						<div class="post-img">
							<img src="/local/templates/prbel/img/page/blob.png" alt="" class="img-responsive">
						</div>
						<div class="post-title">
							20 facts about hydrogel that will make you question reality
						</div>
						<div class="post-excerpt">
							Bad news: you can't eat it. Good news: you can do just about anything else with it
						</div>
					</a>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 posts-item">
					<a href="#">
						<div class="post-img">
							<img src="/local/templates/prbel/img/page/blob.png" alt="" class="img-responsive">
						</div>
						<div class="post-title">
							20 facts about hydrogel that will make you question reality
						</div>
						<div class="post-excerpt">
							Bad news: you can't eat it. Good news: you can do just about anything else with it
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>


<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/include/prbel/contact_us.php",
	"EDIT_TEMPLATE" => ""
),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>