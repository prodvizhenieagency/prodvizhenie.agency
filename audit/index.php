<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Узнайте об эффективности вашего диджитал-маркетинга");

$APPLICATION->AddChainItem("Услуги", "/services/");
$APPLICATION->AddChainItem("Аудит");
?><div class="banner-about banner-with-img">
  <div class="container">
    <div class="row">
      <div class="col-md-10">
        <h1><? $APPLICATION->ShowTitle(false);?></h1>
        <p class="baner-disc">
           Получите список рекомендаций.<br>
           Их внедрение устранит текущие ошибки продвижения и увеличит количество клиентов.
        </p>
      </div>
      <div class="col-md-2 hidden-sm hidden-xs">
         <!--<img src="/local/templates/prbel/img/page/kotly.png" alt="" class="img-responsive">-->
      </div>
    </div>
    <div class="row krosh">
       <?$APPLICATION->IncludeComponent(
  "bitrix:breadcrumb",
  "schema",
Array()
);?>
    </div>
  </div>
</div>
<div class="right-context">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 text-center">
        <h2 style="font-size:26px;">РЕЗУЛЬТАТЫ АУДИТА ПОКАЖУТ</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="context-text">
           Возможные причины<br>
           низких позиций в поиске
        </div>
        <div class="context-icon" style="margin-bottom:0px; margin-top:25px;">
 <img src="/images/services/1n_icon.svg" alt="">
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="context-text">
           Качество работ<br>
           текущего подрядчика
        </div>
        <div class="context-icon" style="margin-bottom:0px; margin-top:25px;">
 <img src="/images/services/2n_icon.svg" alt="">
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="context-text">
           Технические ошибки, препятствующие росту
        </div>
        <div class="context-icon" style="margin-bottom:0px; margin-top:25px;">
 <img src="/images/services/3n_icon.svg" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
#WF_SEO_TEXT_1#
<div class="why-profit">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 text-center">
      <h2 style="font-size:26px;">
      	ЧТО ВКЛЮЧАЕТ АУДИТ </h2>
	  </div>
    </div>
    <div class="row text-center">
      <div class="col-lg-10 col-lg-offset-2 col-md-11 col-md-offset-1">
        <div class="row">
           <!--<div class="col-lg-1 col-md-1 col-sm-2 col-xs-12">
            <div class="img-wrap">
 <img src="/local/templates/prbel/img/page/check.svg" alt="" class="img-responsive">
            </div>
          </div>-->
          <div class="col-lg-9 col-md-10 col-sm-12 col-xs-12">
            <div class="col-lg-2 col-lg-offset-3 col-md-2 col-md-offset-3 col-sm-1 col-sm-offset-3 number-col">
              <div class="wrap">
                 1
              </div>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-4">
              <div class="profit-title audit">
                 Сбор<br>
                 информации
              </div>
            </div>
            <div class="col-xs-12">
              <div class="profit-text">
                 Анализируем целевую аудиторию и рынок. Вникаем в бизнес-процессы. Изучаем сформированный и несформированный спрос. Исследуем текущие каналы продаж и источники трафика.
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-9 col-md-10 col-sm-12 col-xs-12">
            <div class="col-lg-2 col-lg-offset-3 col-md-2 col-md-offset-3 col-sm-1 col-sm-offset-3 number-col">
              <div class="wrap">
                 2
              </div>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-4">
              <div class="profit-title audit">
                 Аудит вашего<br>
                 сайта
              </div>
            </div>
            <div class="col-xs-12">
              <div class="profit-text">
                 Оцениваем состояние, позиции и характеристики сайта в сравнении с конкурентами, соответствие критериям поисковых систем. Анализируем трафик и конверсии.
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-9 col-md-10 col-sm-12 col-xs-12">
            <div class="col-lg-2 col-lg-offset-3 col-md-2 col-md-offset-3 col-sm-1 col-sm-offset-3 number-col">
              <div class="wrap">
                 3
              </div>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-4">
              <div class="profit-title audit">
                 Аудит рекламных<br>
                 кабинетов
              </div>
            </div>
            <div class="col-xs-12">
              <div class="profit-text">
                 Анализируем настройки рекламных кампании в Яндекс.Директ, Google Ads, Facebook, Instagram, ВКонтакте. Находим точки роста и оптимизируем расход бюджета. Фокус не на количество кликов, а на повышение ROI.
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-9 col-md-10 col-sm-12 col-xs-12">
            <div class="col-lg-2 col-lg-offset-3 col-md-2 col-md-offset-3 col-sm-1 col-sm-offset-3 number-col">
              <div class="wrap">
                 4
              </div>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-4">
              <div class="profit-title audit">
                 Подготовка<br>
                 отчёта
              </div>
            </div>
            <div class="col-xs-12">
              <div class="profit-text">
                 Делаем выводы и предлагаем лучшие решения. Предоставляем детальный отчёт и составляем список рекомендаций для внедрения. Помогаем защитить стратегию и обосновать бюджеты.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="cta-banner">
  <div class="container">
    <div class="col-xs-12" style="font-size:26px; color: #fff; font-weight: 700;">
       АУДИТ НУЖЕН, ЕСЛИ
    </div>
  </div>
</div>
<div class="container" style="margin:40px auto;">
  <div class="row">
    <ul class="price_blocks audit">
      <li> <input type="checkbox" checked="">
      <h3 style="font-weight: normal;" align="center">Сайт есть, а трафика нет</h3>
      <p class="audit">
         Наличие сайта — не гарантия клиентов. Для получения целевого трафика и заявок нужна планомерная работа. <br>
 <br>
         Часто при создании сайта не учитываются требования поисковых систем, акцентируются только на визуальной составляющей. <br>
 <br>
         Поэтому сайт может быть красивым, но неэффективным и неприбыльным.
      </p>
 </li>
      <li> <input type="checkbox" checked="">
      <h3 style="font-weight: normal;" align="center">Есть реклама, но нет заявок</h3>
      <p class="audit">
         Настройка рекламы — это самый простой этап. <br>
 <br>
         Дальнейший менеджмент рекламы предполагает управление, контроль, оптимизации, постоянные корректировки настроек, A/B-тестирование рекламы. Если этого нет, деньги сгорают впустую. <br>
 <br>
         Реклама должна быть не для галочки, а для высокого ROI.
      </p>
 </li>
      <li> <input type="checkbox" checked="">
      <h3 style="font-weight: normal;" align="center">Реклама не окупается</h3>
      <p class="audit">
         Вы получаете клики, но не заявки и клиентов. Отдел продаж сообщает о некачественных лидах. Обращается аудитория не из вашего ценового сегмента. <br>
 <br>
         Это происходит из-за некорректных настроек рекламных кампаний или изначально неверно выбранной стратегии. <br>
 <br>
         Проблемы решаются глубоким анализом и корректировкой стратегий.
      </p>
 </li>
      <li> <input type="checkbox" checked="">
      <h3 style="font-weight: normal;" align="center">Не уверены в подрядчике</h3>
      <p class="audit">
         Каждый на рынке называет себя экспертом. <br>
 <br>
         После заключения договора с некоторыми из них вы получаете шаблонные отчёты с красивыми графиками и формулировками. Но не видите результата. Слышите сложные формулировки и не понимаете их значения. Сами инициируете задачи. <br>
 <br>
         Мы можем объективно проверить работу текущего подрядчика.
      </p>
 </li>
    </ul>
  </div>
</div>
 <?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/prbel/result_slider.php"
  )
);?>  <?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/prbel/feedback_slider.php"
  )
);?> <?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/prbel/trust.php"
  )
);?>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/prbel/contact_us.php"
  )
);?>
<div class="services context-service" style="background-color: white;">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-center ">
        <h2 style="margin-bottom: 25px; padding-top: 10px;">Наши услуги</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="description">
           Для достижения наилучшего результата мы объединяем эффективные инструменты в рабочие связки
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-md-10 col-md-offset-1">
      <div class="row row-space ">
 <a href="/sozdanie-sajtov/">
        <div class="col-md-6 col-sm-6 col-space text-center row-space">
          <div class="info-block alt">
            <div class="service-title">
               Разработка сайтов
            </div>
            <p>
               Разрабатываем продающие и оптимизированные сайты для максимальной эффективности интернет-маркетинга
            </p>
            <div class="arrow_link">
            </div>
          </div>
        </div>
 </a>
        <!-- Seo --> <a href="/seo/">
        <div class="col-md-6 col-sm-6 col-space text-center row-space">
          <div class="info-block alt">
            <div class="service-title">
               SEO-продвижение
            </div>
            <p>
               Привлекаем трафик из поисковых систем. Работаем на рост посещаемости сайта и увеличение продаж в долгосрочной перспективе
            </p>
            <div class="arrow_link">
            </div>
          </div>
        </div>
 </a>
        <!-- / Context --> <!-- Services --> <a href="/kontekstnaya-reklama/">
        <div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
          <div class="info-block alt">
            <div class="service-title">
               Контекстная реклама
            </div>
            <p>
               Запускаем эффективные рекламные кампании в Яндекс и Google, ориентированные на конверсии, ROI и целевые показатели
            </p>
            <div class="arrow_link">
            </div>
          </div>
        </div>
 </a> <a href="/podderzhka-sajtov/">
        <div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
          <div class="info-block alt">
            <div class="service-title">
               Техподдержка сайтов
            </div>
            <p>
               Большой опыт позволяет нам осуществлять поддержку и сопровождение сайтов на Bitrix любой сложности
            </p>
            <div class="arrow_link">
            </div>
          </div>
        </div>
 </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-offset-0 col-md-12 col-sm-12 col-space text-center blank-element">
      <div href="#">
         <a class="b24-web-form-popup-btn-15" >
             <h2 style="padding-top: 30px;  margin-bottom: 25px;">Не нашли нужную услугу?</h2>
             <div class="description" style="margin-bottom:0px">
          <span style="color:#333">Опишите какую задачу нужно решить и мы найдем оптимальное решение</span>
        </div>
<button class="webform-services">
Написать</button>
                </a>
      </div>
    </div>
  </div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>