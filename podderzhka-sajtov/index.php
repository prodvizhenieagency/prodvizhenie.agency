<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Техническая поддержка и доработка сайтов на 1С&nbsp;Битрикс");

$APPLICATION->AddChainItem("Услуги", "/services/");
$APPLICATION->AddChainItem("Техническая поддержка и доработка сайтов на 1С-Битрикс");
?>

<section>


<div itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">									
	<div itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">								
		<meta itemprop="url" content="https://prodvizhenie.agency/local/templates/prbel/img/main/logo.png">					  		
	</div>								
	<link itemprop="url" href="https://prodvizhenie.agency/">																
	<div itemprop="address" itemscope="" itemtype="https://schema.org/PostalAddress">					  			
		<meta itemprop="postalCode" content="308033">							
		<meta itemprop="addressCountry" content="Россия">							
		<meta itemprop="addressRegion" content="Белгородская обл.">							
		<meta itemprop="addressLocality" content="г. Белгород,">							
		<meta itemprop="streetAddress" content="ул. Королева, 2а, корп. 2, офис 704">					  		
	</div>								
	<meta itemprop="telephone" content="+7 (4722) 777-431">
	<meta itemprop="telephone" content="+7 (800) 200-55-31">	
	<meta itemprop="email" content="go@pr-bel.ru">	
	<meta itemprop="image" content="https://prodvizhenie.agency/upload/medialibrary/775/7750e3c3025e78dd7fd439b58c50ba92.jpg">																
	<meta itemprop="brand" content="Digital-агенство Продвижение">								
	<meta itemprop="legalName" content="Digital-агенство Продвижение">								
	<meta itemprop="name" content="<?$APPLICATION->ShowTitle()?>">								
	<meta itemprop="description" content="<?$APPLICATION->ShowProperty('description');?>">								
</div>	

<div class="banner-about banner-with-img">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1 itemprop="headline" ><? $APPLICATION->ShowTitle(false);?></h1>
				<p class="baner-disc">
					 Оптимальный вариант для эффективного развития и поддержания сайта в рабочем состоянии — найти постоянного подрядчика, который будет заинтересован в успехе вашего проекта.
				</p>
			</div>
			<div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
				 <!--<img src="/local/templates/prbel/img/page/kotly.png" alt="" class="img-responsive">-->
			</div>
		</div>
		<div class="row krosh">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"schema",
Array()
);?>
		</div>
	</div>
</div>
<div class="direction">
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-2 col-lg-10">
				<div class="row">
					<div class="col-lg-6">
						<p>
							 Наша команда выполняет техническую поддержку сайтов с учетом целей конкретного бизнеса. Большой опыт позволяет нам осуществлять поддержку и сопровождение сайтов на Bitrix любой сложности на долгосрочной основе.
						</p>
						<p>
							 С нами вы получите гарантированный результат в установленные сроки.
						</p>
					</div>
					<div class="col-lg-6">
						<h2 style="margin-bottom:10px;">
						Специализируемся на <span style="white-space:nowrap">«1С-Битрикс»</span></h2>
						<p class="wow fadeInRight" style="visibility: visible; animation-delay: 0.0s; animation-name: fadeInRight;">
 <img width="120px" src="/images/zp.png"> <img width="120px" src="/images/uv.png">
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-10">
						<p>
 <b>
							Мы поможем вам решить следующие задачи: </b>
						</p>
						<ul>
							<li>
							привести функционал сайта в соответствие с бизнес-логикой компании (работа корзины, оформление заказа, способы доставки и т.д); </li>
							<li>
							подключить и настроить онлайн-сервисы (обратный звонок, онлайн-чаты, формы подписки и др.); </li>
							<li>
							выполнить интеграцию с 1С и CRM-системами; </li>
							<li>
							SEO-доработки различной сложности; </li>
							<li>
							интегрировать систему онлайн-оплаты; </li>
							<li>
							создать мобильную или адаптивную версии сайта; </li>
							<li>
							перенести сайт на HTTPS; </li>
							<li>
							оптимизировать скорость загрузки сайта; </li>
							<li>
							устранить проблемы со стабильностью; </li>
							<li>
							выполнить контентные работы, обновить информацию на сайте. </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 #WF_SEO_TEXT_1#
<div class="why-profit">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2>
				Услуга технической поддержки сайта включает<br>
				 в себя работу по трём направлениям:</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-2">
				<div class="row">
					 <!--<div class="col-lg-1 col-md-1 col-sm-2 col-xs-12">
            <div class="img-wrap">
 <img src="/local/templates/prbel/img/page/check.svg" alt="" class="img-responsive">
            </div>
          </div>-->
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="profit-title">
							 Решение текущих задач
						</div>
						<div class="profit-text">
							 Оперативное решение текущих задач – обновление контента на сайте, исправление обнаруженных ошибок, консультирование по работе с сайтом, небольшие срочные доработки функционала.
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="profit-title">
							 Развитие проекта
						</div>
						<div class="profit-text">
							 Наша команда выполняет техническую поддержку сайтов с учетом целей конкретного бизнеса. Большой опыт позволяет нам осуществлять поддержку и сопровождение сайтов любой сложности на долгосрочной основ С нами вы получите гарантированный результат в установленные сроки.
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="profit-title">
							 Стабильность и безопасность
						</div>
						<div class="profit-text">
							 Мониторинг доступности сайта, система резервного копирования, устранение проблем со скоростью работы сайта, мониторинг наличия вредоносного кода.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="step-to-step">
	<div class="container" style="margin-bottom:81px;">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h2 style="color:#1a242d; margin-bottom:81px;">Тарифы</h2>
			</div>
		</div>
		<div class="row">
			<div class="price_blocks">
				<div class="col-lg-12">
					<div class="col-lg-6" style="padding: 0px;">
						<ul class="price_blocks">
							<li>
							<div style="visibility: visible; animation-delay: 0.0s; animation-name: zoomIn;" class="wow zoomIn">
								<h5 style="color:#000; text-transform:uppercase" align="center">Разовые работы</h5>
							</div>
 </li>
						</ul>
					</div>
					<div class="col-lg-6" style="padding: 0px">
						<ul class="price_blocks" style="text-align: center; padding-bottom:16px; color: #000;">
							<li>
							<div style="visibility: visible; animation-delay: 0.0s; animation-name: zoomIn;" class="wow zoomIn">
 <b>1900 ₽/час</b>
							</div>
 </li>
						</ul>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="col-lg-6" style="padding: 0px;">
						<ul class="price_blocks">
							<li> <input type="checkbox" checked="">
							<div style="visibility: visible; animation-delay: 0.5s; animation-name: zoomIn;" class="wow zoomIn">
								<h5 style="color:#000; text-transform:uppercase;" align="center">Абонентская поддержка</h5>
							</div>
							<p class="price_blocks">
								 – Минимальная стоимость часа<br>
								 – Формат подходит, когда есть гарантированный ежемесячный объём работ<br>
								 – Минимальный объём пакета – 10 часов в месяц<br>
								 – Остаток часов из месяца в месяц не переносится<br>
								 – Минимальный срок договора – 3 месяца<br>
								 – Работы сверх пакета тарифицируются как разовые
							</p>
 </li>
						</ul>
					</div>
					<div class="col-lg-6" style="padding: 0px;">
						<ul class="price_blocks" style="text-align: center; color: #000;">
							<li>
							<div style="visibility: visible; animation-delay: 0.5s; animation-name: zoomIn;" class="wow zoomIn">
 <b>1700 ₽/час</b>
							</div>
 </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h2>В любом тарифе включено:</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-12 number-col">
						<div class="wrap">
							 1
						</div>
					</div>
					<div class="col-lg-10 col-ms-10 col-sm-12">
						<div class="step-title">
							 Мониторинг доступности сайта
						</div>
						<div class="step-text">
							 Специальный сервис проверяет доступность сайта каждую минуту, в случае проблем мы моментально получаем уведомление и начинаем устранение
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm1-2 number-col">
						<div class="wrap">
							 2
						</div>
					</div>
					<div class="col-lg-10 col-ms-10 col-sm-12">
						<div class="step-title">
							 Взаимодействие с техподдержкой любых внешних сервисов
						</div>
						<div class="step-text">
							 Возьмём на себя общение с техподдержкой хостинга, доменного регистратора, онлайн-консультанта, сервисом эквайринга и т.д.
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-12 number-col">
						<div class="wrap">
							 3
						</div>
					</div>
					<div class="col-lg-10 col-ms-10 col-sm-12">
						<div class="step-title">
							 Контроль скорости работы сайта
						</div>
						<div class="step-text">
							 Вовремя выявляем снижение производительности сайта или его отдельных частей, предложим варианты решения
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-12 number-col">
						<div class="wrap">
							 4
						</div>
					</div>
					<div class="col-lg-10 col-ms-10 col-sm-12">
						<div class="step-title">
							 Регулярное резервное копирование
						</div>
						<div class="step-text">
							 Настроим автоматическое резервное копирование по расписанию
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-12 number-col">
						<div class="wrap">
							 5
						</div>
					</div>
					<div class="col-lg-10 col-ms-10 col-sm-12">
						<div class="step-title">
							 Тестовая копия сайта
						</div>
						<div class="step-text">
							 Все доработки проводим на тестовой копии сайта и публикуем изменения только после согласования
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-12 number-col">
						<div class="wrap">
							 6
						</div>
					</div>
					<div class="col-lg-10 col-ms-10 col-sm-12">
						<div class="step-title">
							 Контроль продления услуг
						</div>
						<div class="step-text">
							 Вы не пропустите оплату хостинга и домена
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="cta-banner" style="padding-bottom: 0px;">
	<div class="container">
		<div class="col-12 col-md-12 col-lg-12 banner_seo_txt">
			 #WF_SEO_TEXT_2#
<br>
			 <img src="/images/Ira_square-min.png" class="img-responsive" style="border-radius: 50%; width: 80px; margin-top: 20px; float: left; margin-right: 20px;">		

			<p class="name_seo">
				 Ирина Воронкова
			</p>
			<p class="name_job" style="margin-bottom: 70px;">
				 Ведущий менеджер проектов
			</p>
		</div>
		<!-- <div class="col-12 col-md-5 col-lg-5 mt-auto mb-0 block_photo" style="margin-bottom: -14px;">
 <img alt="Ирина Воронкова - Ведущий менеджер проектов" src="/images/Ira.png" title="Ирина Воронкова - Ведущий менеджер проектов" class="podderzhka_photo">
		</div> -->
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"bitrix:news",
	"projects_main",
	Array(
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "100000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "projects_main",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",
		"DETAIL_BRAND_PROP_CODE" => array(0=>"TIZERS",1=>"",),
		"DETAIL_BRAND_USE" => "Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array(0=>"NAME",1=>"PREVIEW_TEXT",2=>"DETAIL_TEXT",3=>"DETAIL_PICTURE",4=>"",),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array(0=>"",1=>"FORM_ORDER",2=>"FORM_QUESTION",3=>"ORDERER",4=>"SITE",5=>"DATA",6=>"AUTHOR",7=>"LINK_PROJECTS",8=>"TASK_PROJECT",9=>"LINK_COMPANY",10=>"LINK_REVIEWS",11=>"LINK_GOODS",12=>"LINK_SERVICES",13=>"FORM_PROJECT",14=>"DOCUMENTS",15=>"PHOTOS",16=>"",),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_STRICT_SECTION_CHECK" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_TYPE_VIEW" => "element_1",
		"FILTER_FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "arProjectFilter",
		"FILTER_PROPERTY_CODE" => array(0=>"",1=>"",),
		"FORM_ID" => "14",
		"FORM_ID_ORDER_SERVISE" => "14",
		"GALLERY_TYPE" => "big",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "35",
		"IBLOCK_TYPE" => "aspro_digital_content",
		"IMAGE_POSITION" => "left",
		"IMAGE_WIDE" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"LINE_ELEMENT_COUNT" => "2",
		"LINE_ELEMENT_COUNT_LIST" => "3",
		"LIST_ACTIVE_DATE_FORMAT" => "j F Y",
		"LIST_FIELD_CODE" => array(0=>"NAME",1=>"PREVIEW_TEXT",2=>"DETAIL_PICTURE",3=>"",),
		"LIST_PROPERTY_CODE" => array(0=>"LINK_TO_CASE",1=>"",),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "211",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SECTION_ELEMENTS_TYPE_VIEW" => "FROM_MODULE",
		"SEF_FOLDER" => "/projects/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => array("news"=>"","section"=>"#SECTION_CODE_PATH#/","detail"=>"#SECTION_CODE_PATH#/#ELEMENT_CODE#/",),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHARE_HANDLERS" => array(0=>"facebook",1=>"mailru",2=>"delicious",3=>"lj",4=>"twitter",5=>"vk",),
		"SHARE_HIDE" => "N",
		"SHARE_SHORTEN_URL_KEY" => "",
		"SHARE_SHORTEN_URL_LOGIN" => "",
		"SHARE_TEMPLATE" => "",
		"SHOW_404" => "N",
		"SHOW_DETAIL_LINK" => "Y",
		"SHOW_NEXT_ELEMENT" => "Y",
		"SHOW_SECTION_DESCRIPTION" => "Y",
		"SHOW_SECTION_PREVIEW_DESCRIPTION" => "Y",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"STRICT_SECTION_CHECK" => "N",
		"S_ASK_QUESTION" => "",
		"S_ORDER_PROJECT" => "Заказать проект",
		"S_ORDER_SERVISE" => "Заказать проект",
		"T_CHARACTERISTICS" => "",
		"T_CLIENTS" => "",
		"T_DOCS" => "",
		"T_GALLERY" => "",
		"T_GOODS" => "",
		"T_NEXT_LINK" => "Следующий проект",
		"T_PREV_LINK" => "",
		"T_PROJECTS" => "Похожие проекты",
		"T_REVIEWS" => "Отзыв клиента",
		"T_SERVICES" => "",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_REVIEW" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "Y"
	)
);?>
<div class="services context-service">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center ">
				<h2 style="margin-bottom: 25px; padding-top: 10px;">Кроме технической поддержки сайтов</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2">
				<div class="description">
					 Для достижения наилучшего результата мы объединяем эффективные инструменты в рабочие связки
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-10 col-md-offset-1">
			<div class="row row-space ">
				 <!-- Seo --> <a href="/seo/">
				<div class="col-md-6 col-sm-6 col-space text-center row-space">
					<div class="info-block alt">
						<div class="service-title">
							 SEO-продвижение
						</div>
						<p>
							 Привлекаем трафик из поисковых систем. Работаем на рост посещаемости сайта и увеличение продаж в долгосрочной перспективе
						</p>
						<div class="arrow_link">
						</div>
					</div>
				</div>
 </a>
				<!-- / Context --> <!-- Services --> <a href="/kontekstnaya-reklama/">
				<div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
					<div class="info-block alt">
						<div class="service-title">
							 Контекстная реклама
						</div>
						<p>
							 Запускаем эффективные рекламные кампании в Яндекс и Google, ориентированные на конверсии, ROI и целевые показатели
						</p>
						<div class="arrow_link">
						</div>
					</div>
				</div>
 </a>
				<!-- / Development --> <!-- Services --> <a href="/sozdanie-sajtov/">
				<div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
					<div class="info-block alt">
						<div class="service-title">
							 Разработка сайтов
						</div>
						<p>
							 Разрабатываем продающие и оптимизированные сайты для максимальной эффективности интернет-маркетинга
						</p>
						<div class="arrow_link">
						</div>
					</div>
				</div>
 </a> <a href="/audit/">
				<div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
					<div class="info-block alt">
						<div class="service-title">
							 Аудит
						</div>
						<p>
							 Проконсультируем, какие действия предпринять, чтобы потенциальные клиенты находили вас быстрее
						</p>
						<div class="arrow_link">
						</div>
					</div>
				</div>
 </a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-offset-0 col-md-12 col-sm-12 col-space text-center blank-element">
			<div href="#">
				 <a class="b24-web-form-popup-btn-15" >
             <h2 style="padding-top: 30px;  margin-bottom: 25px;">Не нашли нужную услугу?</h2>
             <div class="description" style="margin-bottom:0px">
          <span style="color:#333">Опишите какую задачу нужно решить и мы найдем оптимальное решение</span>
        </div>
<button class="webform-services">
Оставить заявку</button>
                </a>
			</div>
		</div>
	</div>
</div>

</section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/trust.php"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/contact_us_podderzhka.php"
	)
);?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>