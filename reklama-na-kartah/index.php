<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "⚡ Эффективная реклама на Яндекс и Google картах от Digital-агентства \"Продвижение\". ⭐Увеличение трафика, приоритетное размещение. Звоните: ☎️ #WF_META#");
$APPLICATION->SetPageProperty("title", "Реклама на картах в #WF_CITY_PRED# - реклама на Яндекс и Google картах");
$APPLICATION->SetTitle("Реклама на картах в #WF_CITY_PRED#");
?>
<div class="banner-about banner-with-img">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1><? $APPLICATION->ShowTitle(false);?></h1>
                <p class="baner-disc">
                     #WF_SEO_TEXT_1#
                </p>
            </div>
            <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                 <!--<img src="/local/templates/prbel/img/page/kotly.png" alt="" class="img-responsive">-->
            </div>
        </div>
        <div class="row krosh">
             <?$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "schema",
Array()
);?>
        </div>
    </div>
</div>


<div class="direction">
    <div class="container">
        <div class="row">
            <div class="col-lg-offset-1 col-lg-11">
                 #WF_SEO_TEXT_2#
         
            </div>
        </div>
    </div>
</div>

#WF_SEO_TEXT_9#


<div class="why-profit" style="background-color: #efefef">
    <div class="container">
        <div class="row text-center">
         <h2>Преимущества рекламы на Яндекс.Картах</h2>
        </div>
        <div class="row">
             #WF_SEO_TEXT_3#
        </div>
    </div>
</div>

<div class="step-to-step" style="background-color: white;">
    <div class="container">
         #WF_SEO_TEXT_4#
    </div>
</div>

<div class="why-profit" style="background-color: #efefef">
        <div class="container">
 #WF_SEO_TEXT_5#
</div>
</div>
      
<div class="why-profit" style="background-color: white">
    <div class="container">
                 #WF_SEO_TEXT_6#
             </div>
         </div>
         
    <div class="right-context" style="background-color: #efefef">
    <div class="container">
 #WF_SEO_TEXT_7#
    </div>
</div>

<div class="step-to-step" id="last_block" style="background-color: white">
    <div class="container">
         #WF_SEO_TEXT_8#
    </div>
</div

<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    Array(
        "AREA_FILE_SHOW" => "file",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/prbel/contact_us_reklama.php"
    )
);?>



<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    Array(
        "AREA_FILE_SHOW" => "file",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/prbel/trust.php"
    )
);?>







<div class="services context-service" style="background-color: #efefef;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center ">
                <h2 style="margin-bottom: 25px; padding-top: 10px;">Кроме рекламы на картах</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <div class="description">
                     Для достижения наилучшего результата мы объединяем эффективные инструменты в рабочие связки
                </div>
            </div>
        </div>
            <div class="col-xs-12 col-md-10 col-md-offset-1">
        <div class="row row-space ">
             <!-- Seo --> <a href="/seo/">
            <div class="col-md-6 col-sm-6 col-space text-center row-space">
                <div class="info-block alt">
                    <div class="service-title">
                         SEO-продвижение
                    </div>
                    <p>
                         Привлекаем трафик из поисковых систем. Работаем на рост посещаемости сайта и увеличение продаж в долгосрочной перспективе
                    </p>
                    <div class="arrow_link">
                    </div>
                </div>
            </div>
 </a>
            <!-- / Context --> <!-- Services --> <a href="/kontekstnaya-reklama/">
            <div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
                <div class="info-block alt">
                    <div class="service-title">
                         Контекстная реклама
                    </div>
                    <p>
                         Запускаем эффективные рекламные кампании в Яндекс и Google, ориентированные на конверсии, ROI и целевые показатели
                    </p>
                    <div class="arrow_link">
                    </div>
                </div>
            </div>
 </a>
            <!-- / Development --> <!-- Services --> <a href="/podderzhka-sajtov/">
            <div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
                <div class="info-block alt">
                    <div class="service-title">
                         Техподдержка сайтов
                    </div>
                    <p>
                         Большой опыт позволяет нам осуществлять поддержку и сопровождение сайтов на Bitrix любой сложности на долгосрочной основе
                    </p>
                    <div class="arrow_link">
                    </div>
                </div>
            </div>
 </a>
<a href="/audit/">
            <div class="col-md-6 col-sm-6 col-md-offset-0 col-space text-center row-space">
                <div class="info-block alt">
                    <div class="service-title">
                         Аудит
                    </div>
                    <p>
                         Проконсультируем, какие действия предпринять, чтобы потенциальные клиенты находили вас быстрее
                    </p>
                    <div class="arrow_link">
                    </div>
                </div>
            </div>
 </a>
        </div>
        </div>
    </div>
<div class="row">
            <div class="col-lg-offset-0 col-md-12 col-sm-12 col-space text-center blank-element">
                <div href="#">
                     <a class="b24-web-form-popup-btn-15" >
                         <h2 style="padding-top: 30px;  margin-bottom: 25px;">Не нашли нужную услугу?</h2>
                         <div class="description" style="margin-bottom:0px">
                    <span style="color:#333">Опишите какую задачу нужно решить и мы найдем оптимальное решение</span>
                </div>
<button class="webform-services">
Написать</button>
                                </a>
                </div>
            </div>
    </div>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>











