<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Композитный сайт");
?><style>
.my_video {
  width: 100%;
  position: relative;
  padding-top: 52%;
}
.my_video iframe, .video object, .video embed{
  position: absolute;
top:0;
left:0;
  width: 88%;
  height: 100%;
}
</style>
<div class="banner-about banner-with-img">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1><? $APPLICATION->ShowTitle(false);?></h1>
				<p class="baner-disc">
					 Уникальная технология сайтов на 1С-Битрикс. Получите преимущество перед конкурентами.
				</p>
			</div>
			<div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
				 <!--<img src="/local/templates/prbel/img/page/kotly.png" alt="" class="img-responsive">-->
			</div>
		</div>
		<div class="row krosh">
			<p>
 <a href="/">Главная</a> <i class="fa fa-angle-right" aria-hidden="true"></i>
				Композитный сайт
			</p>
		</div>
	</div>
</div>
<div class="direction">
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-1 col-lg-11">
				<div class="row">
					<div class="col-lg-7">
						<p>
							 По композитной технологии могут работать все сайты, разработанные на платформе «1С-Битрикс: Управление сайтом».
						</p>
						<p>
							 Уникальная технология производства сайтов объединяет в себе высокую скорость загрузки статического сайта и все возможности динамического сайта.
						</p>
						<p>
							 Пользователь мгновенно получает контент страницы.
						</p>
					</div>
					<div class="col-lg-5">
 <img src="https://bp-oblako.ru/local/img/kompozit.gif" style="margin-left: -8px;"> <br>
 <br>
 <img src="https://partners.1c-bitrix.ru/images/partners/gold_sm.gif">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="why-dev">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h2>Какие преимущества дает Композитный сайт?</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 dev-item">
				<div class="dev-icon">
 <img src="/images/icons/vizitka.svg" alt="">
				</div>
				<div class="dev-title">
					 Отклик сайта в 100 раз быстрее
				</div>
				<div class="dev-text">
					 Совершенно новый цикл загрузки страниц. Пользователь мгновенно получает контент страницы и и может работать с ним.
				</div>
			</div>
			<div class="col-lg-3 dev-item">
				<div class="dev-icon">
 <img src="/images/icons/landing.svg" alt="">
				</div>
				<div class="dev-title">
					 Лучшее ранжирование сайтов в Google и Яндекс
				</div>
				<div class="dev-text">
					 Скорость загрузки сайта существенно влияет на позиции его ранжирования поисковыми системами.
				</div>
			</div>
			<div class="col-lg-3 dev-item">
				<div class="dev-icon">
 <img src="/images/icons/e-commerce.svg" alt="">
				</div>
				<div class="dev-title">
					 Рост конверсии сайта
				</div>
				<div class="dev-text">
					 У посетителей сайта быстро загружаются страницы, фильтруются товары интернет-магазина, не происходит задержек в загрузке большого объема контента.
				</div>
			</div>
			<div class="col-lg-3 dev-item">
				<div class="dev-icon">
 <img src="/images/icons/corporate.svg" alt="">
				</div>
				<div class="dev-title">
					 Удобство администрирования
				</div>
				<div class="dev-text">
					 Редактирование информации и обновление каталога товаров на сайте происходит занимает минимальное количество времени.
				</div>
			</div>
		</div>
	</div>
</div>
<div class="why-profit">
	<div class="container">
		<div class="row text-center">
			<h2>
			Как это работает?</h2>
		</div>
		<div class="row">
			<div class="col-lg-offset-1 col-lg-11">
				<div class="row">
					 <!--<div class="col-lg-1 col-md-1 col-sm-2 col-xs-12">
						<div class="img-wrap">
 <img src="/local/templates/prbel/img/page/check.svg" alt="" class="img-responsive">
						</div>
					</div>-->
					<div class="col-lg-12">
 <img width="250px;" src="https://wbest.ru/load/source/divide-img.jpg" style="float: left; padding-right: 60px; padding-bottom: 20px;">
						<div class="profit-title">
							 Страница разделяется на 2 соcтавляющие: статическую и динамическую
						</div>
						<div class="profit-text">
							 Совершенно новый цикл загрузки страниц
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
 <img width="250px;" src="https://sb-vnedr.ru/upload/medialibrary/b5c/cache.jpg" style="float: left; padding-right: 60px; padding-bottom: 20px;">
						<div class="profit-title">
							 Статическая часть кешируется и отображается мгновенно
						</div>
						<div class="profit-text">
							 Пользователь сразу видит содержимое и может работать с ним
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
 <img width="250px;" src="https://sb-vnedr.ru/upload/medialibrary/129/download.jpg" style="float: left; padding-right: 60px; padding-bottom: 70px;">
						<div class="profit-title">
							 Динамическая часть подгружается в фоновом режиме <br>
							 и кешируется в браузере посетителя
						</div>
					</div>
				</div>
				<div class="my_video">
					 <iframe style="padding-bottom: 50px;" src="https://www.youtube.com/embed/jo4A4Wqlksc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/prbel/contact_us.php"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>